#!/bin/bash

# If the first argument is just '-', do not replace the files
# instead print the result to stdout.

clang-format -i "$@"
# Run clang-format a 2nd time, this stabilizes some of the comment positioning.
clang-format -i "$@"
