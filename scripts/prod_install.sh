#!/bin/sh
PYTHON=/usr/local/bin/python3.7
PIP=/usr/local/bin/pip3.7

# Test if Python 3.7 is installed
if ! [ -x "$(command -v $PYTHON)" ]; then
	# Install Python 3.7 from sources
	sudo apt-get update -y
	sudo apt-get install build-essential tk-dev libncurses5-dev libncursesw5-dev libreadline6-dev libdb5.3-dev libgdbm-dev libsqlite3-dev libssl-dev libbz2-dev libexpat1-dev liblzma-dev zlib1g-dev libffi-dev -y
	wget https://www.python.org/ftp/python/3.7.2/Python-3.7.2.tgz
	tar xvf Python-3.7.2.tgz
	cd Python-3.7.2
	./configure
	make -j 4
	sudo make altinstall
	cd ..
	sudo rm -rf Python-3.7.2
fi

# Test if virtualenv is installed
if ! [ -x "$(command -v virtualenv)"]; then
	# Install virtualenv
	sudo $PIP install virtualenv
fi

# Install server's dependencies from scratch
rm -rf env
virtualenv --python=$PYTHON env
source env/bin/activate
pip install -r raspberry/requirements.txt
deactivate

# Install startup script
echo "
[Unit]
Description=strat-server
Wants=network-online.target
After=network.target

[Service]
TimeoutStartSec=0
Restart=always
WorkingDirectory=$(pwd)
ExecStart=$(pwd)/scripts/start_server_production.sh

[Install]
WantedBy=multi-user.target
" | sudo tee /etc/systemd/system/unitec-strat.service

# Start startup script and enable it at startup
sudo systemctl daemon-reload
sudo systemctl restart unitec-strat
sudo systemctl enable unitec-strat
