#!/bin/bash

virtualenv --python=python3.7 env
source env/bin/activate
pip install -r raspberry/requirements.txt