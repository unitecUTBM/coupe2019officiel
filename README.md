# Python coding rules

## Black

If you're on Debian and you don't have Python 3.6+.. sorry for you

[GitHub link](https://github.com/ambv/black)

### Emacs plugin

https://github.com/proofit404/blacken

### Vim plugin

https://github.com/ambv/black/blob/master/plugin/black.vim


## Type hints

Type hints are mandatory on ALL functions.

[Documentation link](https://docs.python.org/3/library/typing.html)

## Docstrings

Docstrings are mandatory on ALL functions.

# C coding rules

For formating C and protobuf files, we uses [clang-format](https://clang.llvm.org/docs/ClangFormat.html). Formating rules are stored in the .clang-format file.

You can use `clang-format -i your_file` to format a file or `./scripts/clang-format-apply.sh` after a commit to format every modified files compared to head.

### Sublime and VSCode

If you are using cquery lsp, you can use their formating feature.

### Vim plugin

https://github.com/rhysd/vim-clang-format

### Emacs plugin

Ask Bro :P

# Raspberry API documentation

```
GET    /stratlib
GET    /scripts
GET    /scripts/{script_name}
DELETE /scripts/{script_name}
PUT    /scripts/{script_name}
GET    /scripts/selected
PATCH  /scripts/{script_name}
POST   /scripts/{script_name}/select
```

# 2019 Robotic French Cup rules

[2019 rules](https://www.coupederobotique.fr/wp-content/uploads/Eurobot2019_Rules_Cup_OFFICIAL_FR.pdf)
