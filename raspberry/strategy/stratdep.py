from raspberry.strategy.requests import *

from raspberry.communication.communication import Communication
from raspberry.communication.buffer_pb2 import *

import datetime, time

from typing import Callable
from threading import Lock, Thread

__communication = Communication(is_virtual=True)
__communication.listen()

GAME_TIME = 10  # seconds
RESPONSE_TIMEOUT = 0.200  # seconds

CHECK_FREQUENCY = 0.001  # seconds

mutex = Lock()


def external_check():
    """
    Fonction appelé par un thread séparé qui vérifie certains capteurs et
    si le temps de jeu est écoulé

    Elle va interrompre les actions en cours si besoin est.
    """
    check_sensor = True
    while check_sensor:
        time.sleep(CHECK_FREQUENCY)
        # Vérifie si les capteurs infra-rouge détectent un obstacle
        address, request = check_IR_send_infos()
        obstacle = send_and_wait_response(address, request, obstacle_condition)
        # Si c'est le cas, envoi d'une requête pour que le robot s'arrête
        if obstacle:
            address, request = pause_send_infos()
            send_and_wait_response(address, request)
        # Sinon le robot peut continuer ses actions
        else:
            address, request = resume_send_infos()
            send_and_wait_response(address, request)

        # Si le temps de jeu s'est écoulé, arrête la vérification (sort de la boucle)
        if (datetime.datetime.now() - starting_time).total_seconds() > GAME_TIME:
            check_sensor = False
    # Envoi d'une requête pour que le robot s'arrête
    address, request = final_stop_send_infos()
    print("time's up")
    send_and_wait_response(address, request, unlock=False)
    __communication.stop_listening()
    __communication.stack.show()


def send_and_wait_response(
    address: int,
    request: Request,
    condition: Callable[[Item], bool] = no_condition,
    unlock: bool = True,
) -> bool:
    """
    Envoie une requête protobuf et bloque jusqu'à la reception d'une confirmation 

    :param address: l'adresse pour laquelle envoyer le message
    :param request: la requête protobuf à envoyer
    :param condition: une fonction pour vérifier l'item reçu
    :param unlock: si l'envoi de message doit continuer d'être bloqué après la réception

    :return: True si une réponse avec la bonne condition a été reçue, False sinon
    """

    mutex.acquire()
    print("acquired ", datetime.datetime.now() - starting_time)
    __communication.send(address, request)
    item_received = wait_response(address)
    if unlock:
        mutex.release()
        print("release ", datetime.datetime.now() - starting_time)

    if item_received is None:
        return False
    return condition(item_received)


def wait_response(address: int) -> Item:
    """
    Écoute l'envoi de message sur l'adresse spécifiée et bloque jusqu'à ce que la bonne réponse
    est reçue ou que le timeout est écoulé

    :param address: l'adresse à écouter

    :return: l'item de la réponse ou None si aucun paquet n'a été reçu avant le timeout
    """

    timeout_start = datetime.datetime.now()

    is_timeout = False
    response_received = False
    while not response_received and not is_timeout:
        last_item = __communication.stack.get_last()
        if last_item.packet.address == address and not last_item.from_master:
            # if communication.stack.filter(filters.response_items, address=address) != []:
            response_received = True

        if (datetime.datetime.now() - timeout_start).total_seconds() > RESPONSE_TIMEOUT:
            is_timeout = True

    if response_received:
        return last_item
    return None


def send_and_block_until(
    address: int, request: Request, condition: Callable[[Item], bool]
) -> None:
    """
    Envoie une requête protobuf et bloque jusqu'à la reception d'un item avec la bonne condition

    :param address: l'adresse pour laquelle envoyer le message
    :param request: la requête protobuf à envoyer
    :param condition: une fonction pour vérifier l'item reçu
    """
    block = True
    while block:
        mutex.acquire()
        __communication.send(address, request)
        item_received = wait_response(address)
        mutex.release()
        if not item_received is None:
            block = not condition(item_received)


starting_time = datetime.datetime.now()
sensor_check_thread = Thread(target=external_check)
sensor_check_thread.start()
