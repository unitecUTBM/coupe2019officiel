from raspberry.strategy.requests import *
from raspberry.strategy.stratdep import *


def move_forward(length: float, wait_for_end: bool = False) -> None:
    address, request = move_forward_send_infos(length)
    print("moving forward ?")

    if wait_for_end:
        send_and_block_until(address, request, move_ended_condition)
    else:
        if send_and_wait_response(address, request):
            print("moving forward for %f" % length)
        else:
            print("no confirmation for moving forward")


def suck_puck() -> None:
    address, request = suck_puck_send_infos()
    print("sucking puck ?")
    if send_and_wait_response(address, request):
        print("sucking puck")
    else:
        print("no confirmation for sucking puck")


def wait_for_move_end() -> None:
    address, request = check_move_send_infos()
    print("waiting for movement to end")
    send_and_block_until(address, request, move_ended_condition)
    print("movement ended")
