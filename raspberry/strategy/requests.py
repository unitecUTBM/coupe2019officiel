from raspberry.communication.item import Item
from raspberry.communication.buffer_pb2 import *
from raspberry.communication.packet import Board

from typing import Tuple
from enum import IntEnum

# Ce fichier contient les différentes requêtes à envoyer
# et les conditions sur les items reçus

# Les conditions sont des fonctions prenant un item en argument
# et renvoyant un booléen. Elles servent à tester qu'un item de
# réponse correspond bien à ce qu'on attend


def no_condition(item_received: Item) -> bool:
    return True


def move_forward_send_infos(length: float) -> Tuple[int, Request]:
    request = Request()
    request.method = SET
    request.config.kp_position = 11
    request.config.ki_position = length
    return Board.MOTOR, request


def check_move_send_infos() -> Tuple[int, Request]:
    request = Request()
    request.method = GET
    return Board.MOTOR, request


def move_ended_condition(item_received: Item) -> bool:
    return item_received.request.sensor.position.x == 0


def suck_puck_send_infos():
    request = Request()
    request.method = SET
    return Board.PUMP, request


def check_IR_send_infos() -> Tuple[int, Request]:
    request = Request()
    request.method = GET
    return Board.SENSOR, request


def obstacle_condition(item_received: Item) -> bool:
    return item_received.request.sensor.position.x < 100


def final_stop_send_infos() -> Tuple[int, Request]:
    request = Request()
    request.method = SET
    return Board.SENSOR, request


def pause_send_infos() -> Tuple[int, Request]:
    request = Request()
    request.method = SET
    return Board.MOTOR, request


def resume_send_infos() -> Tuple[int, Request]:
    request = Request()
    request.method = SET
    return Board.MOTOR, request
