# -*- coding:utf-8 -*
import os
import pytest
import json
from datetime import datetime
from http import HTTPStatus
from . import server


@pytest.fixture
def client():
    client = server.app.test_client()

    yield client


@pytest.fixture(autouse=True)
def clean_env():
    clean_server_scripts_dir()
    clean_settings()


def clean_server_scripts_dir():
    for file in os.listdir(server.SCRIPT_DIRECTORY):
        os.remove(os.path.join(server.SCRIPT_DIRECTORY, file))


def clean_settings():
    if os.path.exists(server.SETTINGS_PATH):
        os.remove(server.SETTINGS_PATH)
    server.__settings = {}


def test_get_stratlib(client):

    stratlib = server.STRATLIB_PATH
    server.STRATLIB_PATH = "randompath"
    resp = client.get("/stratlib")
    assert resp.status_code == HTTPStatus.NOT_FOUND
    server.STRATLIB_PATH = stratlib

    resp = client.get("/stratlib")
    assert resp.status_code == HTTPStatus.OK

    with open(server.STRATLIB_PATH, "r") as stratlib:
        assert resp.data.decode("utf-8") == stratlib.read()


def test_list_scripts(client):

    os.chdir(server.SCRIPT_DIRECTORY)
    open("script1.py", "w+").close()
    open("script2.py", "w+").close()
    open("script3.md.py", "w+").close()
    open("false_script.py.md", "w+").close()
    open("script1.rb", "w+").close()
    os.chdir("..")

    resp = client.get("/scripts")
    assert resp.status_code == HTTPStatus.OK

    assert (
        len(
            set(json.loads(resp.data.decode("utf-8"))).intersection(
                ["script1.py", "script2.py", "script3.md.py"]
            )
        )
        == 3
    )


def test_selected_script(client):

    resp = client.get("/scripts/selected")
    assert resp.status_code == HTTPStatus.NO_CONTENT

    server.set_settings_value("selected_script", "test.py")
    resp = client.get("/scripts/selected")
    assert resp.status_code == HTTPStatus.OK
    assert resp.data == b"test.py"


def test_select_script(client):

    open(os.path.join(server.SCRIPT_DIRECTORY, "script.py"), "w+").close()

    resp = client.post("/scripts/wrongfile/select")
    assert resp.status_code == HTTPStatus.BAD_REQUEST

    resp = client.post("/scripts/missingfile.py/select")
    assert resp.status_code == HTTPStatus.NOT_FOUND

    resp = client.post("/scripts/script.py/select")
    assert resp.status_code == HTTPStatus.NO_CONTENT
    assert server.get_settings_value("selected_script") == "script.py"


def test_get_script(client):

    script_path = os.path.join(server.SCRIPT_DIRECTORY, "script.py")
    with open(script_path, "w+") as script:
        script.write("this is a test")

    resp = client.get("/scripts/wrongfile")
    assert resp.status_code == HTTPStatus.BAD_REQUEST

    resp = client.get("/scripts/missingfile.py")
    assert resp.status_code == HTTPStatus.NOT_FOUND

    resp = client.get("/scripts/script.py")
    assert resp.status_code == HTTPStatus.OK
    assert resp.data == b"this is a test"

    server.lock_script("script.py")
    resp = client.get("/scripts/script.py")
    assert resp.status_code == HTTPStatus.LOCKED
    server.unlock_script("script.py")


def test_delete_script(client):

    script_path = os.path.join(server.SCRIPT_DIRECTORY, "script.py")
    open(script_path, "w+").close()

    resp = client.delete("/scripts/wrongfile")
    assert resp.status_code == HTTPStatus.BAD_REQUEST

    resp = client.delete("/scripts/missingfile.py")
    assert resp.status_code == HTTPStatus.NOT_FOUND

    server.lock_script("script.py")
    resp = client.delete("/scripts/script.py")
    assert resp.status_code == HTTPStatus.LOCKED
    server.unlock_script("script.py")

    resp = client.delete("/scripts/script.py")
    assert resp.status_code == HTTPStatus.NO_CONTENT
    assert not os.path.exists(script_path)


def test_rename_script(client):

    script_path = os.path.join(server.SCRIPT_DIRECTORY, "script.py")
    with open(script_path, "w+") as script:
        script.write("this is a test")

    resp = client.patch("/scripts/missingfile.py")
    assert resp.status_code == HTTPStatus.NOT_FOUND

    resp = client.patch("/scripts/wrongfile")
    assert resp.status_code == HTTPStatus.BAD_REQUEST

    resp = client.patch("/scripts/script.py")
    assert resp.status_code == HTTPStatus.BAD_REQUEST

    resp = client.patch("/scripts/script.py", data="wrongname")
    assert resp.status_code == HTTPStatus.BAD_REQUEST

    resp = client.patch("/scripts/script.py", data="wrong name")
    assert resp.status_code == HTTPStatus.BAD_REQUEST

    resp = client.patch("/scripts/script.py", data="wrong name \n")
    assert resp.status_code == HTTPStatus.BAD_REQUEST

    resp = client.patch("/scripts/script.py", data="wrong name \n.py")
    assert resp.status_code == HTTPStatus.BAD_REQUEST

    server.lock_script("script.py")
    resp = client.patch("/scripts/script.py", data="test2.py")
    assert resp.status_code == HTTPStatus.LOCKED
    server.unlock_script("script.py")

    resp = client.patch("/scripts/script.py", data="test2.py")
    assert resp.status_code == HTTPStatus.NO_CONTENT
    assert not os.path.exists(script_path)

    script_path = os.path.join(server.SCRIPT_DIRECTORY, "test2.py")
    assert os.path.exists(script_path)
    with open(script_path, "r") as script:
        assert script.read() == "this is a test"


def test_update_script(client):

    script_path = os.path.join(server.SCRIPT_DIRECTORY, "script.py")

    resp = client.put("/scripts/wrongfile")
    assert resp.status_code == HTTPStatus.BAD_REQUEST

    resp = client.put("/scripts/script.py", data="this is a test")
    assert resp.status_code == HTTPStatus.CREATED
    with open(script_path, "r") as script:
        assert script.read() == "this is a test"

    resp = client.put("/scripts/script.py", data="this is another test")
    assert resp.status_code == HTTPStatus.NO_CONTENT
    with open(script_path, "r") as script:
        assert script.read() == "this is another test"

    server.lock_script("script.py")
    resp = client.put("/scripts/script.py", data="this is another another test")
    assert resp.status_code == HTTPStatus.LOCKED
    with open(script_path, "r") as script:
        assert script.read() == "this is another test"
    server.unlock_script("script.py")


def test_script_exists():

    open(os.path.join(server.SCRIPT_DIRECTORY, "script.py"), "w+").close
    assert not server.script_exists("wrong.py")
    assert server.script_exists("script.py")


def test_lock_script():

    lock_path = os.path.join(server.SCRIPT_DIRECTORY, "script.py.lock")
    assert server.lock_script("script.py")
    assert os.path.exists(lock_path)
    assert not server.lock_script("script.py")
    date = datetime(1997, 3, 7)
    with open(lock_path, "w") as lock:
        lock.write(date.isoformat())
    assert server.lock_script("script.py")


def test_unlock_script():

    server.unlock_script("script.py")
    lock_path = os.path.join(server.SCRIPT_DIRECTORY, "script.py.lock")
    open(lock_path, "w+").close()
    server.unlock_script("script.py")
    assert not os.path.exists(lock_path)


def test_get_settings_value():

    assert server.get_settings_value("test") == ""
    assert server.get_settings_value("test", "default") == "default"
    server.__settings["test"] = "not default"
    assert server.get_settings_value("test") == "not default"
    assert server.get_settings_value("test", "default") == "not default"


def test_set_settings_value():

    assert json.dumps(server.__settings) == "{}"
    assert not os.path.exists(server.SETTINGS_PATH)

    server.set_settings_value("test", "not default")
    assert json.dumps(server.__settings) == json.dumps({"test": "not default"})
    with open(server.SETTINGS_PATH, "r") as settings:
        assert settings.read() == json.dumps({"test": "not default"})


def test_is_script_name_valid():

    assert server.is_script_name_valid("test.py")
    assert not server.is_script_name_valid("test")
    assert not server.is_script_name_valid("test.lock")
    assert not server.is_script_name_valid("test.py.test")
    assert not server.is_script_name_valid("this is an invalid test")
    assert not server.is_script_name_valid("this is a \n invalid test")
    assert not server.is_script_name_valid("this is a invalid test.py")
