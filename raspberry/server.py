# -*- coding:utf-8 -*
import os
from werkzeug.exceptions import Locked, NotFound
from werkzeug.utils import secure_filename
from datetime import datetime
from flask import Flask, json, request, render_template
from http import HTTPStatus

app = Flask(__name__)

SCRIPT_DIRECTORY = "server_scripts"
STRATLIB_PATH = "raspberry/strategy/stratlib.py"
SETTINGS_PATH = "settings.json"
SCRIPT_TIMEOUT = 10000  # in microseconds

__settings = {}

if os.path.exists(SETTINGS_PATH):
    with open(SETTINGS_PATH, "r") as settings:
        __settings = json.loads(settings.read())

if not os.path.isdir(SCRIPT_DIRECTORY):
    os.mkdir(SCRIPT_DIRECTORY)


@app.route("/", methods=["GET"])
def serve_webview():
    return render_template("main.jinja")


@app.route("/stratlib", methods=["GET"])
def get_stratlib():

    if not os.path.exists(STRATLIB_PATH):
        raise NotFound

    with open(STRATLIB_PATH, "r") as stratlib:
        data = stratlib.read()

    return app.response_class(
        response=data, status=HTTPStatus.OK, mimetype="text/python"
    )


@app.route("/scripts", methods=["GET"])
def list_scripts():
    return app.response_class(
        response=json.dumps(
            [
                file
                for file in os.listdir(SCRIPT_DIRECTORY)
                if is_script_name_valid(file)
            ]
        ),
        status=HTTPStatus.OK,
        mimetype="application/json",
    )


@app.route("/scripts/selected", methods=["GET"])
def get_selected_script():
    selected = get_settings_value("selected_script", False)
    if not selected:
        return "", HTTPStatus.NO_CONTENT
    return app.response_class(
        response=selected, status=HTTPStatus.OK, mimetype="text/plain"
    )


@app.route("/scripts/<string:script_name>/select", methods=["POST"])
def select_script(script_name: str):

    if not is_script_name_valid(script_name):
        return "Invalid script name", HTTPStatus.BAD_REQUEST

    if not script_exists(script_name):
        raise NotFound

    set_settings_value("selected_script", script_name)

    return "", HTTPStatus.NO_CONTENT


@app.route("/scripts/<string:script_name>", methods=["GET"])
def get_script(script_name: str):

    if not is_script_name_valid(script_name):
        return "Invalid script name", HTTPStatus.BAD_REQUEST

    if not script_exists(script_name):
        raise NotFound

    if not lock_script(script_name):
        raise Locked

    with open(os.path.join(SCRIPT_DIRECTORY, script_name), "r") as script:
        data = script.read()

    unlock_script(script_name)
    return app.response_class(
        response=data, status=HTTPStatus.OK, mimetype="text/python"
    )


@app.route("/scripts/<string:script_name>", methods=["DELETE"])
def delete_script(script_name: str):

    if not is_script_name_valid(script_name):
        return "Invalid script name", HTTPStatus.BAD_REQUEST

    if not script_exists(script_name):
        raise NotFound

    if not lock_script(script_name):
        raise Locked

    os.remove(os.path.join(SCRIPT_DIRECTORY, script_name))

    if script_name == get_settings_value("selected_script"):
        set_settings_value("selected_script", "")

    unlock_script(script_name)
    return "", HTTPStatus.NO_CONTENT


@app.route("/scripts/<string:script_name>", methods=["PATCH"])
def rename_script(script_name: str):

    if not is_script_name_valid(script_name):
        return "Invalid script name", HTTPStatus.BAD_REQUEST

    if not script_exists(script_name):
        raise NotFound

    new_script_name = request.data.decode("utf-8")
    if len(new_script_name) <= 0:
        return "Request data can't be empty", HTTPStatus.BAD_REQUEST

    if not is_script_name_valid(new_script_name):
        return "Invalid destination name", HTTPStatus.BAD_REQUEST

    if not lock_script(script_name):
        raise Locked

    if script_name == get_settings_value("selected_script"):
        set_settings_value("selected_script", new_script_name)

    os.rename(
        os.path.join(SCRIPT_DIRECTORY, script_name),
        os.path.join(SCRIPT_DIRECTORY, new_script_name),
    )

    unlock_script(script_name)

    return "", HTTPStatus.NO_CONTENT


@app.route("/scripts/<string:script_name>", methods=["PUT"])
def update_script(script_name: str):
    def write_script(script_name: str):
        with open(os.path.join(SCRIPT_DIRECTORY, script_name), "w") as script:
            script.write(request.data.decode("utf-8"))

    if not is_script_name_valid(script_name):
        return "Invalid script name", HTTPStatus.BAD_REQUEST

    if not script_exists(script_name):
        write_script(script_name)
        return "", HTTPStatus.CREATED

    if not lock_script(script_name):
        raise Locked

    write_script(script_name)

    unlock_script(script_name)
    return "", HTTPStatus.NO_CONTENT


def script_exists(script_name: str) -> bool:

    if os.path.exists(os.path.join(SCRIPT_DIRECTORY, script_name)):
        return True
    return False


def lock_script(script_name: str) -> bool:
    def create_lock_file(script_lock_name: str):
        with open(script_lock_name, "w") as lock:
            lock.write(datetime.now().isoformat())

    def is_lock_file_expired(script_lock_name: str) -> bool:
        with open(script_lock_name, "r") as lock:
            date = datetime.fromisoformat(lock.readline())
        time_delta = datetime.now() - date
        return time_delta.microseconds >= get_settings_value(
            "script_timeout", SCRIPT_TIMEOUT
        )

    script_lock_path = os.path.join(SCRIPT_DIRECTORY, script_name + ".lock")

    if os.path.exists(script_lock_path) and not is_lock_file_expired(script_lock_path):
        return False

    create_lock_file(script_lock_path)
    return True


def unlock_script(script_name: str) -> None:
    if script_exists(script_name + ".lock"):
        os.remove(os.path.join(SCRIPT_DIRECTORY, script_name + ".lock"))


def get_settings_value(key: str, default_value: object = "") -> object:
    return __settings.get(key, default_value)


def set_settings_value(key: str, value: object) -> None:

    __settings[key] = value

    with open(SETTINGS_PATH, "w+") as settings:
        settings.write(json.dumps(__settings))


def is_script_name_valid(script_name: str) -> bool:
    if script_name != secure_filename(script_name):
        return False
    if script_name.split(".")[-1] != "py":
        return False
    return True


if __name__ == "__main__":
    from waitress import serve

    serve(app, port=5000)
