# API Communication

## Introduction

Pour créer une interface de communication :

```python
communication = Communication()
```

Cela va créer une transmission série par défaut, mais on peut la préciser comme l'on souhaite :

```python
serial = Serial("/dev/ttyUSB0", baudrate=9600, timeout=1)
communication = Communication(serial)

# ou bien :
communication = Communication(port="/dev/ttyUSB0", baudrate=9600, timeout=1)
```

## Envoyer / Recevoir

On peut maintenant envoyer des trames protobuf.
Elles se construisent comme ceci :

```python
request = Request()
request.method = GET
request.config.arg1 = 10
request.config.arg2 = 25
# ...
```

On peut ensuite les envoyer en utilisant :

```python
communication.send(1,request)
# Ici, 1 est l'adresse de la carte à qui la trame est destinée
```

Pour recevoir les trames, il faut appeler :

```python
communication.listen() # cette fonction est non-bloquante
```

Pour arrêter de recevoir les trames :

```python
communication.stop_listening()
```

## Consulter les logs

Toutes les trames reçues et envoyées sont stockés dans une pile, on peut effectuer diverses requêtes sur cette pile :

```python
communication.stack.get_first() # permet de récupérer la première trame envoyée ou reçue
communication.stack.get(4) # permet de récupérer la cinquième trame envoyée ou reçue
communication.stack.show(0, 7) # permet d'afficher dans la console les huit premières trames envoyées ou reçues
```

On peut également utiliser des filtres personnalisés pour récupérer les trames voulues. Quelques filtres d'exemples sont présent dans le fichier *filters.py*.

La fonction de filtre s'utilise comme suit :

```python
# Pour récupérer toutes les trames envoyés :
communication.stack.filter(filters.master_items) 

# Pour récupérer toues les trames envoyés/reçues entre 18h et 19h et les afficher dans la console
communication.stack.filter(filters.hour_items, show=True, hour=18)
```

On peut alors créer et utiliser ses propres filtres à l'aide de la méthode *filter()*. Ces filtres doivent prendre un *Item* de la pile et retourner *True* s'il doit être récupéré par le filtre et *False* sinon.

Ces filtres prennent la forme :

```python
def filter_func(item: Item, **kwargs: Any) -> bool:
```

## Utiliser un port virtuel (pour les tests)

On peut utiliser un port virtuel pour pouvoir simuler un envoi et une reception de trame sur la même machine.

Pour créer un port virtuel, on fait :

```python
communication = Communication(is_virtual=True)
```
