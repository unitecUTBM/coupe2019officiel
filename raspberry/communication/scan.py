from serial import Serial, SerialException
import argparse
import sys
import glob

from decoder import Decoder
from packet import Packet


def available_serial_ports():
    """ Lists serial port names

		:raises EnvironmentError:
			On unsupported or unknown platforms
		:returns:
			A list of the serial ports available on the system
	"""
    if sys.platform.startswith("win"):
        ports = ["COM%s" % (i + 1) for i in range(256)]
    elif sys.platform.startswith("linux") or sys.platform.startswith("cygwin"):
        # this excludes your current terminal "/dev/tty"
        ports = glob.glob("/dev/tty[A-Za-z]*")
    elif sys.platform.startswith("darwin"):
        ports = glob.glob("/dev/tty.*")
    else:
        raise EnvironmentError("Unsupported platform")

    result = []
    for port in ports:
        try:
            s = Serial(port)
            s.close()
            result.append(port)
        except (OSError, SerialException):
            pass
    return result


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="sniffer")
    parser.add_argument(
        "port",
        metavar="port",
        nargs="?",
        help="serial port, for exemple COM3 on windows or /dev/ttyUSB0 on linux",
    )
    parser.add_argument("-baudrate", type=int, default=115200, help="(default: 115200)")
    args = parser.parse_args()
    if args.port is None:
        print("available serial port:")
        print("\n".join(available_serial_ports()))
