import copy
import os
import select

from raspberry.communication.packet import Packet, Board


class Decoder:
    """ Classe permettant l'encodage et le décodage des paquets envoyés et reçus """

    decoder_instance = None

    start_byte = 0x55
    packet_length_max = 255

    def __new__(cls) -> "Decoder":
        if cls.decoder_instance is None:
            cls.decoder_instance = object.__new__(cls)
        return cls.decoder_instance

    def __init__(self, timeout: int = 1) -> None:
        self.packet = None
        self.reset()
        self.timeout = timeout

    def reset(self) -> None:
        # l'index utilisé pour décoder un paquet octet par octet
        self.index = -1
        self.packet = Packet()

    def decode(self, source: int) -> Packet:
        """
        Construit un paquet à partir d'une lecture sur le serial

        :param source: le file descriptor du serial à utiliser

        :return: le paquet construit
        """
        packet = None

        # Tant que le paquet n'a pas fini d'être construit
        while packet is None:
            # vérifie s'il y a des données à lire sur le serial
            res, _, _ = select.select([source], [], [], self.timeout)
            if res:
                # lit octet par octet et construit le paquet
                packet = self.append(os.read(source, 1))
            else:
                # s'il n'y a pas de données à recevoir d'ici la fin du timeout
                return None

        return packet

    def append(self, byte: bytes) -> Packet:
        """
        Construit un paquet octet par octet

        :param byte: le dernier octet lu sur le serial

        :return: le paquet s'il a terminé d'être construit, 
                  None dans tous les autres cas
        """
        try:
            intByte = ord(byte)
        except:
            return None
        if self.index < 0:
            if intByte == self.start_byte:
                self.index = 0
        else:
            if self.index == 0:
                self.packet.header_checksum = (intByte >> 4) & 0x0F
                self.packet.address = Board(intByte & 0x0F)
                self.index += 1
            elif self.index == 1:
                self.packet.length = intByte
                if self.packet.length > self.packet_length_max:
                    self.packet.error = True
                    res = copy.deepcopy(self.packet)
                    self.reset()
                    return res
                self.index += 1
            elif self.index == 2:
                # récupération du checksum du payload
                self.packet.payload_checksum = intByte
                # maintenant qu'on a reçu le checksum du payload on peut vérifier le checksum de l'en-tête
                self.check_header_checksum()
            elif self.index < (self.packet.length + 2):
                # récupération du payload
                self.packet.data.append(intByte)
                self.index += 1
            else:
                self.packet.data.append(intByte)
                self.check_payload_checksum()
                res = copy.deepcopy(self.packet)
                res.data = bytes(res.data)
                self.reset()
                return res
        return None

    def check_header_checksum(self):
        """
        Vérifie si le checksum de l'en-tête reçu correspond au checksum calculé par rapport au message reçu
        """
        real_header_checksum = self.packet.calculate_header_checksum()

        if real_header_checksum == self.packet.header_checksum:
            self.index += 1
        else:
            self.packet.error = True
            self.reset()

    def check_payload_checksum(self):
        """
        Vérifie si le checksum du payload reçu correspond au checksum calculé par rapport au payload reçu
        """
        # une fois le payload récupéré on peut vérifier le checksum du payload
        if self.packet.payload_checksum != ((~sum(self.packet.data)) + 1) & 0xFF:
            self.packet.error = True

    def encode(self, packet: Packet) -> bytearray:
        """
        Transforme un paquet en liste d'octet pour l'envoie sur le serial

        :param packet: le paquet à encoder

        :return: une liste d'octet représentant le paquet
        """
        encoded_packet = bytearray(b"")
        encoded_packet.append(self.start_byte)
        # ajoute l'adresse et le checksum de l'en-tête
        encoded_packet.append((packet.header_checksum << 4) | (packet.address & 0x0F))

        # ajoute la longueur du paquet
        encoded_packet.append(packet.length & 0xFF)
        # ajoute le checksum du payload
        encoded_packet.append(packet.payload_checksum)
        # ajoute les données (requête protobuf)
        encoded_packet.extend(packet.data)
        return encoded_packet
