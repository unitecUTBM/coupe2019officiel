from raspberry.communication.communication import Communication
from raspberry.communication.decoder import Decoder
from raspberry.communication.packet import Packet
from raspberry.communication.buffer_pb2 import *

from serial import Serial
import os, pty, time

import pytest


@pytest.fixture
def communication():
    return Communication(is_virtual=True)


def test_decoder_singleton(communication):
    """
    Vérifie le singleton du décodeur
    """
    assert communication.decoder == Decoder()


# tests unitaires
def test_virtual_send(communication):
    """
    Test d'envoi de packet
    """

    # Envoi d'une requête GET vide
    request = Request()
    request.method = GET

    packet = communication.send(1, request)

    # Vérifie le contenu du paquet
    assert packet.address == 1
    assert packet.error == False
    assert packet.length == 2
    assert packet.header_checksum == 7
    assert packet.payload_checksum == 247
    assert packet.data == b"\x08\x01"


def test_virtual_receive(communication):
    """
    Test d'envoi et de réception de paquet via le port virtuel
    """

    request = Request()
    request.method = GET

    packet = communication.send(1, request)

    communication.listen()

    time.sleep(1)

    communication.stop_listening()

    item = communication.stack.get_last()
    assert item.from_master == False
    assert item.request == request
    assert item.packet.address == packet.address
    assert item.packet.data == packet.data
    assert item.packet.length == packet.length
    assert item.packet.header_checksum == packet.header_checksum
    assert item.packet.payload_checksum == packet.payload_checksum
    assert item.packet.error == False


def test_encode():
    communication = Communication(is_virtual=True)

    packet = Packet(1, bytearray(b"\x08\x01"))
    encoding = communication.decoder.encode(packet)

    assert encoding == bytearray(b"Uq\x02\xf7\x08\x01")


def test_decode():
    communication = Communication(is_virtual=True)
