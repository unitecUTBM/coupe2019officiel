import os
import time
import pty

from serial import Serial, rs485
from threading import Thread
from typing import NewType

from raspberry.communication.stack import Stack
from raspberry.communication.packet import Packet
from raspberry.communication.decoder import Decoder

import raspberry.communication.filters as filters

from raspberry.communication.buffer_pb2 import Request


class Communication:
    """ Classe permettant l'envoi et la reception de trames """

    def __init__(
        self,
        serial: Serial = None,
        port: str = "/dev/ttyUSB0",
        baudrate: int = 67500,
        timeout: int = 1,
        is_virtual: bool = False,
        forRS485: bool = False,
        debug: bool = False,
    ) -> None:
        self.stack = Stack()
        self.listener_thread = Thread(target=self.receiver)

        self.listening = False

        self.debug = debug

        self.decoder = Decoder()

        self.serial = None

        if is_virtual:
            master, slave = pty.openpty()
            self.port = os.ttyname(slave)
            self.serial_source = master
        else:
            self.port = port

        if serial is None:
            # si aucun serial n'a été passé en argument, crée en un
            if forRS485:
                self.serial = rs485.RS485(self.port, baudrate=baudrate, timeout=timeout)
                self.serial.rs485_mode = rs485.RS485Settings()
            else:
                self.serial = Serial(self.port, baudrate=baudrate, timeout=timeout)
        else:
            self.serial = serial

        # Récupère le file descriptor du serial
        if not is_virtual:
            self.serial_source = self.serial.fileno()

    def send(self, address: int, request: Request) -> Packet:
        """
        Envoie la requête protobuf spécifiée à l'adresse spécifiée 

        :param address: l'adresse de la carte à qui la trame est destinée
        :param request: la requête protobuf à envoyer

        :return: le paquet qui a été envoyé
        """
        packet = Packet(address, request.SerializeToString())

        encoded_packet = self.decoder.encode(packet)

        if self.debug:
            print("send:")
            print("  packet: {}".format(packet))
            print(" request: {}".format(request))

        self.stack.add(packet, request, True)
        self.serial.write(encoded_packet)

        return packet

    def listen(self) -> None:
        """ Récupère et enregistre dans la pile toutes les paquets destinés à la raspberry """
        if self.listening:
            self.stop_listening()

        self.listening = True
        self.listener_thread.start()

    def stop_listening(self) -> None:
        """ Arrête d'enregistrer dans la pile les trames destinées à la raspberry """
        self.listening = False
        self.listener_thread.join()
        self.listener_thread = Thread(target=self.receiver)

    def receiver(self) -> None:
        """ Fonction lancé par le thread d'écoute des trames """
        while self.listening:
            self.serial.flushInput()
            self.receive()

    def receive(self) -> Packet:
        """
        Récupère la trame destinée à la raspberry et l'enregistre dans la pile

        :return: le paquet résultant de la trame reçue
        """
        request = None

        packet = self.decoder.decode(self.serial_source)

        if packet is not None:
            request = Request()
            request.ParseFromString(packet.data)
            if self.debug:
                print("result:")
                print("  packet: {}".format(packet))
                print(" request: {}".format(request))

            self.stack.add(packet, request, False)
        else:
            if self.debug:
                print("timeout")

        return packet

    def test(self):
        # petit test qui affiche dans la console les trames reçues et envoyées dans les 5 dernières minutes
        for item in self.stack.filter(filters.lastly_added_items, show=True, minutes=5):
            pass
