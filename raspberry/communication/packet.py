from enum import IntEnum


class Board(IntEnum):
    MOTOR = 1
    SENSOR = 2
    PUMP = 3


class Packet:
    """ Classe représentant un paquet à envoyer sur le bus """

    def __init__(self, address: IntEnum = 0x00, data_arg: bytearray = None) -> None:
        # ajouté ça car les arguments par défaut sont mutables
        if data_arg is None:
            data_arg = bytearray()

        self.address = address
        self.length = len(data_arg)
        self.data = data_arg
        self.payload_checksum = ((~sum(self.data)) + 1) & 0xFF
        self.error = False
        self.header_checksum = self.calculate_header_checksum()

    def calculate_header_checksum(self) -> int:
        return (
            (
                ~sum(
                    [
                        self.address & 0x0F,
                        self.length & 0x0F,
                        self.length >> 4,
                        self.payload_checksum & 0x0F,
                        self.payload_checksum >> 4,
                    ]
                )
            )
            + 1
        ) & 0x0F

    def __str__(self) -> str:
        return (
            "{address: %s, header_checksum: %s, length: %s, payload_checksum: %s, data: %s,  error: %s}"
            % (
                self.address,
                self.header_checksum,
                self.length,
                self.payload_checksum,
                self.data,
                self.error,
            )
        )
