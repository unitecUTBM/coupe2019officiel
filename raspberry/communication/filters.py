from raspberry.communication.item import Item
from typing import Any
from datetime import datetime, timedelta

# La méthode "filter" de la classe "Stack" permet de récupérer des items précis de la pile
# suivant un pattern donné par une fonction

# Dans ce fichier se trouve quelques exemples de fonctions de filtre utilisable par cette méthode

# Exemple d'utilisation :
#
# stack = Stack()
# ...
# stack.filter(hour_items,hour=17)


# Note : Toutes les fonctions doivent avoir comme paramètres : (item, **kwargs)
#        Elles retournent un booléen précisant si oui ou non l'item doit être renvoyé par le filtre
#        **kwargs est pas obligatoire mais c'est mieux


def master_items(item: Item, **kwargs: Any) -> bool:
    """ Filtre les éléments envoyés par le maître """
    return item.from_master


def slave_items(item: Item, **kwargs: Any) -> bool:
    """ Filtre les éléments envoyés par les esclaves """
    return not item.from_master


def hour_items(item: Item, **kwargs: int) -> bool:
    """ Filtre les éléments ajoutés à la liste pendant l'heure spécifiée
        Nécessite un argument 'hour' (entier entre 0 et 23)
    """
    hour = kwargs.get("hour", 17)
    return item.log_time.hour == hour


def correct_items(item: Item, **kwargs: Any) -> bool:
    """  Filtre les éléments ayant un paquet sans erreurs """
    return not item.packet.error


def error_items(item: Item, **kwargs: Any) -> bool:
    """  Filtre les éléments ayant un paquet avec erreurs """
    return item.packet.error


def lastly_added_items(item: Item, **kwargs: int) -> bool:
    """ Filtre les éléments ajoutés à la liste pendant les dernières minutes
        Nécessite un argument 'minutes' (entier)
    """
    minutes = kwargs.get("minutes", 5)
    return abs(datetime.now() - item.log_time) <= timedelta(minutes=minutes)
