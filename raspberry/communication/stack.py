from raspberry.communication.item import Item
from raspberry.communication.packet import Packet
from typing import List, Callable, Any


class Stack:
    """ Classe représentant la pile des trames envoyées et reçues """

    def __init__(self) -> None:
        self.empty()

    def __getitem__(self, value: slice) -> Item:
        """ Surcharge de l'opérateur [] """
        return self.items[value]

    def empty(self) -> None:
        """ Vide la pile """
        self.items = []

    def add(self, packet: Packet, data, is_from_master: bool) -> None:
        """ Ajoute un élément à la pile """
        self.items.append(Item(packet, data, is_from_master))

    def get(self, index: int) -> Item:
        """ Retourne l'élément à l'index spécifié dans la pile """
        return self[index]

    def get_last(self) -> Item:
        """ Retourne le dernier élément ajouté à la pile """
        return self[-1]

    def get_first(self) -> Item:
        """ Retourne le premier élément ajouté à la pile """
        return self[0]

    def get_range(self, start_index: int = 0, end_index: int = -1) -> List[Item]:
        """ Récupère tous les éléments de la pile entre les deux indexes spécifiés en paramètre """
        return self[start_index:end_index]

    def show(self, start_index: int = 0, end_index: int = -1) -> None:
        """ Print all elements between specified indexes """
        if end_index == -1:
            end_index = len(self.items)
        for i in range(start_index, end_index):
            print(self.items[i])

    def filter(
        self,
        filter_func: Callable[[Item, Any], bool],
        show: bool = False,
        **kwargs: Any
    ) -> List[Item]:
        """
        Retourne tous les éléments dans la pile correspondant au filtre spécifié

        :param filter_func: Une foncton qui prend un élément en paramètre
                            et retourne True si l'élément doit être gardé
        :param show: Si l'on doit ou non afficher les résultats à l'écran
        :param kwargs: Les arguments supplémentaires de la fonction passée en paramètre (s'il y en a)
        :return: la liste d'élément filtré
        """
        res = []
        for item in self.items:
            if filter_func(item, **kwargs):
                res.append(item)
                if show:
                    print(item)
        return res
