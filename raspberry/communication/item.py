from datetime import datetime
from raspberry.communication.packet import Packet

from raspberry.communication.buffer_pb2 import Request


class Item:
    """ Classe représentant un élément de la pile des trames """

    def __init__(self, packet: Packet, request: Request, from_master: bool) -> None:
        self.log_time = datetime.now()
        self.packet = packet
        self.request = request
        self.from_master = from_master

    def __str__(self) -> str:
        return "[%s] %s : %s" % (
            self.format_log_time(),
            "M" if self.from_master else "S",
            self.packet,
        )

    def format_log_time(self) -> str:
        """ Retourne une chaîne de caractère représentant l'heure de reception/envoi du paquet """
        return self.log_time.strftime("%H:%M:%S.%f")
