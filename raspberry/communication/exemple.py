from raspberry.communication.communication import Communication
from raspberry.communication.buffer_pb2 import *

import argparse
import time

from serial import Serial, rs485

import os

if __name__ == "__main__":
    # Permet de spécifier des arguments pour tester en ligne de commande
    parser = argparse.ArgumentParser(description="La description")
    parser.add_argument(
        "port",
        metavar="port",
        nargs="?",
        help='serial port, par exemple "COM3" sur windows or "/dev/ttyUSB0" sur linux, "virtual" pour les tests',
    )
    parser.add_argument("-baudrate", type=int, default=57600, help="(default: 57600)")
    parser.add_argument("-timeout", type=int, default=1, help="(default: 1)")
    args = parser.parse_args()

    # Vérifie si un port a été spécifié
    if args.port is None:
        port = "/dev/ttyUSB0"
    else:
        port = args.port

    print("using port %s" % port)
    print("baudrate : %s" % args.baudrate)

    # Attendre un petit peu l'initialisation du côté de la carte
    # TODO : Faire une fonction pour attendre l'initialisation
    time.sleep(1)

    if args.port == "virtual":
        # Instanciation pour utiliser l'API dans le cas d'un port virtuel
        communication = Communication(is_virtual=True, debug=True)
    else:
        # Instanciation pour utiliser l'API
        communication = Communication(
            port=port,
            baudrate=args.baudrate,
            timeout=args.timeout,
            forRS485=True,
            debug=True,
        )

    # configuration de la requête protobuf
    request = Request()
    request.method = GET
    request.config.width = 0
    request.config.kp_position = 0
    request.config.ki_position = 0
    request.config.kd_position = 0
    request.config.kp_angle = 0
    request.config.ki_angle = 0
    request.config.kd_angle = 0

    # Envoie de la requête avec l'adresse 1
    communication.send(1, request)

    # Récupère dans la pile toutes les trames reçues pendant 3 secondes
    communication.listen()
    time.sleep(3)
    communication.stop_listening()

    # Fonction pour tester diverses fonctionnalités
    communication.test()
