<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.3.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="16" fill="1" visible="no" active="no"/>
<layer number="3" name="Route3" color="17" fill="1" visible="no" active="no"/>
<layer number="4" name="Route4" color="18" fill="1" visible="no" active="no"/>
<layer number="5" name="Route5" color="19" fill="1" visible="no" active="no"/>
<layer number="6" name="Route6" color="25" fill="1" visible="no" active="no"/>
<layer number="7" name="Route7" color="26" fill="1" visible="no" active="no"/>
<layer number="8" name="Route8" color="27" fill="1" visible="no" active="no"/>
<layer number="9" name="Route9" color="28" fill="1" visible="no" active="no"/>
<layer number="10" name="Route10" color="29" fill="1" visible="no" active="no"/>
<layer number="11" name="Route11" color="30" fill="1" visible="no" active="no"/>
<layer number="12" name="Route12" color="20" fill="1" visible="no" active="no"/>
<layer number="13" name="Route13" color="21" fill="1" visible="no" active="no"/>
<layer number="14" name="Route14" color="22" fill="1" visible="no" active="no"/>
<layer number="15" name="Route15" color="23" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="24" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="16" fill="1" visible="yes" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="tplace-old" color="10" fill="1" visible="yes" active="yes"/>
<layer number="109" name="ref-old" color="11" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="4" fill="1" visible="yes" active="yes"/>
<layer number="114" name="Badge_Outline" color="7" fill="1" visible="no" active="yes"/>
<layer number="115" name="ReferenceISLANDS" color="7" fill="1" visible="no" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="untiec">
<packages>
<package name="QFP80P1200X1200X120-44" urn="urn:adsk.eagle:footprint:7630333/1">
<description>44-QFP, 0.80 mm pitch, 12.00 mm span, 10.00 X 10.00 X 1.20 mm body
&lt;p&gt;44-pin QFP package with 0.80 mm pitch, 12.00 mm lead span1 X 12.00 mm lead span2 with body size 10.00 X 10.00 X 1.20 mm&lt;/p&gt;</description>
<circle x="-5.703" y="4.7775" radius="0.25" width="0" layer="21"/>
<wire x1="-5" y1="4.5275" x2="-5" y2="5" width="0.12" layer="21"/>
<wire x1="-5" y1="5" x2="-4.5275" y2="5" width="0.12" layer="21"/>
<wire x1="5" y1="4.5275" x2="5" y2="5" width="0.12" layer="21"/>
<wire x1="5" y1="5" x2="4.5275" y2="5" width="0.12" layer="21"/>
<wire x1="5" y1="-4.5275" x2="5" y2="-5" width="0.12" layer="21"/>
<wire x1="5" y1="-5" x2="4.5275" y2="-5" width="0.12" layer="21"/>
<wire x1="-5" y1="-4.5275" x2="-5" y2="-5" width="0.12" layer="21"/>
<wire x1="-5" y1="-5" x2="-4.5275" y2="-5" width="0.12" layer="21"/>
<wire x1="5" y1="-5" x2="-5" y2="-5" width="0.12" layer="51"/>
<wire x1="-5" y1="-5" x2="-5" y2="5" width="0.12" layer="51"/>
<wire x1="-5" y1="5" x2="5" y2="5" width="0.12" layer="51"/>
<wire x1="5" y1="5" x2="5" y2="-5" width="0.12" layer="51"/>
<smd name="1" x="-5.6713" y="4" dx="1.4692" dy="0.5471" layer="1"/>
<smd name="2" x="-5.6713" y="3.2" dx="1.4692" dy="0.5471" layer="1"/>
<smd name="3" x="-5.6713" y="2.4" dx="1.4692" dy="0.5471" layer="1"/>
<smd name="4" x="-5.6713" y="1.6" dx="1.4692" dy="0.5471" layer="1"/>
<smd name="5" x="-5.6713" y="0.8" dx="1.4692" dy="0.5471" layer="1"/>
<smd name="6" x="-5.6713" y="0" dx="1.4692" dy="0.5471" layer="1"/>
<smd name="7" x="-5.6713" y="-0.8" dx="1.4692" dy="0.5471" layer="1"/>
<smd name="8" x="-5.6713" y="-1.6" dx="1.4692" dy="0.5471" layer="1"/>
<smd name="9" x="-5.6713" y="-2.4" dx="1.4692" dy="0.5471" layer="1"/>
<smd name="10" x="-5.6713" y="-3.2" dx="1.4692" dy="0.5471" layer="1"/>
<smd name="11" x="-5.6713" y="-4" dx="1.4692" dy="0.5471" layer="1"/>
<smd name="12" x="-4" y="-5.6713" dx="1.4692" dy="0.5471" layer="1" rot="R90"/>
<smd name="13" x="-3.2" y="-5.6713" dx="1.4692" dy="0.5471" layer="1" rot="R90"/>
<smd name="14" x="-2.4" y="-5.6713" dx="1.4692" dy="0.5471" layer="1" rot="R90"/>
<smd name="15" x="-1.6" y="-5.6713" dx="1.4692" dy="0.5471" layer="1" rot="R90"/>
<smd name="16" x="-0.8" y="-5.6713" dx="1.4692" dy="0.5471" layer="1" rot="R90"/>
<smd name="17" x="0" y="-5.6713" dx="1.4692" dy="0.5471" layer="1" rot="R90"/>
<smd name="18" x="0.8" y="-5.6713" dx="1.4692" dy="0.5471" layer="1" rot="R90"/>
<smd name="19" x="1.6" y="-5.6713" dx="1.4692" dy="0.5471" layer="1" rot="R90"/>
<smd name="20" x="2.4" y="-5.6713" dx="1.4692" dy="0.5471" layer="1" rot="R90"/>
<smd name="21" x="3.2" y="-5.6713" dx="1.4692" dy="0.5471" layer="1" rot="R90"/>
<smd name="22" x="4" y="-5.6713" dx="1.4692" dy="0.5471" layer="1" rot="R90"/>
<smd name="23" x="5.6713" y="-4" dx="1.4692" dy="0.5471" layer="1"/>
<smd name="24" x="5.6713" y="-3.2" dx="1.4692" dy="0.5471" layer="1"/>
<smd name="25" x="5.6713" y="-2.4" dx="1.4692" dy="0.5471" layer="1"/>
<smd name="26" x="5.6713" y="-1.6" dx="1.4692" dy="0.5471" layer="1"/>
<smd name="27" x="5.6713" y="-0.8" dx="1.4692" dy="0.5471" layer="1"/>
<smd name="28" x="5.6713" y="0" dx="1.4692" dy="0.5471" layer="1"/>
<smd name="29" x="5.6713" y="0.8" dx="1.4692" dy="0.5471" layer="1"/>
<smd name="30" x="5.6713" y="1.6" dx="1.4692" dy="0.5471" layer="1"/>
<smd name="31" x="5.6713" y="2.4" dx="1.4692" dy="0.5471" layer="1"/>
<smd name="32" x="5.6713" y="3.2" dx="1.4692" dy="0.5471" layer="1"/>
<smd name="33" x="5.6713" y="4" dx="1.4692" dy="0.5471" layer="1"/>
<smd name="34" x="4" y="5.6713" dx="1.4692" dy="0.5471" layer="1" rot="R90"/>
<smd name="35" x="3.2" y="5.6713" dx="1.4692" dy="0.5471" layer="1" rot="R90"/>
<smd name="36" x="2.4" y="5.6713" dx="1.4692" dy="0.5471" layer="1" rot="R90"/>
<smd name="37" x="1.6" y="5.6713" dx="1.4692" dy="0.5471" layer="1" rot="R90"/>
<smd name="38" x="0.8" y="5.6713" dx="1.4692" dy="0.5471" layer="1" rot="R90"/>
<smd name="39" x="0" y="5.6713" dx="1.4692" dy="0.5471" layer="1" rot="R90"/>
<smd name="40" x="-0.8" y="5.6713" dx="1.4692" dy="0.5471" layer="1" rot="R90"/>
<smd name="41" x="-1.6" y="5.6713" dx="1.4692" dy="0.5471" layer="1" rot="R90"/>
<smd name="42" x="-2.4" y="5.6713" dx="1.4692" dy="0.5471" layer="1" rot="R90"/>
<smd name="43" x="-3.2" y="5.6713" dx="1.4692" dy="0.5471" layer="1" rot="R90"/>
<smd name="44" x="-4" y="5.6713" dx="1.4692" dy="0.5471" layer="1" rot="R90"/>
<text x="0" y="7.0409" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-7.0409" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="HDRVR8W64P254_2X4_1016X508X254B" urn="urn:adsk.eagle:footprint:7643368/1" locally_modified="yes">
<description>Double-row, 8-pin Receptacle Header (Female) Straight, 2.54 mm (0.10 in) row pitch, 2.54 mm (0.10 in) col pitch, 2.54 mm insulator length, 10.16 X 5.08 X 2.54 mm body
&lt;p&gt;Double-row (2X4), 8-pin Receptacle Header (Female) Straight package with 2.54 mm (0.10 in) row pitch, 2.54 mm (0.10 in) col pitch, 0.64 mm lead width, 3.00 mm tail length and 2.54 mm insulator length with overall size 10.16 X 5.08 X 2.54 mm, pin pattern - clockwise from top left&lt;/p&gt;</description>
<text x="0" y="13.159" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-13.445" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
<pad name="P$1" x="-12" y="10" drill="0.8" diameter="1.4224"/>
<pad name="P$2" x="-12" y="8" drill="0.8" diameter="1.4224"/>
<pad name="P$3" x="-12" y="6" drill="0.8" diameter="1.4224"/>
<pad name="P$4" x="-12" y="4" drill="0.8" diameter="1.4224"/>
<pad name="P$5" x="-12" y="2" drill="0.8" diameter="1.4224"/>
<pad name="P$6" x="-12" y="0" drill="0.8" diameter="1.4224"/>
<pad name="P$7" x="-12" y="-2" drill="0.8" diameter="1.4224"/>
<pad name="P$8" x="-12" y="-4" drill="0.8" diameter="1.4224"/>
<pad name="P$9" x="-12" y="-6" drill="0.8" diameter="1.4224"/>
<pad name="P$10" x="-12" y="-8" drill="0.8" diameter="1.4224"/>
<pad name="P$11" x="-12" y="-10" drill="0.8" diameter="1.4224"/>
<pad name="P$12" x="-10" y="-12" drill="0.8" diameter="1.4224"/>
<pad name="P$13" x="-8" y="-12" drill="0.8" diameter="1.4224"/>
<pad name="P$14" x="-6" y="-12" drill="0.8" diameter="1.4224"/>
<pad name="P$15" x="-4" y="-12" drill="0.8" diameter="1.4224"/>
<pad name="P$16" x="-2" y="-12" drill="0.8" diameter="1.4224"/>
<pad name="P$17" x="0" y="-12" drill="0.8" diameter="1.4224"/>
<pad name="P$18" x="2" y="-12" drill="0.8" diameter="1.4224"/>
<pad name="P$19" x="4" y="-12" drill="0.8" diameter="1.4224"/>
<pad name="P$20" x="6" y="-12" drill="0.8" diameter="1.4224"/>
<pad name="P$21" x="8" y="-12" drill="0.8" diameter="1.4224"/>
<pad name="P$22" x="10" y="-12" drill="0.8" diameter="1.4224"/>
<pad name="P$23" x="12" y="-10" drill="0.8" diameter="1.4224"/>
<pad name="P$24" x="12" y="-8" drill="0.8" diameter="1.4224"/>
<pad name="P$25" x="12" y="-6" drill="0.8" diameter="1.4224"/>
<pad name="P$26" x="12" y="-4" drill="0.8" diameter="1.4224"/>
<pad name="P$27" x="12" y="-2" drill="0.8" diameter="1.4224"/>
<pad name="P$28" x="12" y="0" drill="0.8" diameter="1.4224"/>
<pad name="P$29" x="12" y="2" drill="0.8" diameter="1.4224"/>
<pad name="P$30" x="12" y="4" drill="0.8" diameter="1.4224"/>
<pad name="P$31" x="12" y="6" drill="0.8" diameter="1.4224"/>
<pad name="P$32" x="12" y="8" drill="0.8" diameter="1.4224"/>
<pad name="P$33" x="12" y="10" drill="0.8" diameter="1.4224"/>
<pad name="P$34" x="10" y="12" drill="0.8" diameter="1.4224"/>
<pad name="P$35" x="8" y="12" drill="0.8" diameter="1.4224"/>
<pad name="P$36" x="6" y="12" drill="0.8" diameter="1.4224"/>
<pad name="P$37" x="4" y="12" drill="0.8" diameter="1.4224"/>
<pad name="P$38" x="2" y="12" drill="0.8" diameter="1.4224"/>
<pad name="P$39" x="0" y="12" drill="0.8" diameter="1.4224"/>
<pad name="P$40" x="-2" y="12" drill="0.8" diameter="1.4224"/>
<pad name="P$41" x="-4" y="12" drill="0.8" diameter="1.4224"/>
<pad name="P$42" x="-6" y="12" drill="0.8" diameter="1.4224"/>
<pad name="P$43" x="-8" y="12" drill="0.8" diameter="1.4224"/>
<pad name="P$44" x="-10" y="12" drill="0.8" diameter="1.4224"/>
<wire x1="-11" y1="-13" x2="-13" y2="-11" width="0.127" layer="21"/>
<wire x1="-13" y1="-11" x2="-13" y2="11" width="0.127" layer="21"/>
<wire x1="-13" y1="11" x2="-11" y2="13" width="0.127" layer="21"/>
<wire x1="-11" y1="13" x2="11" y2="13" width="0.127" layer="21"/>
<wire x1="11" y1="13" x2="13" y2="11" width="0.127" layer="21"/>
<wire x1="13" y1="11" x2="13" y2="-11" width="0.127" layer="21"/>
<wire x1="13" y1="-11" x2="11" y2="-13" width="0.127" layer="21"/>
<wire x1="11" y1="-13" x2="-11" y2="-13" width="0.127" layer="21"/>
</package>
<package name="SOP65P780X200-28" urn="urn:adsk.eagle:footprint:7639994/1">
<description>28-SOP, 0.65 mm pitch, 7.80 mm span, 10.20 X 5.30 X 2.00 mm body
&lt;p&gt;28-pin SOP package with 0.65 mm pitch, 7.80 mm span with body size 10.20 X 5.30 X 2.00 mm&lt;/p&gt;</description>
<circle x="-3.6269" y="4.9602" radius="0.25" width="0" layer="21"/>
<wire x1="-2.8" y1="4.7102" x2="-2.8" y2="5.25" width="0.12" layer="21"/>
<wire x1="-2.8" y1="5.25" x2="2.8" y2="5.25" width="0.12" layer="21"/>
<wire x1="2.8" y1="5.25" x2="2.8" y2="4.7102" width="0.12" layer="21"/>
<wire x1="-2.8" y1="-4.7102" x2="-2.8" y2="-5.25" width="0.12" layer="21"/>
<wire x1="-2.8" y1="-5.25" x2="2.8" y2="-5.25" width="0.12" layer="21"/>
<wire x1="2.8" y1="-5.25" x2="2.8" y2="-4.7102" width="0.12" layer="21"/>
<wire x1="2.8" y1="-5.25" x2="-2.8" y2="-5.25" width="0.12" layer="51"/>
<wire x1="-2.8" y1="-5.25" x2="-2.8" y2="5.25" width="0.12" layer="51"/>
<wire x1="-2.8" y1="5.25" x2="2.8" y2="5.25" width="0.12" layer="51"/>
<wire x1="2.8" y1="5.25" x2="2.8" y2="-5.25" width="0.12" layer="51"/>
<smd name="1" x="-3.6029" y="4.225" dx="1.702" dy="0.4624" layer="1"/>
<smd name="2" x="-3.6029" y="3.575" dx="1.702" dy="0.4624" layer="1"/>
<smd name="3" x="-3.6029" y="2.925" dx="1.702" dy="0.4624" layer="1"/>
<smd name="4" x="-3.6029" y="2.275" dx="1.702" dy="0.4624" layer="1"/>
<smd name="5" x="-3.6029" y="1.625" dx="1.702" dy="0.4624" layer="1"/>
<smd name="6" x="-3.6029" y="0.975" dx="1.702" dy="0.4624" layer="1"/>
<smd name="7" x="-3.6029" y="0.325" dx="1.702" dy="0.4624" layer="1"/>
<smd name="8" x="-3.6029" y="-0.325" dx="1.702" dy="0.4624" layer="1"/>
<smd name="9" x="-3.6029" y="-0.975" dx="1.702" dy="0.4624" layer="1"/>
<smd name="10" x="-3.6029" y="-1.625" dx="1.702" dy="0.4624" layer="1"/>
<smd name="11" x="-3.6029" y="-2.275" dx="1.702" dy="0.4624" layer="1"/>
<smd name="12" x="-3.6029" y="-2.925" dx="1.702" dy="0.4624" layer="1"/>
<smd name="13" x="-3.6029" y="-3.575" dx="1.702" dy="0.4624" layer="1"/>
<smd name="14" x="-3.6029" y="-4.225" dx="1.702" dy="0.4624" layer="1"/>
<smd name="15" x="3.6029" y="-4.225" dx="1.702" dy="0.4624" layer="1"/>
<smd name="16" x="3.6029" y="-3.575" dx="1.702" dy="0.4624" layer="1"/>
<smd name="17" x="3.6029" y="-2.925" dx="1.702" dy="0.4624" layer="1"/>
<smd name="18" x="3.6029" y="-2.275" dx="1.702" dy="0.4624" layer="1"/>
<smd name="19" x="3.6029" y="-1.625" dx="1.702" dy="0.4624" layer="1"/>
<smd name="20" x="3.6029" y="-0.975" dx="1.702" dy="0.4624" layer="1"/>
<smd name="21" x="3.6029" y="-0.325" dx="1.702" dy="0.4624" layer="1"/>
<smd name="22" x="3.6029" y="0.325" dx="1.702" dy="0.4624" layer="1"/>
<smd name="23" x="3.6029" y="0.975" dx="1.702" dy="0.4624" layer="1"/>
<smd name="24" x="3.6029" y="1.625" dx="1.702" dy="0.4624" layer="1"/>
<smd name="25" x="3.6029" y="2.275" dx="1.702" dy="0.4624" layer="1"/>
<smd name="26" x="3.6029" y="2.925" dx="1.702" dy="0.4624" layer="1"/>
<smd name="27" x="3.6029" y="3.575" dx="1.702" dy="0.4624" layer="1"/>
<smd name="28" x="3.6029" y="4.225" dx="1.702" dy="0.4624" layer="1"/>
<text x="0" y="5.885" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-5.885" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="FUSC3216X50" urn="urn:adsk.eagle:footprint:7657277/1">
<description>Chip, 3.20 X 1.60 X 0.50 mm body
&lt;p&gt;Chip package with body size 3.20 X 1.60 X 0.50 mm&lt;/p&gt;</description>
<wire x1="1.6" y1="1.1699" x2="-1.6" y2="1.1699" width="0.12" layer="21"/>
<wire x1="1.6" y1="-1.1699" x2="-1.6" y2="-1.1699" width="0.12" layer="21"/>
<wire x1="1.6" y1="-0.8" x2="-1.6" y2="-0.8" width="0.12" layer="51"/>
<wire x1="-1.6" y1="-0.8" x2="-1.6" y2="0.8" width="0.12" layer="51"/>
<wire x1="-1.6" y1="0.8" x2="1.6" y2="0.8" width="0.12" layer="51"/>
<wire x1="1.6" y1="0.8" x2="1.6" y2="-0.8" width="0.12" layer="51"/>
<smd name="1" x="-1.525" y="0" dx="0.9618" dy="1.7118" layer="1"/>
<smd name="2" x="1.525" y="0" dx="0.9618" dy="1.7118" layer="1"/>
<text x="0" y="1.8049" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.8049" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="HDRV2W100P508_1X2_1140X830X838B" urn="urn:adsk.eagle:footprint:7642728/1">
<description>Single-row, 2-pin Pin Header (Male) Straight, 5.08 mm (0.20 in) col pitch, 5.84 mm mating length, 11.40 X 8.30 X 8.38 mm body
&lt;p&gt;Single-row (1X2), 2-pin Pin Header (Male) Straight package with 5.08 mm (0.20 in) col pitch, 1.00 mm lead width, 3.00 mm tail length and 5.84 mm mating length with overall size 11.40 X 8.30 X 8.38 mm, pin pattern - clockwise from top left&lt;/p&gt;</description>
<circle x="0" y="4.654" radius="0.25" width="0" layer="21"/>
<wire x1="8.24" y1="-4.15" x2="-3.16" y2="-4.15" width="0.12" layer="21"/>
<wire x1="-3.16" y1="-4.15" x2="-3.16" y2="4.15" width="0.12" layer="21"/>
<wire x1="-3.16" y1="4.15" x2="8.24" y2="4.15" width="0.12" layer="21"/>
<wire x1="8.24" y1="4.15" x2="8.24" y2="-4.15" width="0.12" layer="21"/>
<wire x1="8.24" y1="-4.15" x2="-3.16" y2="-4.15" width="0.12" layer="51"/>
<wire x1="-3.16" y1="-4.15" x2="-3.16" y2="4.15" width="0.12" layer="51"/>
<wire x1="-3.16" y1="4.15" x2="8.24" y2="4.15" width="0.12" layer="51"/>
<wire x1="8.24" y1="4.15" x2="8.24" y2="-4.15" width="0.12" layer="51"/>
<pad name="1" x="0" y="0" drill="1.6142" diameter="4.0355"/>
<pad name="2" x="5.08" y="0" drill="1.6142" diameter="4.0355"/>
<text x="0" y="5.539" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-4.785" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="HDRV2W64P254_1X2_508X254X762B" urn="urn:adsk.eagle:footprint:7642736/1" locally_modified="yes">
<description>Single-row, 2-pin Pin Header (Male) Straight, 2.54 mm (0.10 in) col pitch, 5.08 mm mating length, 5.08 X 2.54 X 7.62 mm body
&lt;p&gt;Single-row (1X2), 2-pin Pin Header (Male) Straight package with 2.54 mm (0.10 in) col pitch, 0.64 mm lead width, 3.00 mm tail length and 5.08 mm mating length with overall size 5.08 X 2.54 X 7.62 mm, pin pattern - clockwise from top left&lt;/p&gt;</description>
<wire x1="3.9007" y1="-1.3591" x2="-1.3607" y2="-1.3591" width="0.12" layer="21"/>
<wire x1="-1.3607" y1="-1.3591" x2="-1.3607" y2="1.3591" width="0.12" layer="21"/>
<wire x1="-1.3607" y1="1.3591" x2="3.9007" y2="1.3591" width="0.12" layer="21"/>
<wire x1="3.9007" y1="1.3591" x2="3.9007" y2="-1.3591" width="0.12" layer="21"/>
<wire x1="3.81" y1="-1.27" x2="-1.27" y2="-1.27" width="0.12" layer="51"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="1.27" width="0.12" layer="51"/>
<wire x1="-1.27" y1="1.27" x2="3.81" y2="1.27" width="0.12" layer="51"/>
<wire x1="3.81" y1="1.27" x2="3.81" y2="-1.27" width="0.12" layer="51"/>
<pad name="1" x="0" y="0" drill="1.1051" diameter="2.2102"/>
<pad name="2" x="2.54" y="0" drill="1.1051" diameter="2.2102"/>
<text x="0" y="2.7481" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.9941" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="RESCAV127P508X210X70-8" urn="urn:adsk.eagle:footprint:7643968/1" locally_modified="yes">
<description>8-Chiparray 2-Side Flat, 1.27 mm pitch, 5.08 X 2.10 X 0.70 mm body
&lt;p&gt;8-pin Chiparray 2-Side Flat package with 1.27 mm pitch with body size 5.08 X 2.10 X 0.70 mm&lt;/p&gt;</description>
<text x="0" y="8.7891" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-8.405" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
<smd name="P$1" x="0.05" y="-2.9" dx="1.9" dy="2.375" layer="1" rot="R180"/>
<smd name="P$2" x="0.05" y="2.9" dx="1.9" dy="2.375" layer="1" rot="R180"/>
<smd name="P$3" x="0.05" y="-0.8" dx="1.9" dy="1.175" layer="1" rot="R180"/>
<smd name="P$4" x="0.05" y="0.8" dx="1.9" dy="1.175" layer="1" rot="R180"/>
<smd name="P$5" x="-2.25" y="2.45" dx="2.1" dy="1.475" layer="1" rot="R180"/>
<smd name="P$6" x="-2.25" y="-2.45" dx="2.1" dy="1.475" layer="1" rot="R180"/>
<smd name="D+" x="-2.6" y="0" dx="1.38" dy="0.45" layer="1" rot="R180"/>
<smd name="ID" x="-2.6" y="0.65" dx="1.38" dy="0.45" layer="1" rot="R180"/>
<smd name="D-" x="-2.6" y="-0.65" dx="1.38" dy="0.45" layer="1" rot="R180"/>
<smd name="GND" x="-2.6" y="1.3" dx="1.38" dy="0.45" layer="1" rot="R180"/>
<smd name="VCC" x="-2.6" y="-1.3" dx="1.38" dy="0.45" layer="1" rot="R180"/>
</package>
<package name="SOIC127P600X175-8" urn="urn:adsk.eagle:footprint:7656505/1">
<description>8-SOIC, 1.27 mm pitch, 6.00 mm span, 4.92 X 3.95 X 1.75 mm body
&lt;p&gt;8-pin SOIC package with 1.27 mm pitch, 6.00 mm span with body size 4.92 X 3.95 X 1.75 mm&lt;/p&gt;</description>
<circle x="-2.7901" y="2.7099" radius="0.25" width="0" layer="21"/>
<wire x1="-2.075" y1="2.4599" x2="-2.075" y2="2.56" width="0.12" layer="21"/>
<wire x1="-2.075" y1="2.56" x2="2.075" y2="2.56" width="0.12" layer="21"/>
<wire x1="2.075" y1="2.56" x2="2.075" y2="2.4599" width="0.12" layer="21"/>
<wire x1="-2.075" y1="-2.4599" x2="-2.075" y2="-2.56" width="0.12" layer="21"/>
<wire x1="-2.075" y1="-2.56" x2="2.075" y2="-2.56" width="0.12" layer="21"/>
<wire x1="2.075" y1="-2.56" x2="2.075" y2="-2.4599" width="0.12" layer="21"/>
<wire x1="2.075" y1="-2.56" x2="-2.075" y2="-2.56" width="0.12" layer="51"/>
<wire x1="-2.075" y1="-2.56" x2="-2.075" y2="2.56" width="0.12" layer="51"/>
<wire x1="-2.075" y1="2.56" x2="2.075" y2="2.56" width="0.12" layer="51"/>
<wire x1="2.075" y1="2.56" x2="2.075" y2="-2.56" width="0.12" layer="51"/>
<smd name="1" x="-2.4878" y="1.905" dx="2.0347" dy="0.6019" layer="1"/>
<smd name="2" x="-2.4878" y="0.635" dx="2.0347" dy="0.6019" layer="1"/>
<smd name="3" x="-2.4878" y="-0.635" dx="2.0347" dy="0.6019" layer="1"/>
<smd name="4" x="-2.4878" y="-1.905" dx="2.0347" dy="0.6019" layer="1"/>
<smd name="5" x="2.4878" y="-1.905" dx="2.0347" dy="0.6019" layer="1"/>
<smd name="6" x="2.4878" y="-0.635" dx="2.0347" dy="0.6019" layer="1"/>
<smd name="7" x="2.4878" y="0.635" dx="2.0347" dy="0.6019" layer="1"/>
<smd name="8" x="2.4878" y="1.905" dx="2.0347" dy="0.6019" layer="1"/>
<text x="0" y="3.5949" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-3.195" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="HDRV6W64P254_1X6_1524X254X838B" urn="urn:adsk.eagle:footprint:7657284/1" locally_modified="yes">
<description>Single-row, 6-pin Pin Header (Male) Straight, 2.54 mm (0.10 in) col pitch, 5.84 mm mating length, 15.24 X 2.54 X 8.38 mm body
&lt;p&gt;Single-row (1X6), 6-pin Pin Header (Male) Straight package with 2.54 mm (0.10 in) col pitch, 0.64 mm lead width, 3.00 mm tail length and 5.84 mm mating length with overall size 15.24 X 2.54 X 8.38 mm, pin pattern - clockwise from top left&lt;/p&gt;</description>
<circle x="0" y="1.774" radius="0.25" width="0" layer="21"/>
<wire x1="13.97" y1="-1.27" x2="-1.27" y2="-1.27" width="0.12" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="1.27" width="0.12" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="13.97" y2="1.27" width="0.12" layer="21"/>
<wire x1="13.97" y1="1.27" x2="13.97" y2="-1.27" width="0.12" layer="21"/>
<wire x1="13.97" y1="-1.27" x2="-1.27" y2="-1.27" width="0.12" layer="51"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="1.27" width="0.12" layer="51"/>
<wire x1="-1.27" y1="1.27" x2="13.97" y2="1.27" width="0.12" layer="51"/>
<wire x1="13.97" y1="1.27" x2="13.97" y2="-1.27" width="0.12" layer="51"/>
<pad name="1" x="0" y="0" drill="1.1051" diameter="2.2102"/>
<pad name="2" x="2.54" y="0" drill="1.1051" diameter="2.2102"/>
<pad name="3" x="5.08" y="0" drill="1.1051" diameter="2.2102"/>
<pad name="4" x="7.62" y="0" drill="1.1051" diameter="2.2102"/>
<pad name="5" x="10.16" y="0" drill="1.1051" diameter="2.2102"/>
<pad name="6" x="12.7" y="0" drill="1.1051" diameter="2.2102"/>
<text x="0" y="2.659" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.905" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="HDRV5W64P254_1X5_1270X254X838B" urn="urn:adsk.eagle:footprint:7657287/1" locally_modified="yes">
<description>Single-row, 5-pin Pin Header (Male) Straight, 2.54 mm (0.10 in) col pitch, 5.84 mm mating length, 12.70 X 2.54 X 8.38 mm body
&lt;p&gt;Single-row (1X5), 5-pin Pin Header (Male) Straight package with 2.54 mm (0.10 in) col pitch, 0.64 mm lead width, 3.00 mm tail length and 5.84 mm mating length with overall size 12.70 X 2.54 X 8.38 mm, pin pattern - clockwise from top left&lt;/p&gt;</description>
<circle x="0" y="1.774" radius="0.25" width="0" layer="21"/>
<wire x1="11.43" y1="-1.27" x2="-1.27" y2="-1.27" width="0.12" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="1.27" width="0.12" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="11.43" y2="1.27" width="0.12" layer="21"/>
<wire x1="11.43" y1="1.27" x2="11.43" y2="-1.27" width="0.12" layer="21"/>
<wire x1="11.43" y1="-1.27" x2="-1.27" y2="-1.27" width="0.12" layer="51"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="1.27" width="0.12" layer="51"/>
<wire x1="-1.27" y1="1.27" x2="11.43" y2="1.27" width="0.12" layer="51"/>
<wire x1="11.43" y1="1.27" x2="11.43" y2="-1.27" width="0.12" layer="51"/>
<pad name="1" x="0" y="0" drill="1.1051" diameter="2.2102"/>
<pad name="2" x="2.54" y="0" drill="1.1051" diameter="2.2102"/>
<pad name="3" x="5.08" y="0" drill="1.1051" diameter="2.2102"/>
<pad name="4" x="7.62" y="0" drill="1.1051" diameter="2.2102"/>
<pad name="5" x="10.16" y="0" drill="1.1051" diameter="2.2102"/>
<text x="0" y="2.659" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.905" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="HDRV3W64P254_1X3_762X254X838B" urn="urn:adsk.eagle:footprint:7657292/1">
<description>Single-row, 3-pin Pin Header (Male) Straight, 2.54 mm (0.10 in) col pitch, 5.84 mm mating length, 7.62 X 2.54 X 8.38 mm body
&lt;p&gt;Single-row (1X3), 3-pin Pin Header (Male) Straight package with 2.54 mm (0.10 in) col pitch, 0.64 mm lead width, 3.00 mm tail length and 5.84 mm mating length with overall size 7.62 X 2.54 X 8.38 mm, pin pattern - clockwise from top left&lt;/p&gt;</description>
<circle x="0" y="1.8631" radius="0.25" width="0" layer="21"/>
<wire x1="6.4407" y1="-1.3591" x2="-1.3607" y2="-1.3591" width="0.12" layer="21"/>
<wire x1="-1.3607" y1="-1.3591" x2="-1.3607" y2="1.3591" width="0.12" layer="21"/>
<wire x1="-1.3607" y1="1.3591" x2="6.4407" y2="1.3591" width="0.12" layer="21"/>
<wire x1="6.4407" y1="1.3591" x2="6.4407" y2="-1.3591" width="0.12" layer="21"/>
<wire x1="6.35" y1="-1.27" x2="-1.27" y2="-1.27" width="0.12" layer="51"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="1.27" width="0.12" layer="51"/>
<wire x1="-1.27" y1="1.27" x2="6.35" y2="1.27" width="0.12" layer="51"/>
<wire x1="6.35" y1="1.27" x2="6.35" y2="-1.27" width="0.12" layer="51"/>
<pad name="1" x="0" y="0" drill="1.1051" diameter="2.2102"/>
<pad name="2" x="2.54" y="0" drill="1.1051" diameter="2.2102"/>
<pad name="3" x="5.08" y="0" drill="1.1051" diameter="2.2102"/>
<text x="0" y="2.7481" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.9941" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="TO457P991X255-3" urn="urn:adsk.eagle:footprint:7648647/1">
<description>3-TO, DPAK, 4.57 mm pitch, 9.91 mm span, 6.54 X 6.09 X 2.55 mm body
&lt;p&gt;3-pin TO, DPAK package with 4.57 mm pitch, 9.91 mm span with body size 6.54 X 6.09 X 2.55 mm&lt;/p&gt;</description>
<circle x="-4.6971" y="3.2714" radius="0.25" width="0" layer="21"/>
<wire x1="4.11" y1="3.0899" x2="4.11" y2="3.365" width="0.12" layer="21"/>
<wire x1="4.11" y1="3.365" x2="-2.11" y2="3.365" width="0.12" layer="21"/>
<wire x1="-2.11" y1="3.365" x2="-2.11" y2="-3.365" width="0.12" layer="21"/>
<wire x1="-2.11" y1="-3.365" x2="4.11" y2="-3.365" width="0.12" layer="21"/>
<wire x1="4.11" y1="-3.365" x2="4.11" y2="-3.0899" width="0.12" layer="21"/>
<wire x1="4.11" y1="-3.365" x2="-2.11" y2="-3.365" width="0.12" layer="51"/>
<wire x1="-2.11" y1="-3.365" x2="-2.11" y2="3.365" width="0.12" layer="51"/>
<wire x1="-2.11" y1="3.365" x2="4.11" y2="3.365" width="0.12" layer="51"/>
<wire x1="4.11" y1="3.365" x2="4.11" y2="-3.365" width="0.12" layer="51"/>
<smd name="1" x="-4.6971" y="2.285" dx="1.732" dy="0.9648" layer="1"/>
<smd name="2" x="-4.6971" y="-2.285" dx="1.732" dy="0.9648" layer="1"/>
<smd name="3" x="2.265" y="0" dx="6.5961" dy="5.6718" layer="1"/>
<text x="0" y="4.1564" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-4" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="SOT230P700X180-4" urn="urn:adsk.eagle:footprint:7654763/1">
<description>4-SOT223, 2.30 mm pitch, 7.00 mm span, 6.50 X 3.50 X 1.80 mm body
&lt;p&gt;4-pin SOT223 package with 2.30 mm pitch, 7.00 mm span with body size 6.50 X 3.50 X 1.80 mm&lt;/p&gt;</description>
<circle x="-2.9276" y="3.2486" radius="0.25" width="0" layer="21"/>
<wire x1="-1.85" y1="2.9986" x2="-1.85" y2="3.35" width="0.12" layer="21"/>
<wire x1="-1.85" y1="3.35" x2="1.85" y2="3.35" width="0.12" layer="21"/>
<wire x1="1.85" y1="3.35" x2="1.85" y2="1.8486" width="0.12" layer="21"/>
<wire x1="-1.85" y1="-2.9986" x2="-1.85" y2="-3.35" width="0.12" layer="21"/>
<wire x1="-1.85" y1="-3.35" x2="1.85" y2="-3.35" width="0.12" layer="21"/>
<wire x1="1.85" y1="-3.35" x2="1.85" y2="-1.8486" width="0.12" layer="21"/>
<wire x1="1.85" y1="-3.35" x2="-1.85" y2="-3.35" width="0.12" layer="51"/>
<wire x1="-1.85" y1="-3.35" x2="-1.85" y2="3.35" width="0.12" layer="51"/>
<wire x1="-1.85" y1="3.35" x2="1.85" y2="3.35" width="0.12" layer="51"/>
<wire x1="1.85" y1="3.35" x2="1.85" y2="-3.35" width="0.12" layer="51"/>
<smd name="1" x="-3.0226" y="2.3" dx="1.9651" dy="0.8891" layer="1"/>
<smd name="2" x="-3.0226" y="0" dx="1.9651" dy="0.8891" layer="1"/>
<smd name="3" x="-3.0226" y="-2.3" dx="1.9651" dy="0.8891" layer="1"/>
<smd name="4" x="3.0226" y="0" dx="1.9651" dy="3.1891" layer="1"/>
<text x="0" y="4.1336" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-3.985" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPAE660X565" urn="urn:adsk.eagle:footprint:7654792/1">
<description>ECAP (Aluminum Electrolytic Capacitor), 6.60 X 5.65 mm body
&lt;p&gt;ECAP (Aluminum Electrolytic Capacitor) package with body size 6.60 X 5.65 mm&lt;/p&gt;</description>
<wire x1="-3.4" y1="1.0436" x2="-3.4" y2="2.0948" width="0.12" layer="21"/>
<wire x1="-3.4" y1="2.0948" x2="-2.0948" y2="3.4" width="0.12" layer="21"/>
<wire x1="-2.0948" y1="3.4" x2="3.4" y2="3.4" width="0.12" layer="21"/>
<wire x1="3.4" y1="3.4" x2="3.4" y2="1.0436" width="0.12" layer="21"/>
<wire x1="-3.4" y1="-1.0436" x2="-3.4" y2="-2.0948" width="0.12" layer="21"/>
<wire x1="-3.4" y1="-2.0948" x2="-2.0948" y2="-3.4" width="0.12" layer="21"/>
<wire x1="-2.0948" y1="-3.4" x2="3.4" y2="-3.4" width="0.12" layer="21"/>
<wire x1="3.4" y1="-3.4" x2="3.4" y2="-1.0436" width="0.12" layer="21"/>
<wire x1="3.4" y1="-3.4" x2="-3.4" y2="-3.4" width="0.12" layer="51"/>
<wire x1="-3.4" y1="-3.4" x2="-3.4" y2="3.4" width="0.12" layer="51"/>
<wire x1="-3.4" y1="3.4" x2="3.4" y2="3.4" width="0.12" layer="51"/>
<wire x1="3.4" y1="3.4" x2="3.4" y2="-3.4" width="0.12" layer="51"/>
<smd name="1" x="-2.6464" y="0" dx="3.5081" dy="1.5791" layer="1"/>
<smd name="2" x="2.6464" y="0" dx="3.5081" dy="1.5791" layer="1"/>
<text x="0" y="4.035" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-4.035" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPPAD1070W10L850D250B" urn="urn:adsk.eagle:footprint:7657503/1" locally_modified="yes">
<description>Axial Polarized capacitor, 10.70 mm pitch, 8.50 mm body length, 2.50 mm body diameter
&lt;p&gt;Axial Polarized capacitor package with 10.70 mm pitch, 0.10 mm lead diameter, 8.50 mm body length and 2.50 mm body diameter&lt;/p&gt;</description>
<pad name="GND" x="-3.75" y="0" drill="0.8" diameter="2.1844" shape="octagon"/>
<pad name="2" x="3.75" y="0" drill="0.8" diameter="2.1844"/>
<text x="0" y="8.635" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0.25" y="-8.635" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
<circle x="0" y="0" radius="8" width="0.127" layer="21"/>
<rectangle x1="-8" y1="-0.5" x2="-6.5" y2="0.5" layer="21"/>
</package>
<package name="CAPC1608X50" urn="urn:adsk.eagle:footprint:7642702/1" locally_modified="yes">
<description>Chip, 1.60 X 0.80 X 0.50 mm body
&lt;p&gt;Chip package with body size 1.60 X 0.80 X 0.50 mm&lt;/p&gt;</description>
<wire x1="0.85" y1="-0.45" x2="-0.85" y2="-0.45" width="0.12" layer="51"/>
<wire x1="-0.85" y1="-0.45" x2="-0.85" y2="0.45" width="0.12" layer="51"/>
<wire x1="-0.85" y1="0.45" x2="0.85" y2="0.45" width="0.12" layer="51"/>
<wire x1="0.85" y1="0.45" x2="0.85" y2="-0.45" width="0.12" layer="51"/>
<smd name="1" x="-0.7797" y="0" dx="0.8697" dy="0.9291" layer="1"/>
<smd name="2" x="0.7797" y="0" dx="0.8697" dy="0.9291" layer="1"/>
<text x="0" y="1.4136" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.4136" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPC2012X70" urn="urn:adsk.eagle:footprint:7642708/1" locally_modified="yes">
<description>Chip, 2.00 X 1.25 X 0.70 mm body
&lt;p&gt;Chip package with body size 2.00 X 1.25 X 0.70 mm&lt;/p&gt;</description>
<wire x1="1.05" y1="-0.675" x2="-1.05" y2="-0.675" width="0.12" layer="51"/>
<wire x1="-1.05" y1="-0.675" x2="-1.05" y2="0.675" width="0.12" layer="51"/>
<wire x1="-1.05" y1="0.675" x2="1.05" y2="0.675" width="0.12" layer="51"/>
<wire x1="1.05" y1="0.675" x2="1.05" y2="-0.675" width="0.12" layer="51"/>
<smd name="1" x="-0.8883" y="0" dx="1.0525" dy="1.3791" layer="1"/>
<smd name="2" x="0.8883" y="0" dx="1.0525" dy="1.3791" layer="1"/>
<text x="0" y="1.6386" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.6386" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="DIOM6859X262" urn="urn:adsk.eagle:footprint:7707353/1">
<description>Molded Body, 6.86 X 5.91 X 2.62 mm body
&lt;p&gt;Molded Body package with body size 6.86 X 5.91 X 2.62 mm&lt;/p&gt;</description>
<wire x1="3.555" y1="3.11" x2="-4.3751" y2="3.11" width="0.12" layer="21"/>
<wire x1="-4.3751" y1="3.11" x2="-4.3751" y2="-3.11" width="0.12" layer="21"/>
<wire x1="-4.3751" y1="-3.11" x2="3.555" y2="-3.11" width="0.12" layer="21"/>
<wire x1="3.555" y1="-3.11" x2="-3.555" y2="-3.11" width="0.12" layer="51"/>
<wire x1="-3.555" y1="-3.11" x2="-3.555" y2="3.11" width="0.12" layer="51"/>
<wire x1="-3.555" y1="3.11" x2="3.555" y2="3.11" width="0.12" layer="51"/>
<wire x1="3.555" y1="3.11" x2="3.555" y2="-3.11" width="0.12" layer="51"/>
<smd name="1" x="-2.9493" y="0" dx="2.2236" dy="3.1202" layer="1"/>
<smd name="2" x="2.9493" y="0" dx="2.2236" dy="3.1202" layer="1"/>
<text x="0" y="3.745" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-3.745" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="DIOM4226X242" urn="urn:adsk.eagle:footprint:7707361/1">
<description>Molded Body, 4.25 X 2.67 X 2.42 mm body
&lt;p&gt;Molded Body package with body size 4.25 X 2.67 X 2.42 mm&lt;/p&gt;</description>
<wire x1="2.25" y1="1.4" x2="-3.0701" y2="1.4" width="0.12" layer="21"/>
<wire x1="-3.0701" y1="1.4" x2="-3.0701" y2="-1.4" width="0.12" layer="21"/>
<wire x1="-3.0701" y1="-1.4" x2="2.25" y2="-1.4" width="0.12" layer="21"/>
<wire x1="2.25" y1="-1.4" x2="-2.25" y2="-1.4" width="0.12" layer="51"/>
<wire x1="-2.25" y1="-1.4" x2="-2.25" y2="1.4" width="0.12" layer="51"/>
<wire x1="-2.25" y1="1.4" x2="2.25" y2="1.4" width="0.12" layer="51"/>
<wire x1="2.25" y1="1.4" x2="2.25" y2="-1.4" width="0.12" layer="51"/>
<smd name="1" x="-1.6443" y="0" dx="2.2236" dy="1.6153" layer="1"/>
<smd name="2" x="1.6443" y="0" dx="2.2236" dy="1.6153" layer="1"/>
<text x="0" y="2.035" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.035" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="RESC2012X65" urn="urn:adsk.eagle:footprint:7641822/1" locally_modified="yes">
<description>Chip, 2.00 X 1.25 X 0.65 mm body
&lt;p&gt;Chip package with body size 2.00 X 1.25 X 0.65 mm&lt;/p&gt;</description>
<wire x1="1.05" y1="-0.675" x2="-1.05" y2="-0.675" width="0.12" layer="51"/>
<wire x1="-1.05" y1="-0.675" x2="-1.05" y2="0.675" width="0.12" layer="51"/>
<wire x1="-1.05" y1="0.675" x2="1.05" y2="0.675" width="0.12" layer="51"/>
<wire x1="1.05" y1="0.675" x2="1.05" y2="-0.675" width="0.12" layer="51"/>
<smd name="1" x="-0.8797" y="0" dx="1.0697" dy="1.3791" layer="1"/>
<smd name="2" x="0.8797" y="0" dx="1.0697" dy="1.3791" layer="1"/>
<text x="0" y="1.6386" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.6386" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="RESC3216X70" urn="urn:adsk.eagle:footprint:7642713/1" locally_modified="yes">
<description>Chip, 3.20 X 1.60 X 0.70 mm body
&lt;p&gt;Chip package with body size 3.20 X 1.60 X 0.70 mm&lt;/p&gt;</description>
<wire x1="1.675" y1="-0.875" x2="-1.675" y2="-0.875" width="0.12" layer="51"/>
<wire x1="-1.675" y1="-0.875" x2="-1.675" y2="0.875" width="0.12" layer="51"/>
<wire x1="-1.675" y1="0.875" x2="1.675" y2="0.875" width="0.12" layer="51"/>
<wire x1="1.675" y1="0.875" x2="1.675" y2="-0.875" width="0.12" layer="51"/>
<smd name="1" x="-1.4945" y="0" dx="1.0812" dy="1.7702" layer="1"/>
<smd name="2" x="1.4945" y="0" dx="1.0812" dy="1.7702" layer="1"/>
<text x="0" y="1.8341" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.8341" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="LEDC2012X110" urn="urn:adsk.eagle:footprint:7707401/1">
<description>Chip, 2.00 X 1.25 X 1.10 mm body
&lt;p&gt;Chip package with body size 2.00 X 1.25 X 1.10 mm&lt;/p&gt;</description>
<wire x1="1" y1="0.9949" x2="-1.6599" y2="0.9949" width="0.12" layer="21"/>
<wire x1="-1.6599" y1="0.9949" x2="-1.6599" y2="-0.9949" width="0.12" layer="21"/>
<wire x1="-1.6599" y1="-0.9949" x2="1" y2="-0.9949" width="0.12" layer="21"/>
<wire x1="1" y1="-0.625" x2="-1" y2="-0.625" width="0.12" layer="51"/>
<wire x1="-1" y1="-0.625" x2="-1" y2="0.625" width="0.12" layer="51"/>
<wire x1="-1" y1="0.625" x2="1" y2="0.625" width="0.12" layer="51"/>
<wire x1="1" y1="0.625" x2="1" y2="-0.625" width="0.12" layer="51"/>
<smd name="1" x="-0.8624" y="0" dx="1.0871" dy="1.3618" layer="1"/>
<smd name="2" x="0.8624" y="0" dx="1.0871" dy="1.3618" layer="1"/>
<text x="0" y="1.6299" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.6299" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="DIOM1208X70" urn="urn:adsk.eagle:footprint:7707416/1">
<description>Molded Body, 1.20 X 0.80 X 0.70 mm body
&lt;p&gt;Molded Body package with body size 1.20 X 0.80 X 0.70 mm&lt;/p&gt;</description>
<wire x1="0.65" y1="0.464" x2="-1.4786" y2="0.464" width="0.12" layer="21"/>
<wire x1="-1.4786" y1="0.464" x2="-1.4786" y2="-0.464" width="0.12" layer="21"/>
<wire x1="-1.4786" y1="-0.464" x2="0.65" y2="-0.464" width="0.12" layer="21"/>
<wire x1="0.65" y1="-0.45" x2="-0.65" y2="-0.45" width="0.12" layer="51"/>
<wire x1="-0.65" y1="-0.45" x2="-0.65" y2="0.45" width="0.12" layer="51"/>
<wire x1="-0.65" y1="0.45" x2="0.65" y2="0.45" width="0.12" layer="51"/>
<wire x1="0.65" y1="0.45" x2="0.65" y2="-0.45" width="0.12" layer="51"/>
<smd name="1" x="-0.6596" y="0" dx="1.01" dy="0.3" layer="1"/>
<smd name="2" x="0.6596" y="0" dx="1.01" dy="0.3" layer="1"/>
<text x="0" y="1.099" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.099" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="SOT95P240X110-3" urn="urn:adsk.eagle:footprint:7656000/1">
<description>3-SOT23, 0.95 mm pitch, 2.40 mm span, 2.90 X 1.30 X 1.10 mm body
&lt;p&gt;3-pin SOT23 package with 0.95 mm pitch, 2.40 mm span with body size 2.90 X 1.30 X 1.10 mm&lt;/p&gt;</description>
<circle x="-1.204" y="1.7586" radius="0.25" width="0" layer="21"/>
<wire x1="-0.7" y1="1.5686" x2="0.7" y2="1.5686" width="0.12" layer="21"/>
<wire x1="0.7" y1="1.5686" x2="0.7" y2="0.5586" width="0.12" layer="21"/>
<wire x1="-0.7" y1="-1.5686" x2="0.7" y2="-1.5686" width="0.12" layer="21"/>
<wire x1="0.7" y1="-1.5686" x2="0.7" y2="-0.5586" width="0.12" layer="21"/>
<wire x1="0.7" y1="-1.5" x2="-0.7" y2="-1.5" width="0.12" layer="51"/>
<wire x1="-0.7" y1="-1.5" x2="-0.7" y2="1.5" width="0.12" layer="51"/>
<wire x1="-0.7" y1="1.5" x2="0.7" y2="1.5" width="0.12" layer="51"/>
<wire x1="0.7" y1="1.5" x2="0.7" y2="-1.5" width="0.12" layer="51"/>
<smd name="1" x="-1.0245" y="0.95" dx="1.1801" dy="0.6092" layer="1"/>
<smd name="2" x="-1.0245" y="-0.95" dx="1.1801" dy="0.6092" layer="1"/>
<smd name="3" x="1.0245" y="0" dx="1.1801" dy="0.6092" layer="1"/>
<text x="0" y="2.6436" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.2036" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="HDRV8W64P254_2X4_1016X508X838B" urn="urn:adsk.eagle:footprint:7657588/1" locally_modified="yes">
<description>Double-row, 8-pin Pin Header (Male) Straight, 2.54 mm (0.10 in) row pitch, 2.54 mm (0.10 in) col pitch, 5.84 mm mating length, 10.16 X 5.08 X 8.38 mm body
&lt;p&gt;Double-row (2X4), 8-pin Pin Header (Male) Straight package with 2.54 mm (0.10 in) row pitch, 2.54 mm (0.10 in) col pitch, 0.64 mm lead width, 3.00 mm tail length and 5.84 mm mating length with overall size 10.16 X 5.08 X 8.38 mm, pin pattern - clockwise from top left&lt;/p&gt;</description>
<wire x1="6.858" y1="-5.842" x2="6.858" y2="9.779" width="0.2032" layer="21"/>
<wire x1="6.858" y1="9.779" x2="-6.858" y2="9.779" width="0.2032" layer="21"/>
<wire x1="-6.858" y1="9.779" x2="-6.858" y2="-5.842" width="0.2032" layer="21"/>
<wire x1="6.858" y1="-5.842" x2="-6.858" y2="-5.842" width="0.2032" layer="21"/>
<wire x1="-6.858" y1="-5.842" x2="-6.858" y2="-8.763" width="0.2032" layer="51"/>
<wire x1="-6.858" y1="-8.763" x2="6.858" y2="-8.763" width="0.2032" layer="51"/>
<wire x1="6.858" y1="-8.763" x2="6.858" y2="-5.842" width="0.2032" layer="51"/>
<pad name="1" x="-3.175" y="8.255" drill="1.016" diameter="1.8796"/>
<pad name="2" x="-1.905" y="5.715" drill="1.016" diameter="1.8796"/>
<pad name="3" x="-0.635" y="8.255" drill="1.016" diameter="1.8796"/>
<pad name="4" x="0.635" y="5.715" drill="1.016" diameter="1.8796"/>
<pad name="5" x="1.905" y="8.255" drill="1.016" diameter="1.8796"/>
<pad name="6" x="3.175" y="5.715" drill="1.016" diameter="1.8796"/>
<text x="-1.3208" y="1.778" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.651" y="-3.429" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
<hole x="-5.08" y="-0.635" drill="3.3"/>
<hole x="5.08" y="-0.635" drill="3.3"/>
</package>
<package name="HDRV6W64P254_2X3_762X508X838B" urn="urn:adsk.eagle:footprint:7707328/1" locally_modified="yes">
<description>Double-row, 6-pin Pin Header (Male) Straight, 2.54 mm (0.10 in) row pitch, 2.54 mm (0.10 in) col pitch, 5.84 mm mating length, 7.62 X 5.08 X 8.38 mm body
&lt;p&gt;Double-row (2X3), 6-pin Pin Header (Male) Straight package with 2.54 mm (0.10 in) row pitch, 2.54 mm (0.10 in) col pitch, 0.64 mm lead width, 3.00 mm tail length and 5.84 mm mating length with overall size 7.62 X 5.08 X 8.38 mm, pin pattern - clockwise from top left&lt;/p&gt;</description>
<wire x1="6.858" y1="-5.842" x2="6.858" y2="9.779" width="0.2032" layer="21"/>
<wire x1="6.858" y1="9.779" x2="-6.858" y2="9.779" width="0.2032" layer="21"/>
<wire x1="-6.858" y1="9.779" x2="-6.858" y2="-5.842" width="0.2032" layer="21"/>
<wire x1="6.858" y1="-5.842" x2="-6.858" y2="-5.842" width="0.2032" layer="21"/>
<wire x1="-6.858" y1="-5.842" x2="-6.858" y2="-8.763" width="0.2032" layer="51"/>
<wire x1="-6.858" y1="-8.763" x2="6.858" y2="-8.763" width="0.2032" layer="51"/>
<wire x1="6.858" y1="-8.763" x2="6.858" y2="-5.842" width="0.2032" layer="51"/>
<pad name="1" x="-3.175" y="8.255" drill="1.016" diameter="1.8796"/>
<pad name="2" x="-1.905" y="5.715" drill="1.016" diameter="1.8796"/>
<pad name="3" x="-0.635" y="8.255" drill="1.016" diameter="1.8796"/>
<pad name="4" x="0.635" y="5.715" drill="1.016" diameter="1.8796"/>
<pad name="5" x="1.905" y="8.255" drill="1.016" diameter="1.8796"/>
<pad name="6" x="3.175" y="5.715" drill="1.016" diameter="1.8796"/>
<hole x="-5.08" y="-0.635" drill="3.3"/>
<hole x="5.08" y="-0.635" drill="3.3"/>
</package>
<package name="HDRVR6W100P508_1X6_3048X508X254B" urn="urn:adsk.eagle:footprint:7708562/1">
<description>Single-row, 6-pin Receptacle Header (Female) Straight, 5.08 mm (0.20 in) col pitch, 2.54 mm insulator length, 30.48 X 5.08 X 2.54 mm body
&lt;p&gt;Single-row (1X6), 6-pin Receptacle Header (Female) Straight package with 5.08 mm (0.20 in) col pitch, 1.00 mm lead width, 3.00 mm tail length and 2.54 mm insulator length with overall size 30.48 X 5.08 X 2.54 mm, pin pattern - clockwise from top left&lt;/p&gt;</description>
<circle x="0" y="3.044" radius="0.25" width="0" layer="21"/>
<wire x1="27.94" y1="-2.54" x2="-2.54" y2="-2.54" width="0.12" layer="21"/>
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="2.54" width="0.12" layer="21"/>
<wire x1="-2.54" y1="2.54" x2="27.94" y2="2.54" width="0.12" layer="21"/>
<wire x1="27.94" y1="2.54" x2="27.94" y2="-2.54" width="0.12" layer="21"/>
<wire x1="27.94" y1="-2.54" x2="-2.54" y2="-2.54" width="0.12" layer="51"/>
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="2.54" width="0.12" layer="51"/>
<wire x1="-2.54" y1="2.54" x2="27.94" y2="2.54" width="0.12" layer="51"/>
<wire x1="27.94" y1="2.54" x2="27.94" y2="-2.54" width="0.12" layer="51"/>
<pad name="1" x="0" y="0" drill="1.6142" diameter="3.2284"/>
<pad name="2" x="5.08" y="0" drill="1.6142" diameter="3.2284"/>
<pad name="3" x="10.16" y="0" drill="1.6142" diameter="3.2284"/>
<pad name="4" x="15.24" y="0" drill="1.6142" diameter="3.2284"/>
<pad name="5" x="20.32" y="0" drill="1.6142" diameter="3.2284"/>
<pad name="6" x="25.4" y="0" drill="1.6142" diameter="3.2284"/>
<text x="0" y="3.929" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-3.175" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="HDRV2W120P2260_1X2_2260X900X1554B" urn="urn:adsk.eagle:footprint:7654936/1" locally_modified="yes">
<description>Single-row, 2-pin Pin Header (Male) Straight, 22.60 mm (0.89 in) col pitch, 5.84 mm mating length, 22.60 X 9.00 X 15.54 mm body
&lt;p&gt;Single-row (1X2), 2-pin Pin Header (Male) Straight package with 22.60 mm (0.89 in) col pitch, 1.20 mm lead width, 3.00 mm tail length and 5.84 mm mating length with overall size 22.60 X 9.00 X 15.54 mm, pin pattern - clockwise from top left&lt;/p&gt;</description>
<circle x="-11.3" y="5.004" radius="0.25" width="0" layer="21"/>
<wire x1="12.9789" y1="-4.5" x2="-12.9789" y2="-4.5" width="0.12" layer="21"/>
<wire x1="-12.9789" y1="-4.5" x2="-12.9789" y2="4.5" width="0.12" layer="21"/>
<wire x1="-12.9789" y1="4.5" x2="12.9789" y2="4.5" width="0.12" layer="21"/>
<wire x1="12.9789" y1="4.5" x2="12.9789" y2="-4.5" width="0.12" layer="21"/>
<wire x1="11.3" y1="-4.5" x2="-11.3" y2="-4.5" width="0.12" layer="51"/>
<wire x1="-11.3" y1="-4.5" x2="-11.3" y2="4.5" width="0.12" layer="51"/>
<wire x1="-11.3" y1="4.5" x2="11.3" y2="4.5" width="0.12" layer="51"/>
<wire x1="11.3" y1="4.5" x2="11.3" y2="-4.5" width="0.12" layer="51"/>
<text x="0" y="5.889" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-5.135" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
<pad name="P$1" x="-11.43" y="0" drill="1.2" diameter="3"/>
<pad name="P$2" x="11.43" y="0" drill="1.2" diameter="3"/>
</package>
<package name="HDRV4W64P254_1X4_1016X254X838B" urn="urn:adsk.eagle:footprint:7706677/1" locally_modified="yes">
<description>Single-row, 4-pin Pin Header (Male) Straight, 2.54 mm (0.10 in) col pitch, 5.84 mm mating length, 10.16 X 2.54 X 8.38 mm body
&lt;p&gt;Single-row (1X4), 4-pin Pin Header (Male) Straight package with 2.54 mm (0.10 in) col pitch, 0.64 mm lead width, 3.00 mm tail length and 5.84 mm mating length with overall size 10.16 X 2.54 X 8.38 mm, pin pattern - clockwise from top left&lt;/p&gt;</description>
<circle x="0" y="1.774" radius="0.25" width="0" layer="21"/>
<wire x1="8.89" y1="-1.27" x2="-1.27" y2="-1.27" width="0.12" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="1.27" width="0.12" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="8.89" y2="1.27" width="0.12" layer="21"/>
<wire x1="8.89" y1="1.27" x2="8.89" y2="-1.27" width="0.12" layer="21"/>
<wire x1="8.89" y1="-1.27" x2="-1.27" y2="-1.27" width="0.12" layer="51"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="1.27" width="0.12" layer="51"/>
<wire x1="-1.27" y1="1.27" x2="8.89" y2="1.27" width="0.12" layer="51"/>
<wire x1="8.89" y1="1.27" x2="8.89" y2="-1.27" width="0.12" layer="51"/>
<pad name="1" x="0" y="0" drill="1.1051" diameter="2.2102"/>
<pad name="2" x="2.54" y="0" drill="1.1051" diameter="2.2102"/>
<pad name="3" x="5.08" y="0" drill="1.1051" diameter="2.2102"/>
<pad name="4" x="7.62" y="0" drill="1.1051" diameter="2.2102"/>
<text x="0" y="2.659" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.905" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="HDRV4W100P450X650_2X2_600X500X360B" urn="urn:adsk.eagle:footprint:7707449/1" locally_modified="yes">
<description>Double-row, 4-pin Pin Header (Male) Straight, 4.50 mm (0.18 in) pitch, 0.10 mm mating length, 6.00 X 5.00 X 3.60 mm body
&lt;p&gt;Double-row (2X2), 4-pin Pin Header (Male) Straight package with 4.50 mm (0.18 in) pitch, 1.00 mm lead width, 3.00 mm tail length and 0.10 mm mating length with overall size 6.00 X 5.00 X 3.60 mm, pin pattern - clockwise from top left&lt;/p&gt;</description>
<circle x="0" y="2.3722" radius="0.25" width="0" layer="21"/>
<wire x1="6.25" y1="-4.75" x2="0.25" y2="-4.75" width="0.12" layer="51"/>
<wire x1="0.25" y1="-4.75" x2="0.25" y2="0.25" width="0.12" layer="51"/>
<wire x1="0.25" y1="0.25" x2="6.25" y2="0.25" width="0.12" layer="51"/>
<wire x1="6.25" y1="0.25" x2="6.25" y2="-4.75" width="0.12" layer="51"/>
<pad name="1" x="0" y="0" drill="1" diameter="2.54"/>
<pad name="2" x="6.5" y="0" drill="1" diameter="2.54"/>
<pad name="3" x="6.5" y="-4.5" drill="1" diameter="2.54"/>
<pad name="4" x="0" y="-4.5" drill="1" diameter="2.54"/>
<text x="0" y="3.2572" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-7.0032" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="SOT95P240X110-5" urn="urn:adsk.eagle:footprint:9867717/1">
<description>5-SOT23, 0.95 mm pitch, 2.40 mm span, 2.90 X 1.30 X 1.10 mm body
&lt;p&gt;5-pin SOT23 package with 0.95 mm pitch, 2.40 mm span with body size 2.90 X 1.30 X 1.10 mm&lt;/p&gt;</description>
<circle x="-1.204" y="1.7586" radius="0.25" width="0" layer="21"/>
<wire x1="-0.7" y1="1.5686" x2="0.7" y2="1.5686" width="0.12" layer="21"/>
<wire x1="-0.7" y1="-1.5686" x2="0.7" y2="-1.5686" width="0.12" layer="21"/>
<wire x1="0.7" y1="-1.5" x2="-0.7" y2="-1.5" width="0.12" layer="51"/>
<wire x1="-0.7" y1="-1.5" x2="-0.7" y2="1.5" width="0.12" layer="51"/>
<wire x1="-0.7" y1="1.5" x2="0.7" y2="1.5" width="0.12" layer="51"/>
<wire x1="0.7" y1="1.5" x2="0.7" y2="-1.5" width="0.12" layer="51"/>
<smd name="1" x="-1.0245" y="0.95" dx="1.1801" dy="0.6092" layer="1"/>
<smd name="2" x="-1.0245" y="0" dx="1.1801" dy="0.6092" layer="1"/>
<smd name="3" x="-1.0245" y="-0.95" dx="1.1801" dy="0.6092" layer="1"/>
<smd name="4" x="1.0245" y="-0.95" dx="1.1801" dy="0.6092" layer="1"/>
<smd name="5" x="1.0245" y="0.95" dx="1.1801" dy="0.6092" layer="1"/>
<text x="0" y="2.6436" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.2036" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="CAPAE1030X1050" urn="urn:adsk.eagle:footprint:10452072/1">
<description>ECAP (Aluminum Electrolytic Capacitor), 10.30 X 10.50 mm body
&lt;p&gt;ECAP (Aluminum Electrolytic Capacitor) package with body size 10.30 X 10.50 mm&lt;/p&gt;</description>
<wire x1="-5.25" y1="1.3117" x2="-5.25" y2="3.1538" width="0.12" layer="21"/>
<wire x1="-5.25" y1="3.1538" x2="-3.1538" y2="5.25" width="0.12" layer="21"/>
<wire x1="-3.1538" y1="5.25" x2="5.25" y2="5.25" width="0.12" layer="21"/>
<wire x1="5.25" y1="5.25" x2="5.25" y2="1.3117" width="0.12" layer="21"/>
<wire x1="-5.25" y1="-1.3117" x2="-5.25" y2="-3.1538" width="0.12" layer="21"/>
<wire x1="-5.25" y1="-3.1538" x2="-3.1538" y2="-5.25" width="0.12" layer="21"/>
<wire x1="-3.1538" y1="-5.25" x2="5.25" y2="-5.25" width="0.12" layer="21"/>
<wire x1="5.25" y1="-5.25" x2="5.25" y2="-1.3117" width="0.12" layer="21"/>
<wire x1="5.25" y1="-5.25" x2="-5.25" y2="-5.25" width="0.12" layer="51"/>
<wire x1="-5.25" y1="-5.25" x2="-5.25" y2="5.25" width="0.12" layer="51"/>
<wire x1="-5.25" y1="5.25" x2="5.25" y2="5.25" width="0.12" layer="51"/>
<wire x1="5.25" y1="5.25" x2="5.25" y2="-5.25" width="0.12" layer="51"/>
<smd name="1" x="-3.9504" y="0" dx="3.6111" dy="2.1153" layer="1"/>
<smd name="2" x="3.9504" y="0" dx="3.6111" dy="2.1153" layer="1"/>
<text x="0" y="5.885" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-5.885" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="TO228P998X240-3" urn="urn:adsk.eagle:footprint:10452090/1" locally_modified="yes">
<description>3-TO, DPAK, 2.28 mm pitch, 9.98 mm span, 6.55 X 6.32 X 2.40 mm body
&lt;p&gt;3-pin TO, DPAK package with 2.28 mm pitch, 9.98 mm span with body size 6.55 X 6.32 X 2.40 mm&lt;/p&gt;</description>
<circle x="-4.34" y="3.2266" radius="0.25" width="0" layer="21"/>
<wire x1="4.145" y1="3.0486" x2="4.145" y2="3.325" width="0.12" layer="21"/>
<wire x1="4.145" y1="3.325" x2="-2.275" y2="3.325" width="0.12" layer="21"/>
<wire x1="-2.275" y1="3.325" x2="-2.275" y2="-3.325" width="0.12" layer="21"/>
<wire x1="-2.275" y1="-3.325" x2="4.145" y2="-3.325" width="0.12" layer="21"/>
<wire x1="4.145" y1="-3.325" x2="4.145" y2="-3.0486" width="0.12" layer="21"/>
<wire x1="4.145" y1="-3.325" x2="-2.275" y2="-3.325" width="0.12" layer="51"/>
<wire x1="-2.275" y1="-3.325" x2="-2.275" y2="3.325" width="0.12" layer="51"/>
<wire x1="-2.275" y1="3.325" x2="4.145" y2="3.325" width="0.12" layer="51"/>
<wire x1="4.145" y1="3.325" x2="4.145" y2="-3.325" width="0.12" layer="51"/>
<smd name="1" x="-4.34" y="2.28" dx="2.5062" dy="0.8852" layer="1"/>
<smd name="3" x="-4.34" y="-2.28" dx="2.5062" dy="0.8852" layer="1"/>
<smd name="2" x="2.5038" y="0" dx="6.1787" dy="5.5891" layer="1"/>
<text x="0" y="4.1116" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-3.96" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
<package name="SOP100P1900X235-30" urn="urn:adsk.eagle:footprint:7638379/1" locally_modified="yes">
<description>30-SOP, 1.00 mm pitch, 19.00 mm span, 17.20 X 16.00 X 2.35 mm body
&lt;p&gt;30-pin SOP package with 1.00 mm pitch, 19.00 mm span with body size 17.20 X 16.00 X 2.35 mm&lt;/p&gt;</description>
<circle x="-8.9925" y="7.8416" radius="0.25" width="0" layer="21"/>
<wire x1="-8.05" y1="7.5916" x2="-8.05" y2="8.65" width="0.12" layer="21"/>
<wire x1="-8.05" y1="8.65" x2="8.05" y2="8.65" width="0.12" layer="21"/>
<wire x1="8.05" y1="8.65" x2="8.05" y2="7.5916" width="0.12" layer="21"/>
<wire x1="-8.05" y1="-7.5916" x2="-8.05" y2="-8.65" width="0.12" layer="21"/>
<wire x1="-8.05" y1="-8.65" x2="8.05" y2="-8.65" width="0.12" layer="21"/>
<wire x1="8.05" y1="-8.65" x2="8.05" y2="-7.5916" width="0.12" layer="21"/>
<wire x1="8.05" y1="-8.65" x2="-8.05" y2="-8.65" width="0.12" layer="51"/>
<wire x1="-8.05" y1="-8.65" x2="-8.05" y2="8.65" width="0.12" layer="51"/>
<wire x1="-8.05" y1="8.65" x2="8.05" y2="8.65" width="0.12" layer="51"/>
<wire x1="8.05" y1="8.65" x2="8.05" y2="-8.65" width="0.12" layer="51"/>
<smd name="1" x="-8.98" y="7" dx="1.9101" dy="0.6752" layer="1"/>
<smd name="2" x="-8.98" y="6" dx="1.9101" dy="0.6752" layer="1"/>
<smd name="3" x="-8.98" y="5" dx="1.9101" dy="0.6752" layer="1"/>
<smd name="4" x="-8.98" y="4" dx="1.9101" dy="0.6752" layer="1"/>
<smd name="5" x="-8.98" y="3" dx="1.9101" dy="0.6752" layer="1"/>
<smd name="6" x="-8.98" y="2" dx="1.9101" dy="0.6752" layer="1"/>
<smd name="7" x="-8.98" y="1" dx="1.9101" dy="0.6752" layer="1"/>
<smd name="8" x="-8.98" y="0" dx="1.9101" dy="0.6752" layer="1"/>
<smd name="9" x="-8.98" y="-1" dx="1.9101" dy="0.6752" layer="1"/>
<smd name="10" x="-8.98" y="-2" dx="1.9101" dy="0.6752" layer="1"/>
<smd name="11" x="-8.98" y="-3" dx="1.9101" dy="0.6752" layer="1"/>
<smd name="12" x="-8.98" y="-4" dx="1.9101" dy="0.6752" layer="1"/>
<smd name="13" x="-8.98" y="-5" dx="1.9101" dy="0.6752" layer="1"/>
<smd name="14" x="-8.98" y="-6" dx="1.9101" dy="0.6752" layer="1"/>
<smd name="15" x="-8.98" y="-7" dx="1.9101" dy="0.6752" layer="1"/>
<smd name="16" x="8.98" y="-7" dx="1.9101" dy="0.6752" layer="1"/>
<smd name="17" x="8.98" y="-6" dx="1.9101" dy="0.6752" layer="1"/>
<smd name="18" x="8.98" y="-5" dx="1.9101" dy="0.6752" layer="1"/>
<smd name="19" x="8.98" y="-4" dx="1.9101" dy="0.6752" layer="1"/>
<smd name="20" x="8.98" y="-3" dx="1.9101" dy="0.6752" layer="1"/>
<smd name="21" x="8.98" y="-2" dx="1.9101" dy="0.6752" layer="1"/>
<smd name="22" x="8.98" y="-1" dx="1.9101" dy="0.6752" layer="1"/>
<smd name="23" x="8.98" y="0" dx="1.9101" dy="0.6752" layer="1"/>
<smd name="24" x="8.98" y="1" dx="1.9101" dy="0.6752" layer="1"/>
<smd name="25" x="8.98" y="2" dx="1.9101" dy="0.6752" layer="1"/>
<smd name="26" x="8.98" y="3" dx="1.9101" dy="0.6752" layer="1"/>
<smd name="27" x="8.98" y="4" dx="1.9101" dy="0.6752" layer="1"/>
<smd name="28" x="8.98" y="5" dx="1.9101" dy="0.6752" layer="1"/>
<smd name="29" x="8.98" y="6" dx="1.9101" dy="0.6752" layer="1"/>
<smd name="30" x="8.98" y="7" dx="1.9101" dy="0.6752" layer="1"/>
<text x="0" y="9.285" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-9.285" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
<smd name="SLUG1" x="-3" y="0" dx="10.3" dy="5.25" layer="1" rot="R90"/>
<smd name="SLUG2" x="3" y="4.2" dx="6.23" dy="5.25" layer="1" rot="R90"/>
<smd name="SLUG3" x="3" y="-4.2" dx="6.23" dy="5.25" layer="1" rot="R90"/>
</package>
<package name="HDRVR4W100P508_1X4_2032X508X1200B" urn="urn:adsk.eagle:footprint:10452783/1" locally_modified="yes">
<description>Single-row, 4-pin Receptacle Header (Female) Straight, 5.08 mm (0.20 in) col pitch, 12.00 mm insulator length, 20.32 X 5.08 X 12.00 mm body
&lt;p&gt;Single-row (1X4), 4-pin Receptacle Header (Female) Straight package with 5.08 mm (0.20 in) col pitch, 1.00 mm lead width, 3.00 mm tail length and 12.00 mm insulator length with overall size 20.32 X 5.08 X 12.00 mm, pin pattern - clockwise from top left&lt;/p&gt;</description>
<wire x1="19.05" y1="-4.15" x2="-3.81" y2="-4.15" width="0.3048" layer="21"/>
<wire x1="-3.81" y1="-4.15" x2="-3.81" y2="4.15" width="0.3048" layer="21"/>
<wire x1="-3.81" y1="4.15" x2="19.05" y2="4.15" width="0.3048" layer="21"/>
<wire x1="19.05" y1="4.15" x2="19.05" y2="-4.15" width="0.3048" layer="21"/>
<pad name="1" x="0" y="0" drill="1.6142" diameter="4.8426"/>
<pad name="2" x="5.08" y="0" drill="1.6142" diameter="4.8426"/>
<pad name="3" x="10.16" y="0" drill="1.6142" diameter="4.8426"/>
<pad name="4" x="15.24" y="0" drill="1.6142" diameter="4.8426"/>
<text x="0" y="4.6993" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-4.5803" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
</package>
</packages>
<packages3d>
<package3d name="QFP80P1200X1200X120-44" urn="urn:adsk.eagle:package:7630256/1" type="model">
<description>44-QFP, 0.80 mm pitch, 12.00 mm span, 10.00 X 10.00 X 1.20 mm body
&lt;p&gt;44-pin QFP package with 0.80 mm pitch, 12.00 mm lead span1 X 12.00 mm lead span2 with body size 10.00 X 10.00 X 1.20 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="QFP80P1200X1200X120-44"/>
</packageinstances>
</package3d>
<package3d name="HDRVR8W64P254_2X4_1016X508X254B" urn="urn:adsk.eagle:package:7643367/2" locally_modified="yes" type="model">
<description>Double-row, 8-pin Receptacle Header (Female) Straight, 2.54 mm (0.10 in) row pitch, 2.54 mm (0.10 in) col pitch, 2.54 mm insulator length, 10.16 X 5.08 X 2.54 mm body
&lt;p&gt;Double-row (2X4), 8-pin Receptacle Header (Female) Straight package with 2.54 mm (0.10 in) row pitch, 2.54 mm (0.10 in) col pitch, 0.64 mm lead width, 3.00 mm tail length and 2.54 mm insulator length with overall size 10.16 X 5.08 X 2.54 mm, pin pattern - clockwise from top left&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="HDRVR8W64P254_2X4_1016X508X254B"/>
</packageinstances>
</package3d>
<package3d name="SOP65P780X200-28" urn="urn:adsk.eagle:package:7639630/1" type="model">
<description>28-SOP, 0.65 mm pitch, 7.80 mm span, 10.20 X 5.30 X 2.00 mm body
&lt;p&gt;28-pin SOP package with 0.65 mm pitch, 7.80 mm span with body size 10.20 X 5.30 X 2.00 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="SOP65P780X200-28"/>
</packageinstances>
</package3d>
<package3d name="FUSC3216X50" urn="urn:adsk.eagle:package:7657273/1" type="model">
<description>Chip, 3.20 X 1.60 X 0.50 mm body
&lt;p&gt;Chip package with body size 3.20 X 1.60 X 0.50 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="FUSC3216X50"/>
</packageinstances>
</package3d>
<package3d name="HDRV2W100P508_1X2_1140X830X838B" urn="urn:adsk.eagle:package:7642726/2" type="model">
<description>Single-row, 2-pin Pin Header (Male) Straight, 5.08 mm (0.20 in) col pitch, 5.84 mm mating length, 11.40 X 8.30 X 8.38 mm body
&lt;p&gt;Single-row (1X2), 2-pin Pin Header (Male) Straight package with 5.08 mm (0.20 in) col pitch, 1.00 mm lead width, 3.00 mm tail length and 5.84 mm mating length with overall size 11.40 X 8.30 X 8.38 mm, pin pattern - clockwise from top left&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="HDRV2W100P508_1X2_1140X830X838B"/>
</packageinstances>
</package3d>
<package3d name="HDRV2W64P254_1X2_508X254X762B" urn="urn:adsk.eagle:package:7642735/1" locally_modified="yes" type="model">
<description>Single-row, 2-pin Pin Header (Male) Straight, 2.54 mm (0.10 in) col pitch, 5.08 mm mating length, 5.08 X 2.54 X 7.62 mm body
&lt;p&gt;Single-row (1X2), 2-pin Pin Header (Male) Straight package with 2.54 mm (0.10 in) col pitch, 0.64 mm lead width, 3.00 mm tail length and 5.08 mm mating length with overall size 5.08 X 2.54 X 7.62 mm, pin pattern - clockwise from top left&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="HDRV2W64P254_1X2_508X254X762B"/>
</packageinstances>
</package3d>
<package3d name="RESCAV127P508X210X70-8" urn="urn:adsk.eagle:package:7643962/2" locally_modified="yes" type="model">
<description>8-Chiparray 2-Side Flat, 1.27 mm pitch, 5.08 X 2.10 X 0.70 mm body
&lt;p&gt;8-pin Chiparray 2-Side Flat package with 1.27 mm pitch with body size 5.08 X 2.10 X 0.70 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESCAV127P508X210X70-8"/>
</packageinstances>
</package3d>
<package3d name="SOIC127P600X175-8" urn="urn:adsk.eagle:package:7656496/1" type="model">
<description>8-SOIC, 1.27 mm pitch, 6.00 mm span, 4.92 X 3.95 X 1.75 mm body
&lt;p&gt;8-pin SOIC package with 1.27 mm pitch, 6.00 mm span with body size 4.92 X 3.95 X 1.75 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="SOIC127P600X175-8"/>
</packageinstances>
</package3d>
<package3d name="HDRV6W64P254_1X6_1524X254X838B" urn="urn:adsk.eagle:package:7657283/1" locally_modified="yes" type="model">
<description>Single-row, 6-pin Pin Header (Male) Straight, 2.54 mm (0.10 in) col pitch, 5.84 mm mating length, 15.24 X 2.54 X 8.38 mm body
&lt;p&gt;Single-row (1X6), 6-pin Pin Header (Male) Straight package with 2.54 mm (0.10 in) col pitch, 0.64 mm lead width, 3.00 mm tail length and 5.84 mm mating length with overall size 15.24 X 2.54 X 8.38 mm, pin pattern - clockwise from top left&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="HDRV6W64P254_1X6_1524X254X838B"/>
</packageinstances>
</package3d>
<package3d name="HDRV5W64P254_1X5_1270X254X838B" urn="urn:adsk.eagle:package:7657286/1" locally_modified="yes" type="model">
<description>Single-row, 5-pin Pin Header (Male) Straight, 2.54 mm (0.10 in) col pitch, 5.84 mm mating length, 12.70 X 2.54 X 8.38 mm body
&lt;p&gt;Single-row (1X5), 5-pin Pin Header (Male) Straight package with 2.54 mm (0.10 in) col pitch, 0.64 mm lead width, 3.00 mm tail length and 5.84 mm mating length with overall size 12.70 X 2.54 X 8.38 mm, pin pattern - clockwise from top left&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="HDRV5W64P254_1X5_1270X254X838B"/>
</packageinstances>
</package3d>
<package3d name="HDRV3W64P254_1X3_762X254X838B" urn="urn:adsk.eagle:package:7657289/1" type="model">
<description>Single-row, 3-pin Pin Header (Male) Straight, 2.54 mm (0.10 in) col pitch, 5.84 mm mating length, 7.62 X 2.54 X 8.38 mm body
&lt;p&gt;Single-row (1X3), 3-pin Pin Header (Male) Straight package with 2.54 mm (0.10 in) col pitch, 0.64 mm lead width, 3.00 mm tail length and 5.84 mm mating length with overall size 7.62 X 2.54 X 8.38 mm, pin pattern - clockwise from top left&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="HDRV3W64P254_1X3_762X254X838B"/>
</packageinstances>
</package3d>
<package3d name="TO457P991X255-3" urn="urn:adsk.eagle:package:7648643/1" type="model">
<description>3-TO, DPAK, 4.57 mm pitch, 9.91 mm span, 6.54 X 6.09 X 2.55 mm body
&lt;p&gt;3-pin TO, DPAK package with 4.57 mm pitch, 9.91 mm span with body size 6.54 X 6.09 X 2.55 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="TO457P991X255-3"/>
</packageinstances>
</package3d>
<package3d name="SOT230P700X180-4" urn="urn:adsk.eagle:package:7654761/1" type="model">
<description>4-SOT223, 2.30 mm pitch, 7.00 mm span, 6.50 X 3.50 X 1.80 mm body
&lt;p&gt;4-pin SOT223 package with 2.30 mm pitch, 7.00 mm span with body size 6.50 X 3.50 X 1.80 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="SOT230P700X180-4"/>
</packageinstances>
</package3d>
<package3d name="CAPAE660X565" urn="urn:adsk.eagle:package:7654791/1" type="model">
<description>ECAP (Aluminum Electrolytic Capacitor), 6.60 X 5.65 mm body
&lt;p&gt;ECAP (Aluminum Electrolytic Capacitor) package with body size 6.60 X 5.65 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPAE660X565"/>
</packageinstances>
</package3d>
<package3d name="CAPPAD1070W10L850D250B" urn="urn:adsk.eagle:package:7657502/2" locally_modified="yes" type="model">
<description>Axial Polarized capacitor, 10.70 mm pitch, 8.50 mm body length, 2.50 mm body diameter
&lt;p&gt;Axial Polarized capacitor package with 10.70 mm pitch, 0.10 mm lead diameter, 8.50 mm body length and 2.50 mm body diameter&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPPAD1070W10L850D250B"/>
</packageinstances>
</package3d>
<package3d name="CAPC1608X50" urn="urn:adsk.eagle:package:7642693/1" locally_modified="yes" type="model">
<description>Chip, 1.60 X 0.80 X 0.50 mm body
&lt;p&gt;Chip package with body size 1.60 X 0.80 X 0.50 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC1608X50"/>
</packageinstances>
</package3d>
<package3d name="CAPC2012X70" urn="urn:adsk.eagle:package:7642704/1" locally_modified="yes" type="model">
<description>Chip, 2.00 X 1.25 X 0.70 mm body
&lt;p&gt;Chip package with body size 2.00 X 1.25 X 0.70 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPC2012X70"/>
</packageinstances>
</package3d>
<package3d name="DIOM6859X262" urn="urn:adsk.eagle:package:7707351/1" type="model">
<description>Molded Body, 6.86 X 5.91 X 2.62 mm body
&lt;p&gt;Molded Body package with body size 6.86 X 5.91 X 2.62 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="DIOM6859X262"/>
</packageinstances>
</package3d>
<package3d name="DIOM4226X242" urn="urn:adsk.eagle:package:7707360/1" type="model">
<description>Molded Body, 4.25 X 2.67 X 2.42 mm body
&lt;p&gt;Molded Body package with body size 4.25 X 2.67 X 2.42 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="DIOM4226X242"/>
</packageinstances>
</package3d>
<package3d name="RESC2012X65" urn="urn:adsk.eagle:package:7640128/1" locally_modified="yes" type="model">
<description>Chip, 2.00 X 1.25 X 0.65 mm body
&lt;p&gt;Chip package with body size 2.00 X 1.25 X 0.65 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESC2012X65"/>
</packageinstances>
</package3d>
<package3d name="RESC3216X70" urn="urn:adsk.eagle:package:7642712/1" locally_modified="yes" type="model">
<description>Chip, 3.20 X 1.60 X 0.70 mm body
&lt;p&gt;Chip package with body size 3.20 X 1.60 X 0.70 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="RESC3216X70"/>
</packageinstances>
</package3d>
<package3d name="LEDC2012X110" urn="urn:adsk.eagle:package:7707399/2" type="model">
<description>Chip, 2.00 X 1.25 X 1.10 mm body
&lt;p&gt;Chip package with body size 2.00 X 1.25 X 1.10 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="LEDC2012X110"/>
</packageinstances>
</package3d>
<package3d name="DIOM1208X70" urn="urn:adsk.eagle:package:7707409/1" type="model">
<description>Molded Body, 1.20 X 0.80 X 0.70 mm body
&lt;p&gt;Molded Body package with body size 1.20 X 0.80 X 0.70 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="DIOM1208X70"/>
</packageinstances>
</package3d>
<package3d name="SOT95P240X110-3" urn="urn:adsk.eagle:package:7655736/1" type="model">
<description>3-SOT23, 0.95 mm pitch, 2.40 mm span, 2.90 X 1.30 X 1.10 mm body
&lt;p&gt;3-pin SOT23 package with 0.95 mm pitch, 2.40 mm span with body size 2.90 X 1.30 X 1.10 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="SOT95P240X110-3"/>
</packageinstances>
</package3d>
<package3d name="HDRV8W64P254_2X4_1016X508X838B" urn="urn:adsk.eagle:package:7657587/2" locally_modified="yes" type="model">
<description>Double-row, 8-pin Pin Header (Male) Straight, 2.54 mm (0.10 in) row pitch, 2.54 mm (0.10 in) col pitch, 5.84 mm mating length, 10.16 X 5.08 X 8.38 mm body
&lt;p&gt;Double-row (2X4), 8-pin Pin Header (Male) Straight package with 2.54 mm (0.10 in) row pitch, 2.54 mm (0.10 in) col pitch, 0.64 mm lead width, 3.00 mm tail length and 5.84 mm mating length with overall size 10.16 X 5.08 X 8.38 mm, pin pattern - clockwise from top left&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="HDRV8W64P254_2X4_1016X508X838B"/>
</packageinstances>
</package3d>
<package3d name="HDRV6W64P254_2X3_762X508X838B" urn="urn:adsk.eagle:package:7707326/3" locally_modified="yes" type="model">
<description>Double-row, 6-pin Pin Header (Male) Straight, 2.54 mm (0.10 in) row pitch, 2.54 mm (0.10 in) col pitch, 5.84 mm mating length, 7.62 X 5.08 X 8.38 mm body
&lt;p&gt;Double-row (2X3), 6-pin Pin Header (Male) Straight package with 2.54 mm (0.10 in) row pitch, 2.54 mm (0.10 in) col pitch, 0.64 mm lead width, 3.00 mm tail length and 5.84 mm mating length with overall size 7.62 X 5.08 X 8.38 mm, pin pattern - clockwise from top left&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="HDRV6W64P254_2X3_762X508X838B"/>
</packageinstances>
</package3d>
<package3d name="HDRV2W120P2260_1X2_2260X900X1554B" urn="urn:adsk.eagle:package:7654933/2" locally_modified="yes" type="model">
<description>Single-row, 2-pin Pin Header (Male) Straight, 22.60 mm (0.89 in) col pitch, 5.84 mm mating length, 22.60 X 9.00 X 15.54 mm body
&lt;p&gt;Single-row (1X2), 2-pin Pin Header (Male) Straight package with 22.60 mm (0.89 in) col pitch, 1.20 mm lead width, 3.00 mm tail length and 5.84 mm mating length with overall size 22.60 X 9.00 X 15.54 mm, pin pattern - clockwise from top left&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="HDRV2W120P2260_1X2_2260X900X1554B"/>
</packageinstances>
</package3d>
<package3d name="HDRV4W64P254_1X4_1016X254X838B" urn="urn:adsk.eagle:package:7706676/1" locally_modified="yes" type="model">
<description>Single-row, 4-pin Pin Header (Male) Straight, 2.54 mm (0.10 in) col pitch, 5.84 mm mating length, 10.16 X 2.54 X 8.38 mm body
&lt;p&gt;Single-row (1X4), 4-pin Pin Header (Male) Straight package with 2.54 mm (0.10 in) col pitch, 0.64 mm lead width, 3.00 mm tail length and 5.84 mm mating length with overall size 10.16 X 2.54 X 8.38 mm, pin pattern - clockwise from top left&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="HDRV4W64P254_1X4_1016X254X838B"/>
</packageinstances>
</package3d>
<package3d name="HDRVR6W100P508_1X6_3048X508X254B" urn="urn:adsk.eagle:package:7708557/2" type="model">
<description>Single-row, 6-pin Receptacle Header (Female) Straight, 5.08 mm (0.20 in) col pitch, 2.54 mm insulator length, 30.48 X 5.08 X 2.54 mm body
&lt;p&gt;Single-row (1X6), 6-pin Receptacle Header (Female) Straight package with 5.08 mm (0.20 in) col pitch, 1.00 mm lead width, 3.00 mm tail length and 2.54 mm insulator length with overall size 30.48 X 5.08 X 2.54 mm, pin pattern - clockwise from top left&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="HDRVR6W100P508_1X6_3048X508X254B"/>
</packageinstances>
</package3d>
<package3d name="HDRV4W100P450X650_2X2_600X500X360B" urn="urn:adsk.eagle:package:7707443/2" locally_modified="yes" type="model">
<description>Double-row, 4-pin Pin Header (Male) Straight, 4.50 mm (0.18 in) pitch, 0.10 mm mating length, 6.00 X 5.00 X 3.60 mm body
&lt;p&gt;Double-row (2X2), 4-pin Pin Header (Male) Straight package with 4.50 mm (0.18 in) pitch, 1.00 mm lead width, 3.00 mm tail length and 0.10 mm mating length with overall size 6.00 X 5.00 X 3.60 mm, pin pattern - clockwise from top left&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="HDRV4W100P450X650_2X2_600X500X360B"/>
</packageinstances>
</package3d>
<package3d name="SOT95P240X110-5" urn="urn:adsk.eagle:package:9867714/1" type="model">
<description>5-SOT23, 0.95 mm pitch, 2.40 mm span, 2.90 X 1.30 X 1.10 mm body
&lt;p&gt;5-pin SOT23 package with 0.95 mm pitch, 2.40 mm span with body size 2.90 X 1.30 X 1.10 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="SOT95P240X110-5"/>
</packageinstances>
</package3d>
<package3d name="CAPAE1030X1050" urn="urn:adsk.eagle:package:10452068/1" type="model">
<description>ECAP (Aluminum Electrolytic Capacitor), 10.30 X 10.50 mm body
&lt;p&gt;ECAP (Aluminum Electrolytic Capacitor) package with body size 10.30 X 10.50 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="CAPAE1030X1050"/>
</packageinstances>
</package3d>
<package3d name="TO228P998X240-3" urn="urn:adsk.eagle:package:10452077/1" locally_modified="yes" type="model">
<description>3-TO, DPAK, 2.28 mm pitch, 9.98 mm span, 6.55 X 6.32 X 2.40 mm body
&lt;p&gt;3-pin TO, DPAK package with 2.28 mm pitch, 9.98 mm span with body size 6.55 X 6.32 X 2.40 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="TO228P998X240-3"/>
</packageinstances>
</package3d>
<package3d name="SOP100P1900X235-30" urn="urn:adsk.eagle:package:7638378/1" locally_modified="yes" type="model">
<description>30-SOP, 1.00 mm pitch, 19.00 mm span, 17.20 X 16.00 X 2.35 mm body
&lt;p&gt;30-pin SOP package with 1.00 mm pitch, 19.00 mm span with body size 17.20 X 16.00 X 2.35 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="SOP100P1900X235-30"/>
</packageinstances>
</package3d>
<package3d name="HDRVR4W100P508_1X4_2032X508X1200B" urn="urn:adsk.eagle:package:10452165/2" locally_modified="yes" type="model">
<description>Single-row, 4-pin Receptacle Header (Female) Straight, 5.08 mm (0.20 in) col pitch, 12.00 mm insulator length, 20.32 X 5.08 X 12.00 mm body
&lt;p&gt;Single-row (1X4), 4-pin Receptacle Header (Female) Straight package with 5.08 mm (0.20 in) col pitch, 1.00 mm lead width, 3.00 mm tail length and 12.00 mm insulator length with overall size 20.32 X 5.08 X 12.00 mm, pin pattern - clockwise from top left&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="HDRVR4W100P508_1X4_2032X508X1200B"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="DSPIC33FJ128MC804">
<pin name="SDA1/RP9/CN21/PMD3/RB9" x="-55.88" y="53.34" length="middle"/>
<pin name="PWM2H1/RP22/CN18/PMA1/RC6" x="-55.88" y="48.26" length="middle"/>
<pin name="PWM2L1/RP23/CN17/PMA0/RC7" x="-55.88" y="43.18" length="middle"/>
<pin name="RP24/CN20/PMA5/RC8" x="-55.88" y="38.1" length="middle"/>
<pin name="RP25/CN19/PMA6/RC9" x="-55.88" y="33.02" length="middle"/>
<pin name="VSS" x="-55.88" y="27.94" length="middle"/>
<pin name="VCAP" x="-55.88" y="22.86" length="middle"/>
<pin name="PGED2/EMCD2/PWM1H3/RP10/CN16/PMD2/RB10" x="-55.88" y="17.78" length="middle"/>
<pin name="PGEC2/PWM1L3/RP11/CN15/PMD1/RB11" x="-55.88" y="12.7" length="middle"/>
<pin name="PWM1H2/DAC1RP/RP12/CN14/PMD0/RB12" x="-55.88" y="7.62" length="middle"/>
<pin name="PWM1L2/DAC1RN/RP13/CN13/PMRD/RB13" x="-55.88" y="2.54" length="middle"/>
<pin name="TMS/PMA10/RA10" x="-55.88" y="-2.54" length="middle"/>
<pin name="TCK/PMA7/RA7" x="-55.88" y="-7.62" length="middle"/>
<pin name="PWM1H1/DAC1LP/RTCC/RP14/CN12/PMWR/RB14" x="-55.88" y="-12.7" length="middle"/>
<pin name="PWM1L1/DAC1LN/RP15/CN11/PMCS1/RB15" x="-55.88" y="-17.78" length="middle"/>
<pin name="AVSS" x="-55.88" y="-22.86" length="middle"/>
<pin name="AVDD" x="-55.88" y="-27.94" length="middle"/>
<pin name="_MCLR" x="-55.88" y="-33.02" length="middle"/>
<pin name="AN0/VREF+/CN2/RA0" x="-55.88" y="-38.1" length="middle"/>
<pin name="AN1/VREF-/CN3/RA1" x="-55.88" y="-43.18" length="middle"/>
<pin name="PGED1/AN2/C2IN-/RP0/CN4/RB0" x="-55.88" y="-48.26" length="middle"/>
<pin name="PGEC1/AN3/C2IN+/RP1/CN5/RB1" x="-55.88" y="-53.34" length="middle"/>
<pin name="AN4/C1IN-/RP2/CN6/RB2" x="58.42" y="-53.34" length="middle" rot="R180"/>
<pin name="AN5/C1IN+/RP3/CN7/RB3" x="58.42" y="-48.26" length="middle" rot="R180"/>
<pin name="AN6/DAC1RM/RP16/CN8/RC0" x="58.42" y="-43.18" length="middle" rot="R180"/>
<pin name="AN7/DAC1LM/RP17/CN9/RC1" x="58.42" y="-38.1" length="middle" rot="R180"/>
<pin name="AN8/CVREF/RP18/PMA2/CN10/RC2" x="58.42" y="-33.02" length="middle" rot="R180"/>
<pin name="VDD2" x="58.42" y="-27.94" length="middle" rot="R180"/>
<pin name="VSS2" x="58.42" y="-22.86" length="middle" rot="R180"/>
<pin name="OSC1/CLKI/CN30/RA2" x="58.42" y="-17.78" length="middle" rot="R180"/>
<pin name="OSC2/CLKO/CN29/RA3" x="58.42" y="-12.7" length="middle" rot="R180"/>
<pin name="TDO/PMA8/RA8" x="58.42" y="-7.62" length="middle" rot="R180"/>
<pin name="SOSCI/RP4/CN1/RB4" x="58.42" y="-2.54" length="middle" rot="R180"/>
<pin name="SOSCO/T1CK/CN0/RA4" x="58.42" y="2.54" length="middle" rot="R180"/>
<pin name="TDI/PMA9/RA9" x="58.42" y="7.62" length="middle" rot="R180"/>
<pin name="RP19/CN28/PMBE/RC3" x="58.42" y="12.7" length="middle" rot="R180"/>
<pin name="RP20/CN25/PMA4/RC4" x="58.42" y="17.78" length="middle" rot="R180"/>
<pin name="RP21/CN26/PMA3/RC5" x="58.42" y="22.86" length="middle" rot="R180"/>
<pin name="VSS3" x="58.42" y="27.94" length="middle" rot="R180"/>
<pin name="VDD3" x="58.42" y="33.02" length="middle" rot="R180"/>
<pin name="PGED3/ASDA1/RP5/CN27/PMD7/RB5" x="58.42" y="38.1" length="middle" rot="R180"/>
<pin name="PGEC3/ASCL1/RP6/CN24/PMD6/RB6" x="58.42" y="43.18" length="middle" rot="R180"/>
<pin name="INT0/RP7/CN23/PMD5/RB7" x="58.42" y="48.26" length="middle" rot="R180"/>
<pin name="SCL1/RP8/CN22/PMD4/RB8" x="58.42" y="53.34" length="middle" rot="R180"/>
<wire x1="-50.8" y1="55.88" x2="53.34" y2="55.88" width="0.254" layer="94"/>
<wire x1="53.34" y1="55.88" x2="53.34" y2="-55.88" width="0.254" layer="94"/>
<wire x1="53.34" y1="-55.88" x2="-50.8" y2="-55.88" width="0.254" layer="94"/>
<wire x1="-50.8" y1="-55.88" x2="-50.8" y2="55.88" width="0.254" layer="94"/>
</symbol>
<symbol name="FT232RL">
<pin name="TXD" x="22.86" y="30.48" length="middle" rot="R180"/>
<pin name="_DTR" x="22.86" y="10.16" length="middle" rot="R180"/>
<pin name="_RST" x="22.86" y="20.32" length="middle" rot="R180"/>
<pin name="VCCIO" x="-22.86" y="30.48" length="middle"/>
<pin name="RXD" x="22.86" y="25.4" length="middle" rot="R180"/>
<pin name="CBUS0" x="22.86" y="-10.16" length="middle" rot="R180"/>
<pin name="AGND" x="-5.08" y="-40.64" length="middle" rot="R90"/>
<pin name="_CTS" x="22.86" y="15.24" length="middle" rot="R180"/>
<pin name="_DSR" x="22.86" y="5.08" length="middle" rot="R180"/>
<pin name="_RI" x="22.86" y="-5.08" length="middle" rot="R180"/>
<pin name="CBUS1" x="22.86" y="-15.24" length="middle" rot="R180"/>
<pin name="CBUS2" x="22.86" y="-20.32" length="middle" rot="R180"/>
<pin name="CBUS3" x="22.86" y="-25.4" length="middle" rot="R180"/>
<pin name="CBUS4" x="22.86" y="-30.48" length="middle" rot="R180"/>
<pin name="GND" x="0" y="-40.64" length="middle" rot="R90"/>
<pin name="TEST" x="5.08" y="-40.64" length="middle" rot="R90"/>
<pin name="VCC" x="-22.86" y="25.4" length="middle"/>
<pin name="USBDM" x="-22.86" y="20.32" length="middle"/>
<pin name="USBDP" x="-22.86" y="15.24" length="middle"/>
<pin name="_RESET" x="-22.86" y="0" length="middle"/>
<pin name="OSCO" x="-22.86" y="-10.16" length="middle"/>
<pin name="OSCI" x="-22.86" y="-5.08" length="middle"/>
<pin name="3V3OUT" x="-22.86" y="-30.48" length="middle"/>
<wire x1="-17.78" y1="33.02" x2="17.78" y2="33.02" width="0.254" layer="94"/>
<wire x1="17.78" y1="33.02" x2="17.78" y2="-35.56" width="0.254" layer="94"/>
<wire x1="17.78" y1="-35.56" x2="-17.78" y2="-35.56" width="0.254" layer="94"/>
<wire x1="-17.78" y1="-35.56" x2="-17.78" y2="33.02" width="0.254" layer="94"/>
<pin name="_DCD" x="22.86" y="0" length="middle" rot="R180"/>
</symbol>
<symbol name="FUSE">
<wire x1="-3.81" y1="-0.762" x2="3.81" y2="-0.762" width="0.254" layer="94"/>
<wire x1="3.81" y1="0.762" x2="-3.81" y2="0.762" width="0.254" layer="94"/>
<wire x1="3.81" y1="-0.762" x2="3.81" y2="0.762" width="0.254" layer="94"/>
<wire x1="-3.81" y1="0.762" x2="-3.81" y2="-0.762" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<text x="-3.81" y="1.397" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-2.921" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="HEADER2">
<pin name="1" x="7.62" y="2.54" length="middle" rot="R180"/>
<pin name="2" x="7.62" y="-2.54" length="middle" rot="R180"/>
<wire x1="-2.54" y1="5.08" x2="2.54" y2="5.08" width="0.254" layer="94"/>
<wire x1="2.54" y1="5.08" x2="2.54" y2="-5.08" width="0.254" layer="94"/>
<wire x1="2.54" y1="-5.08" x2="-2.54" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="-2.54" y2="5.08" width="0.254" layer="94"/>
</symbol>
<symbol name="USB">
<pin name="GND" x="10.16" y="-10.16" length="middle" rot="R180"/>
<pin name="ID" x="10.16" y="-5.08" length="middle" rot="R180"/>
<pin name="D+" x="10.16" y="0" length="middle" rot="R180"/>
<pin name="D-" x="10.16" y="5.08" length="middle" rot="R180"/>
<pin name="VBUS" x="10.16" y="10.16" length="middle" rot="R180"/>
<wire x1="-5.08" y1="12.7" x2="-5.08" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-12.7" x2="5.08" y2="-12.7" width="0.254" layer="94"/>
<wire x1="5.08" y1="-12.7" x2="5.08" y2="12.7" width="0.254" layer="94"/>
<wire x1="5.08" y1="12.7" x2="-5.08" y2="12.7" width="0.254" layer="94"/>
</symbol>
<symbol name="HEADER6">
<pin name="1" x="7.62" y="12.7" length="middle" rot="R180"/>
<pin name="2" x="7.62" y="7.62" length="middle" rot="R180"/>
<pin name="3" x="7.62" y="2.54" length="middle" rot="R180"/>
<pin name="4" x="7.62" y="-2.54" length="middle" rot="R180"/>
<pin name="5" x="7.62" y="-7.62" length="middle" rot="R180"/>
<pin name="6" x="7.62" y="-12.7" length="middle" rot="R180"/>
<wire x1="-2.54" y1="15.24" x2="2.54" y2="15.24" width="0.254" layer="94"/>
<wire x1="2.54" y1="15.24" x2="2.54" y2="-15.24" width="0.254" layer="94"/>
<wire x1="2.54" y1="-15.24" x2="-2.54" y2="-15.24" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-15.24" x2="-2.54" y2="15.24" width="0.254" layer="94"/>
</symbol>
<symbol name="MAX485">
<pin name="RO" x="-15.24" y="7.62" length="middle"/>
<pin name="_RE" x="-15.24" y="2.54" length="middle"/>
<pin name="DE" x="-15.24" y="-2.54" length="middle"/>
<pin name="DI" x="-15.24" y="-7.62" length="middle"/>
<pin name="GND" x="15.24" y="-7.62" length="middle" rot="R180"/>
<pin name="A" x="15.24" y="-2.54" length="middle" rot="R180"/>
<pin name="B" x="15.24" y="2.54" length="middle" rot="R180"/>
<pin name="VCC" x="15.24" y="7.62" length="middle" rot="R180"/>
<wire x1="-10.16" y1="10.16" x2="10.16" y2="10.16" width="0.254" layer="94"/>
<wire x1="10.16" y1="10.16" x2="10.16" y2="-10.16" width="0.254" layer="94"/>
<wire x1="10.16" y1="-10.16" x2="-10.16" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-10.16" x2="-10.16" y2="10.16" width="0.254" layer="94"/>
</symbol>
<symbol name="HEADER5">
<pin name="1" x="7.62" y="10.16" length="middle" rot="R180"/>
<pin name="2" x="7.62" y="5.08" length="middle" rot="R180"/>
<pin name="3" x="7.62" y="0" length="middle" rot="R180"/>
<pin name="4" x="7.62" y="-5.08" length="middle" rot="R180"/>
<pin name="5" x="7.62" y="-10.16" length="middle" rot="R180"/>
<wire x1="-2.54" y1="12.7" x2="2.54" y2="12.7" width="0.254" layer="94"/>
<wire x1="2.54" y1="12.7" x2="2.54" y2="-12.7" width="0.254" layer="94"/>
<wire x1="2.54" y1="-12.7" x2="-2.54" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-12.7" x2="-2.54" y2="12.7" width="0.254" layer="94"/>
</symbol>
<symbol name="HEADER3">
<pin name="1" x="7.62" y="5.08" length="middle" rot="R180"/>
<pin name="2" x="7.62" y="0" length="middle" rot="R180"/>
<pin name="3" x="7.62" y="-5.08" length="middle" rot="R180"/>
<wire x1="-2.54" y1="7.62" x2="2.54" y2="7.62" width="0.254" layer="94"/>
<wire x1="2.54" y1="7.62" x2="2.54" y2="-7.62" width="0.254" layer="94"/>
<wire x1="2.54" y1="-7.62" x2="-2.54" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-7.62" x2="-2.54" y2="7.62" width="0.254" layer="94"/>
</symbol>
<symbol name="REGULATOR">
<pin name="VOUT" x="15.24" y="2.54" length="middle" rot="R180"/>
<pin name="GND" x="0" y="-10.16" length="middle" rot="R90"/>
<pin name="VIN" x="-15.24" y="2.54" length="middle"/>
<wire x1="-10.16" y1="5.08" x2="10.16" y2="5.08" width="0.254" layer="94"/>
<wire x1="10.16" y1="5.08" x2="10.16" y2="-5.08" width="0.254" layer="94"/>
<wire x1="10.16" y1="-5.08" x2="-10.16" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-5.08" x2="-10.16" y2="5.08" width="0.254" layer="94"/>
</symbol>
<symbol name="CPOL">
<wire x1="-1.524" y1="-0.889" x2="1.524" y2="-0.889" width="0.254" layer="94"/>
<wire x1="1.524" y1="-0.889" x2="1.524" y2="0" width="0.254" layer="94"/>
<wire x1="-1.524" y1="0" x2="-1.524" y2="-0.889" width="0.254" layer="94"/>
<wire x1="-1.524" y1="0" x2="1.524" y2="0" width="0.254" layer="94"/>
<text x="1.143" y="0.4826" size="1.778" layer="95">&gt;NAME</text>
<text x="-0.5842" y="0.4064" size="1.27" layer="94" rot="R90">+</text>
<text x="1.143" y="-4.5974" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-2.54" x2="1.651" y2="-1.651" layer="94"/>
<pin name="-" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="+" x="0" y="2.54" visible="off" length="short" direction="pas" rot="R270"/>
</symbol>
<symbol name="C">
<description>Capacitor</description>
<rectangle x1="-2.032" y1="-2.032" x2="2.032" y2="-1.524" layer="94"/>
<rectangle x1="-2.032" y1="-1.016" x2="2.032" y2="-0.508" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-2.032" width="0.1524" layer="94"/>
<pin name="1" x="0" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
<text x="2.54" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="0" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="DIODE">
<wire x1="-1.27" y1="-1.905" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.905" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.905" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<wire x1="1.397" y1="1.905" x2="1.397" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.3114" y="2.6416" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.5654" y="-4.4958" size="1.778" layer="96">&gt;VALUE</text>
<pin name="A" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<pin name="C" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="R">
<description>RESISTOR</description>
<wire x1="-2.54" y1="-0.889" x2="2.54" y2="-0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="-0.889" x2="2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<text x="-2.54" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="LED">
<wire x1="1.27" y1="0" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="-0.762" x2="-3.429" y2="-2.159" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="-1.905" x2="-3.302" y2="-3.302" width="0.1524" layer="94"/>
<text x="3.556" y="-4.572" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="5.715" y="-4.572" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="C" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="A" x="0" y="2.54" visible="off" length="short" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="-3.429" y="-2.159"/>
<vertex x="-3.048" y="-1.27"/>
<vertex x="-2.54" y="-1.778"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-3.302" y="-3.302"/>
<vertex x="-2.921" y="-2.413"/>
<vertex x="-2.413" y="-2.921"/>
</polygon>
</symbol>
<symbol name="HEADER4">
<pin name="1" x="7.62" y="7.62" length="middle" rot="R180"/>
<pin name="2" x="7.62" y="2.54" length="middle" rot="R180"/>
<pin name="3" x="7.62" y="-2.54" length="middle" rot="R180"/>
<pin name="4" x="7.62" y="-7.62" length="middle" rot="R180"/>
<wire x1="-2.54" y1="10.16" x2="2.54" y2="10.16" width="0.254" layer="94"/>
<wire x1="2.54" y1="10.16" x2="2.54" y2="-10.16" width="0.254" layer="94"/>
<wire x1="2.54" y1="-10.16" x2="-2.54" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-10.16" x2="-2.54" y2="10.16" width="0.254" layer="94"/>
</symbol>
<symbol name="TS">
<wire x1="0" y1="-3.175" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="3.175" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-0.635" y2="0" width="0.254" layer="94"/>
<wire x1="-6.35" y1="1.905" x2="-5.08" y2="1.905" width="0.254" layer="94"/>
<wire x1="-6.35" y1="1.905" x2="-6.35" y2="0" width="0.254" layer="94"/>
<wire x1="-6.35" y1="-1.905" x2="-5.08" y2="-1.905" width="0.254" layer="94"/>
<wire x1="-6.35" y1="0" x2="-4.445" y2="0" width="0.1524" layer="94"/>
<wire x1="-6.35" y1="0" x2="-6.35" y2="-1.905" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.905" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0" x2="-0.635" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.635" y1="0" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="-3.81" y1="0" x2="-3.175" y2="0" width="0.1524" layer="94"/>
<text x="-6.985" y="-1.905" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="-4.445" y="3.175" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="P" x="0" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="S" x="0" y="5.08" visible="pad" length="short" direction="pas" rot="R270"/>
</symbol>
<symbol name="NPN">
<wire x1="2.54" y1="2.54" x2="0.508" y2="1.524" width="0.1524" layer="94"/>
<wire x1="1.778" y1="-1.524" x2="2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="1.27" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="1.778" y2="-1.524" width="0.1524" layer="94"/>
<wire x1="1.54" y1="-2.04" x2="0.308" y2="-1.424" width="0.1524" layer="94"/>
<wire x1="1.524" y1="-2.413" x2="2.286" y2="-2.413" width="0.254" layer="94"/>
<wire x1="2.286" y1="-2.413" x2="1.778" y2="-1.778" width="0.254" layer="94"/>
<wire x1="1.778" y1="-1.778" x2="1.524" y2="-2.286" width="0.254" layer="94"/>
<wire x1="1.524" y1="-2.286" x2="1.905" y2="-2.286" width="0.254" layer="94"/>
<wire x1="1.905" y1="-2.286" x2="1.778" y2="-2.032" width="0.254" layer="94"/>
<text x="-10.16" y="7.62" size="1.778" layer="95">&gt;NAME</text>
<text x="-10.16" y="5.08" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-0.254" y1="-2.54" x2="0.508" y2="2.54" layer="94"/>
<pin name="B" x="-2.54" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="E" x="2.54" y="-5.08" visible="off" length="short" direction="pas" swaplevel="3" rot="R90"/>
<pin name="C" x="2.54" y="5.08" visible="off" length="short" direction="pas" swaplevel="2" rot="R270"/>
</symbol>
<symbol name="MAX40203">
<pin name="VDD" x="-7.62" y="5.08" length="short"/>
<pin name="GND" x="2.54" y="-7.62" length="short" rot="R90"/>
<pin name="EN" x="-7.62" y="0" length="short"/>
<pin name="OUT" x="12.7" y="5.08" length="short" rot="R180"/>
<wire x1="-5.08" y1="7.62" x2="10.16" y2="7.62" width="0.254" layer="94"/>
<wire x1="10.16" y1="7.62" x2="10.16" y2="-5.08" width="0.254" layer="94"/>
<wire x1="10.16" y1="-5.08" x2="-5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-5.08" x2="-5.08" y2="7.62" width="0.254" layer="94"/>
</symbol>
<symbol name="NMOS-FET-E">
<description>MOSFET N-channel - Enhancement mode</description>
<wire x1="0.762" y1="0.762" x2="0.762" y2="0" width="0.254" layer="94"/>
<wire x1="0.762" y1="0" x2="0.762" y2="-0.762" width="0.254" layer="94"/>
<wire x1="0.762" y1="3.175" x2="0.762" y2="2.54" width="0.254" layer="94"/>
<wire x1="0.762" y1="2.54" x2="0.762" y2="1.905" width="0.254" layer="94"/>
<wire x1="0.762" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0.762" y1="-1.905" x2="0.762" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0.762" y1="-2.54" x2="0.762" y2="-3.175" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="0.762" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="3.81" y1="2.54" x2="3.81" y2="0.508" width="0.1524" layer="94"/>
<wire x1="3.81" y1="0.508" x2="3.81" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="3.81" y1="-0.508" x2="3.81" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="3.81" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0.762" y1="2.54" x2="3.81" y2="2.54" width="0.1524" layer="94"/>
<wire x1="3.302" y1="0.508" x2="3.81" y2="0.508" width="0.1524" layer="94"/>
<wire x1="3.81" y1="0.508" x2="4.318" y2="0.508" width="0.1524" layer="94"/>
<circle x="2.54" y="-2.54" radius="0.254" width="0" layer="94"/>
<circle x="2.54" y="2.54" radius="0.254" width="0" layer="94"/>
<text x="-11.43" y="3.81" size="1.778" layer="96" rot="MR180">&gt;VALUE</text>
<text x="-11.43" y="1.27" size="1.778" layer="95" rot="MR180">&gt;NAME</text>
<pin name="S" x="2.54" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="G" x="-2.54" y="-2.54" visible="off" length="short" direction="pas"/>
<pin name="D" x="2.54" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="1.016" y="0"/>
<vertex x="2.032" y="0.508"/>
<vertex x="2.032" y="-0.508"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="3.81" y="0.508"/>
<vertex x="3.302" y="-0.254"/>
<vertex x="4.318" y="-0.254"/>
</polygon>
</symbol>
<symbol name="VNH5019">
<pin name="INA" x="-20.32" y="10.16" length="middle"/>
<pin name="INB" x="-20.32" y="5.08" length="middle"/>
<pin name="DIAGA/ENA" x="-20.32" y="0" length="middle"/>
<pin name="DIAGB/ENB" x="-20.32" y="-5.08" length="middle"/>
<pin name="PWM" x="-7.62" y="-20.32" length="middle" rot="R90"/>
<pin name="GNDA" x="5.08" y="-20.32" length="middle" rot="R90"/>
<pin name="GNDB" x="10.16" y="-20.32" length="middle" rot="R90"/>
<pin name="OUTA" x="20.32" y="10.16" length="middle" rot="R180"/>
<pin name="OUTB" x="20.32" y="5.08" length="middle" rot="R180"/>
<pin name="CS" x="20.32" y="0" length="middle" rot="R180"/>
<pin name="VBAT" x="0" y="20.32" length="middle" rot="R270"/>
<wire x1="-15.24" y1="15.24" x2="15.24" y2="15.24" width="0.254" layer="94"/>
<wire x1="15.24" y1="15.24" x2="15.24" y2="-15.24" width="0.254" layer="94"/>
<wire x1="15.24" y1="-15.24" x2="-15.24" y2="-15.24" width="0.254" layer="94"/>
<wire x1="-15.24" y1="-15.24" x2="-15.24" y2="15.24" width="0.254" layer="94"/>
<pin name="CS_DIS" x="20.32" y="-5.08" length="middle" rot="R180"/>
<pin name="CP" x="-5.08" y="20.32" length="middle" rot="R270"/>
<pin name="VCC" x="5.08" y="20.32" length="middle" rot="R270"/>
</symbol>
<symbol name="FERRITE">
<pin name="P$1" x="-5.207" y="0" visible="off" length="short"/>
<pin name="P$2" x="5.207" y="0" visible="off" length="short" rot="R180"/>
<wire x1="2.667" y1="0.762" x2="2.667" y2="-0.762" width="0.254" layer="94"/>
<wire x1="2.667" y1="-0.762" x2="-2.667" y2="-0.762" width="0.254" layer="94"/>
<wire x1="-2.667" y1="-0.762" x2="-2.667" y2="0.762" width="0.254" layer="94"/>
<wire x1="-2.667" y1="0.762" x2="2.667" y2="0.762" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="DSPIC33FJ128MC804">
<gates>
<gate name="G$1" symbol="DSPIC33FJ128MC804" x="0" y="0"/>
</gates>
<devices>
<device name="TQFP44" package="QFP80P1200X1200X120-44">
<connects>
<connect gate="G$1" pin="AN0/VREF+/CN2/RA0" pad="19"/>
<connect gate="G$1" pin="AN1/VREF-/CN3/RA1" pad="20"/>
<connect gate="G$1" pin="AN4/C1IN-/RP2/CN6/RB2" pad="23"/>
<connect gate="G$1" pin="AN5/C1IN+/RP3/CN7/RB3" pad="24"/>
<connect gate="G$1" pin="AN6/DAC1RM/RP16/CN8/RC0" pad="25"/>
<connect gate="G$1" pin="AN7/DAC1LM/RP17/CN9/RC1" pad="26"/>
<connect gate="G$1" pin="AN8/CVREF/RP18/PMA2/CN10/RC2" pad="27"/>
<connect gate="G$1" pin="AVDD" pad="17"/>
<connect gate="G$1" pin="AVSS" pad="16"/>
<connect gate="G$1" pin="INT0/RP7/CN23/PMD5/RB7" pad="43"/>
<connect gate="G$1" pin="OSC1/CLKI/CN30/RA2" pad="30"/>
<connect gate="G$1" pin="OSC2/CLKO/CN29/RA3" pad="31"/>
<connect gate="G$1" pin="PGEC1/AN3/C2IN+/RP1/CN5/RB1" pad="22"/>
<connect gate="G$1" pin="PGEC2/PWM1L3/RP11/CN15/PMD1/RB11" pad="9"/>
<connect gate="G$1" pin="PGEC3/ASCL1/RP6/CN24/PMD6/RB6" pad="42"/>
<connect gate="G$1" pin="PGED1/AN2/C2IN-/RP0/CN4/RB0" pad="21"/>
<connect gate="G$1" pin="PGED2/EMCD2/PWM1H3/RP10/CN16/PMD2/RB10" pad="8"/>
<connect gate="G$1" pin="PGED3/ASDA1/RP5/CN27/PMD7/RB5" pad="41"/>
<connect gate="G$1" pin="PWM1H1/DAC1LP/RTCC/RP14/CN12/PMWR/RB14" pad="14"/>
<connect gate="G$1" pin="PWM1H2/DAC1RP/RP12/CN14/PMD0/RB12" pad="10"/>
<connect gate="G$1" pin="PWM1L1/DAC1LN/RP15/CN11/PMCS1/RB15" pad="15"/>
<connect gate="G$1" pin="PWM1L2/DAC1RN/RP13/CN13/PMRD/RB13" pad="11"/>
<connect gate="G$1" pin="PWM2H1/RP22/CN18/PMA1/RC6" pad="2"/>
<connect gate="G$1" pin="PWM2L1/RP23/CN17/PMA0/RC7" pad="3"/>
<connect gate="G$1" pin="RP19/CN28/PMBE/RC3" pad="36"/>
<connect gate="G$1" pin="RP20/CN25/PMA4/RC4" pad="37"/>
<connect gate="G$1" pin="RP21/CN26/PMA3/RC5" pad="38"/>
<connect gate="G$1" pin="RP24/CN20/PMA5/RC8" pad="4"/>
<connect gate="G$1" pin="RP25/CN19/PMA6/RC9" pad="5"/>
<connect gate="G$1" pin="SCL1/RP8/CN22/PMD4/RB8" pad="44"/>
<connect gate="G$1" pin="SDA1/RP9/CN21/PMD3/RB9" pad="1"/>
<connect gate="G$1" pin="SOSCI/RP4/CN1/RB4" pad="33"/>
<connect gate="G$1" pin="SOSCO/T1CK/CN0/RA4" pad="34"/>
<connect gate="G$1" pin="TCK/PMA7/RA7" pad="13"/>
<connect gate="G$1" pin="TDI/PMA9/RA9" pad="35"/>
<connect gate="G$1" pin="TDO/PMA8/RA8" pad="32"/>
<connect gate="G$1" pin="TMS/PMA10/RA10" pad="12"/>
<connect gate="G$1" pin="VCAP" pad="7"/>
<connect gate="G$1" pin="VDD2" pad="28"/>
<connect gate="G$1" pin="VDD3" pad="40"/>
<connect gate="G$1" pin="VSS" pad="6"/>
<connect gate="G$1" pin="VSS2" pad="29"/>
<connect gate="G$1" pin="VSS3" pad="39"/>
<connect gate="G$1" pin="_MCLR" pad="18"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7630256/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="UNITEC_BOARD" package="HDRVR8W64P254_2X4_1016X508X254B">
<connects>
<connect gate="G$1" pin="AN0/VREF+/CN2/RA0" pad="P$19"/>
<connect gate="G$1" pin="AN1/VREF-/CN3/RA1" pad="P$20"/>
<connect gate="G$1" pin="AN4/C1IN-/RP2/CN6/RB2" pad="P$23"/>
<connect gate="G$1" pin="AN5/C1IN+/RP3/CN7/RB3" pad="P$24"/>
<connect gate="G$1" pin="AN6/DAC1RM/RP16/CN8/RC0" pad="P$25"/>
<connect gate="G$1" pin="AN7/DAC1LM/RP17/CN9/RC1" pad="P$26"/>
<connect gate="G$1" pin="AN8/CVREF/RP18/PMA2/CN10/RC2" pad="P$27"/>
<connect gate="G$1" pin="AVDD" pad="P$17"/>
<connect gate="G$1" pin="AVSS" pad="P$16"/>
<connect gate="G$1" pin="INT0/RP7/CN23/PMD5/RB7" pad="P$43"/>
<connect gate="G$1" pin="OSC1/CLKI/CN30/RA2" pad="P$30"/>
<connect gate="G$1" pin="OSC2/CLKO/CN29/RA3" pad="P$31"/>
<connect gate="G$1" pin="PGEC1/AN3/C2IN+/RP1/CN5/RB1" pad="P$22"/>
<connect gate="G$1" pin="PGEC2/PWM1L3/RP11/CN15/PMD1/RB11" pad="P$9"/>
<connect gate="G$1" pin="PGEC3/ASCL1/RP6/CN24/PMD6/RB6" pad="P$42"/>
<connect gate="G$1" pin="PGED1/AN2/C2IN-/RP0/CN4/RB0" pad="P$21"/>
<connect gate="G$1" pin="PGED2/EMCD2/PWM1H3/RP10/CN16/PMD2/RB10" pad="P$8"/>
<connect gate="G$1" pin="PGED3/ASDA1/RP5/CN27/PMD7/RB5" pad="P$41"/>
<connect gate="G$1" pin="PWM1H1/DAC1LP/RTCC/RP14/CN12/PMWR/RB14" pad="P$14"/>
<connect gate="G$1" pin="PWM1H2/DAC1RP/RP12/CN14/PMD0/RB12" pad="P$10"/>
<connect gate="G$1" pin="PWM1L1/DAC1LN/RP15/CN11/PMCS1/RB15" pad="P$15"/>
<connect gate="G$1" pin="PWM1L2/DAC1RN/RP13/CN13/PMRD/RB13" pad="P$11"/>
<connect gate="G$1" pin="PWM2H1/RP22/CN18/PMA1/RC6" pad="P$2"/>
<connect gate="G$1" pin="PWM2L1/RP23/CN17/PMA0/RC7" pad="P$3"/>
<connect gate="G$1" pin="RP19/CN28/PMBE/RC3" pad="P$36"/>
<connect gate="G$1" pin="RP20/CN25/PMA4/RC4" pad="P$37"/>
<connect gate="G$1" pin="RP21/CN26/PMA3/RC5" pad="P$38"/>
<connect gate="G$1" pin="RP24/CN20/PMA5/RC8" pad="P$4"/>
<connect gate="G$1" pin="RP25/CN19/PMA6/RC9" pad="P$5"/>
<connect gate="G$1" pin="SCL1/RP8/CN22/PMD4/RB8" pad="P$44"/>
<connect gate="G$1" pin="SDA1/RP9/CN21/PMD3/RB9" pad="P$1"/>
<connect gate="G$1" pin="SOSCI/RP4/CN1/RB4" pad="P$33"/>
<connect gate="G$1" pin="SOSCO/T1CK/CN0/RA4" pad="P$34"/>
<connect gate="G$1" pin="TCK/PMA7/RA7" pad="P$13"/>
<connect gate="G$1" pin="TDI/PMA9/RA9" pad="P$35"/>
<connect gate="G$1" pin="TDO/PMA8/RA8" pad="P$32"/>
<connect gate="G$1" pin="TMS/PMA10/RA10" pad="P$12"/>
<connect gate="G$1" pin="VCAP" pad="P$7"/>
<connect gate="G$1" pin="VDD2" pad="P$28"/>
<connect gate="G$1" pin="VDD3" pad="P$40"/>
<connect gate="G$1" pin="VSS" pad="P$6"/>
<connect gate="G$1" pin="VSS2" pad="P$29"/>
<connect gate="G$1" pin="VSS3" pad="P$39"/>
<connect gate="G$1" pin="_MCLR" pad="P$18"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7643367/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="FT232RL">
<gates>
<gate name="G$1" symbol="FT232RL" x="0" y="0"/>
</gates>
<devices>
<device name="SSOP28" package="SOP65P780X200-28">
<connects>
<connect gate="G$1" pin="3V3OUT" pad="17"/>
<connect gate="G$1" pin="AGND" pad="25"/>
<connect gate="G$1" pin="CBUS0" pad="23"/>
<connect gate="G$1" pin="CBUS1" pad="22"/>
<connect gate="G$1" pin="CBUS2" pad="13"/>
<connect gate="G$1" pin="CBUS3" pad="14"/>
<connect gate="G$1" pin="CBUS4" pad="12"/>
<connect gate="G$1" pin="GND" pad="7 18 21"/>
<connect gate="G$1" pin="OSCI" pad="27"/>
<connect gate="G$1" pin="OSCO" pad="28"/>
<connect gate="G$1" pin="RXD" pad="5"/>
<connect gate="G$1" pin="TEST" pad="26"/>
<connect gate="G$1" pin="TXD" pad="1"/>
<connect gate="G$1" pin="USBDM" pad="16"/>
<connect gate="G$1" pin="USBDP" pad="15"/>
<connect gate="G$1" pin="VCC" pad="20"/>
<connect gate="G$1" pin="VCCIO" pad="4"/>
<connect gate="G$1" pin="_CTS" pad="11"/>
<connect gate="G$1" pin="_DCD" pad="10"/>
<connect gate="G$1" pin="_DSR" pad="9"/>
<connect gate="G$1" pin="_DTR" pad="2"/>
<connect gate="G$1" pin="_RESET" pad="19"/>
<connect gate="G$1" pin="_RI" pad="6"/>
<connect gate="G$1" pin="_RST" pad="3"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7639630/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="FUSE">
<gates>
<gate name="G$1" symbol="FUSE" x="0" y="0"/>
</gates>
<devices>
<device name="20X5MM" package="HDRV2W120P2260_1X2_2260X900X1554B">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7654933/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="FUSC3216X50">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7657273/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="HEADER2">
<gates>
<gate name="G$1" symbol="HEADER2" x="0" y="0"/>
</gates>
<devices>
<device name="5.08MM-POWER" package="HDRV2W100P508_1X2_1140X830X838B">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7642726/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2.54MM-MALE" package="HDRV2W64P254_1X2_508X254X762B">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7642735/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="USB">
<gates>
<gate name="G$1" symbol="USB" x="0" y="0"/>
</gates>
<devices>
<device name="MICRO" package="RESCAV127P508X210X70-8">
<connects>
<connect gate="G$1" pin="D+" pad="D+"/>
<connect gate="G$1" pin="D-" pad="D-"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="ID" pad="ID"/>
<connect gate="G$1" pin="VBUS" pad="VCC"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7643962/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MAX485">
<gates>
<gate name="G$1" symbol="MAX485" x="0" y="0"/>
</gates>
<devices>
<device name="SO8" package="SOIC127P600X175-8">
<connects>
<connect gate="G$1" pin="A" pad="6"/>
<connect gate="G$1" pin="B" pad="7"/>
<connect gate="G$1" pin="DE" pad="3"/>
<connect gate="G$1" pin="DI" pad="4"/>
<connect gate="G$1" pin="GND" pad="5"/>
<connect gate="G$1" pin="RO" pad="1"/>
<connect gate="G$1" pin="VCC" pad="8"/>
<connect gate="G$1" pin="_RE" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7656496/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="HEADER6">
<gates>
<gate name="G$1" symbol="HEADER6" x="0" y="0"/>
</gates>
<devices>
<device name="2.54MM-MALE" package="HDRV6W64P254_1X6_1524X254X838B">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7657283/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MSTBVA2,5HC/6-G-5,08" package="HDRVR6W100P508_1X6_3048X508X254B">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7708557/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="HEADER5">
<gates>
<gate name="G$1" symbol="HEADER5" x="0" y="0"/>
</gates>
<devices>
<device name="2.54MM-MALE" package="HDRV5W64P254_1X5_1270X254X838B">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7657286/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="HEADER3">
<gates>
<gate name="G$1" symbol="HEADER3" x="0" y="0"/>
</gates>
<devices>
<device name="2.54MM-MALE" package="HDRV3W64P254_1X3_762X254X838B">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7657289/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LM1117">
<gates>
<gate name="G$1" symbol="REGULATOR" x="0" y="0"/>
</gates>
<devices>
<device name="TO252" package="TO457P991X255-3">
<connects>
<connect gate="G$1" pin="GND" pad="1"/>
<connect gate="G$1" pin="VIN" pad="2"/>
<connect gate="G$1" pin="VOUT" pad="3"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7648643/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOT223" package="SOT230P700X180-4">
<connects>
<connect gate="G$1" pin="GND" pad="1"/>
<connect gate="G$1" pin="VIN" pad="3"/>
<connect gate="G$1" pin="VOUT" pad="2 4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7654761/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAPACITORP">
<gates>
<gate name="G$1" symbol="CPOL" x="0" y="2.54"/>
</gates>
<devices>
<device name="6.3MM" package="CAPAE660X565">
<connects>
<connect gate="G$1" pin="+" pad="1"/>
<connect gate="G$1" pin="-" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7654791/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="16MM" package="CAPPAD1070W10L850D250B">
<connects>
<connect gate="G$1" pin="+" pad="2"/>
<connect gate="G$1" pin="-" pad="GND"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7657502/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="10MM" package="CAPAE1030X1050">
<connects>
<connect gate="G$1" pin="+" pad="1"/>
<connect gate="G$1" pin="-" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:10452068/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAPACITOR">
<gates>
<gate name="G$1" symbol="C" x="0" y="0"/>
</gates>
<devices>
<device name="0603" package="CAPC1608X50">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7642693/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="CAPC2012X70">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7642704/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DIODE">
<gates>
<gate name="G$1" symbol="DIODE" x="0" y="0"/>
</gates>
<devices>
<device name="DO-214AB/SMC" package="DIOM6859X262">
<connects>
<connect gate="G$1" pin="A" pad="2"/>
<connect gate="G$1" pin="C" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7707351/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="DO-214AC/SMA" package="DIOM4226X242">
<connects>
<connect gate="G$1" pin="A" pad="2"/>
<connect gate="G$1" pin="C" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7707360/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOD-523" package="DIOM1208X70">
<connects>
<connect gate="G$1" pin="A" pad="2"/>
<connect gate="G$1" pin="C" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7707409/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RESISTOR">
<gates>
<gate name="G$1" symbol="R" x="0" y="0"/>
</gates>
<devices>
<device name="0805" package="RESC2012X65">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7640128/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="RESC3216X70">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7642712/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LED">
<gates>
<gate name="G$1" symbol="LED" x="0" y="0"/>
</gates>
<devices>
<device name="0805" package="LEDC2012X110">
<connects>
<connect gate="G$1" pin="A" pad="2"/>
<connect gate="G$1" pin="C" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7707399/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RJ11">
<gates>
<gate name="G$1" symbol="HEADER6" x="0" y="0"/>
</gates>
<devices>
<device name="" package="HDRV8W64P254_2X4_1016X508X838B">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7657587/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="NICE-3D" package="HDRV6W64P254_2X3_762X508X838B">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7707326/3"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="HEADER4">
<gates>
<gate name="G$1" symbol="HEADER4" x="0" y="0"/>
</gates>
<devices>
<device name="MALE2.54MM" package="HDRV4W64P254_1X4_1016X254X838B">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7706676/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="5.08MM" package="HDRVR4W100P508_1X4_2032X508X1200B">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:10452165/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="BUTTON">
<gates>
<gate name="G$1" symbol="TS" x="0" y="0"/>
</gates>
<devices>
<device name="6MM" package="HDRV4W100P450X650_2X2_600X500X360B">
<connects>
<connect gate="G$1" pin="P" pad="1 2"/>
<connect gate="G$1" pin="S" pad="3 4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7707443/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="NPN">
<gates>
<gate name="G$1" symbol="NPN" x="0" y="0"/>
</gates>
<devices>
<device name="SOT-23" package="SOT95P240X110-3">
<connects>
<connect gate="G$1" pin="B" pad="1"/>
<connect gate="G$1" pin="C" pad="3"/>
<connect gate="G$1" pin="E" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7655736/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MAX40203">
<gates>
<gate name="G$1" symbol="MAX40203" x="0" y="2.54"/>
</gates>
<devices>
<device name="SOT-23" package="SOT95P240X110-5">
<connects>
<connect gate="G$1" pin="EN" pad="3"/>
<connect gate="G$1" pin="GND" pad="2"/>
<connect gate="G$1" pin="OUT" pad="5"/>
<connect gate="G$1" pin="VDD" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:9867714/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MOSFET-N">
<gates>
<gate name="G$1" symbol="NMOS-FET-E" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT95P240X110-3">
<connects>
<connect gate="G$1" pin="D" pad="3"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7655736/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TO252" package="TO228P998X240-3">
<connects>
<connect gate="G$1" pin="D" pad="2"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="3"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:10452077/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="VNH5019A">
<gates>
<gate name="G$1" symbol="VNH5019" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOP100P1900X235-30">
<connects>
<connect gate="G$1" pin="CP" pad="11"/>
<connect gate="G$1" pin="CS" pad="8"/>
<connect gate="G$1" pin="CS_DIS" pad="6"/>
<connect gate="G$1" pin="DIAGA/ENA" pad="5"/>
<connect gate="G$1" pin="DIAGB/ENB" pad="9"/>
<connect gate="G$1" pin="GNDA" pad="26 27 28"/>
<connect gate="G$1" pin="GNDB" pad="18 19 20"/>
<connect gate="G$1" pin="INA" pad="4"/>
<connect gate="G$1" pin="INB" pad="10"/>
<connect gate="G$1" pin="OUTA" pad="1 25 30 SLUG2"/>
<connect gate="G$1" pin="OUTB" pad="15 16 21 SLUG3"/>
<connect gate="G$1" pin="PWM" pad="7"/>
<connect gate="G$1" pin="VBAT" pad="12"/>
<connect gate="G$1" pin="VCC" pad="3 13 23 SLUG1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7638378/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="FERRITE">
<gates>
<gate name="G$1" symbol="FERRITE" x="0" y="0"/>
</gates>
<devices>
<device name="805" package="RESC2012X65">
<connects>
<connect gate="G$1" pin="P$1" pad="1"/>
<connect gate="G$1" pin="P$2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7640128/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-PowerSymbols">
<description>&lt;h3&gt;SparkFun Power Symbols&lt;/h3&gt;
This library contains power, ground, and voltage-supply symbols.
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
</packages>
<symbols>
<symbol name="5V">
<description>&lt;h3&gt;5V Voltage Supply&lt;/h3&gt;</description>
<wire x1="0.762" y1="1.27" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-0.762" y2="1.27" width="0.254" layer="94"/>
<pin name="5V" x="0" y="0" visible="off" length="short" direction="sup" rot="R90"/>
<text x="0" y="2.794" size="1.778" layer="96" align="bottom-center">&gt;VALUE</text>
</symbol>
<symbol name="3.3V">
<description>&lt;h3&gt;3.3V Voltage Supply&lt;/h3&gt;</description>
<wire x1="0.762" y1="1.27" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-0.762" y2="1.27" width="0.254" layer="94"/>
<pin name="3.3V" x="0" y="0" visible="off" length="short" direction="sup" rot="R90"/>
<text x="0" y="2.794" size="1.778" layer="96" align="bottom-center">&gt;VALUE</text>
</symbol>
<symbol name="DGND">
<description>&lt;h3&gt;Digital Ground Supply&lt;/h3&gt;</description>
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
<text x="0" y="-0.254" size="1.778" layer="96" align="top-center">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="5V" prefix="SUPPLY">
<description>&lt;h3&gt;5V Supply Symbol&lt;/h3&gt;
&lt;p&gt;Power supply symbol for a specifically-stated 5V source.&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="5V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="3.3V" prefix="SUPPLY">
<description>&lt;h3&gt;3.3V Supply Symbol&lt;/h3&gt;
&lt;p&gt;Power supply symbol for a specifically-stated 3.3V source.&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="3.3V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GND" prefix="GND">
<description>&lt;h3&gt;Ground Supply Symbol&lt;/h3&gt;
&lt;p&gt;Generic signal ground supply symbol.&lt;/p&gt;</description>
<gates>
<gate name="1" symbol="DGND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0.2032" drill="0.1524">
<clearance class="0" value="0.1524"/>
</class>
</classes>
<parts>
<part name="SH1" library="untiec" deviceset="DSPIC33FJ128MC804" device="UNITEC_BOARD" package3d_urn="urn:adsk.eagle:package:7643367/2"/>
<part name="IC4" library="untiec" deviceset="FT232RL" device="SSOP28" package3d_urn="urn:adsk.eagle:package:7639630/1"/>
<part name="F3" library="untiec" deviceset="FUSE" device="1206" package3d_urn="urn:adsk.eagle:package:7657273/1" value="200mA"/>
<part name="USB" library="untiec" deviceset="USB" device="MICRO" package3d_urn="urn:adsk.eagle:package:7643962/2"/>
<part name="IC5" library="untiec" deviceset="MAX485" device="SO8" package3d_urn="urn:adsk.eagle:package:7656496/1"/>
<part name="ICSP" library="untiec" deviceset="HEADER6" device="2.54MM-MALE" package3d_urn="urn:adsk.eagle:package:7657283/1"/>
<part name="INCREMENTAL2" library="untiec" deviceset="HEADER5" device="2.54MM-MALE" package3d_urn="urn:adsk.eagle:package:7657286/1"/>
<part name="INCREMENTAL1" library="untiec" deviceset="HEADER5" device="2.54MM-MALE" package3d_urn="urn:adsk.eagle:package:7657286/1"/>
<part name="CONTACT1" library="untiec" deviceset="HEADER3" device="2.54MM-MALE" package3d_urn="urn:adsk.eagle:package:7657289/1"/>
<part name="CONTACT2" library="untiec" deviceset="HEADER3" device="2.54MM-MALE" package3d_urn="urn:adsk.eagle:package:7657289/1"/>
<part name="J6" library="untiec" deviceset="HEADER2" device="2.54MM-MALE" package3d_urn="urn:adsk.eagle:package:7642735/1"/>
<part name="POWER_EN" library="untiec" deviceset="HEADER2" device="5.08MM-POWER" package3d_urn="urn:adsk.eagle:package:7642726/2"/>
<part name="R2" library="untiec" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:7640128/1" value="10k"/>
<part name="R1" library="untiec" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:7640128/1" value="1.5k"/>
<part name="R3" library="untiec" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:7640128/1" value="1k"/>
<part name="R5" library="untiec" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:7640128/1" value="1k"/>
<part name="R6" library="untiec" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:7640128/1" value="1k"/>
<part name="R7" library="untiec" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:7640128/1" value="1k"/>
<part name="C7" library="untiec" deviceset="CAPACITOR" device="0805" package3d_urn="urn:adsk.eagle:package:7642704/1" value="33nf"/>
<part name="C10" library="untiec" deviceset="CAPACITOR" device="0805" package3d_urn="urn:adsk.eagle:package:7642704/1" value="100nf"/>
<part name="C11" library="untiec" deviceset="CAPACITOR" device="0805" package3d_urn="urn:adsk.eagle:package:7642704/1" value="10uf"/>
<part name="C12" library="untiec" deviceset="CAPACITOR" device="0805" package3d_urn="urn:adsk.eagle:package:7642704/1" value="100nf"/>
<part name="R22" library="untiec" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:7640128/1" value="1.5k"/>
<part name="R23" library="untiec" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:7640128/1" value="1.5k"/>
<part name="C13" library="untiec" deviceset="CAPACITOR" device="0805" package3d_urn="urn:adsk.eagle:package:7642704/1" value="100nf"/>
<part name="SUPPLY1" library="SparkFun-PowerSymbols" deviceset="5V" device=""/>
<part name="R24" library="untiec" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:7640128/1" value="1k"/>
<part name="R25" library="untiec" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:7640128/1" value="1k"/>
<part name="SUPPLY2" library="SparkFun-PowerSymbols" deviceset="5V" device=""/>
<part name="GND1" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="GND2" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="GND3" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="GND7" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="SUPPLY10" library="SparkFun-PowerSymbols" deviceset="5V" device=""/>
<part name="R29" library="untiec" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:7640128/1" value="3.3k"/>
<part name="SUPPLY13" library="SparkFun-PowerSymbols" deviceset="3.3V" device=""/>
<part name="SUPPLY14" library="SparkFun-PowerSymbols" deviceset="3.3V" device=""/>
<part name="SUPPLY15" library="SparkFun-PowerSymbols" deviceset="3.3V" device=""/>
<part name="SUPPLY16" library="SparkFun-PowerSymbols" deviceset="3.3V" device=""/>
<part name="GND4" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="GND11" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="GND12" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="GND13" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="R35" library="untiec" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:7640128/1" value="4.7k"/>
<part name="R36" library="untiec" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:7640128/1" value="4.7k"/>
<part name="R37" library="untiec" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:7640128/1" value="10k"/>
<part name="R38" library="untiec" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:7640128/1" value="10k"/>
<part name="R39" library="untiec" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:7640128/1" value="4.7k"/>
<part name="R40" library="untiec" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:7640128/1" value="4.7k"/>
<part name="R41" library="untiec" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:7640128/1" value="10k"/>
<part name="R42" library="untiec" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:7640128/1" value="10k"/>
<part name="SUPPLY17" library="SparkFun-PowerSymbols" deviceset="5V" device=""/>
<part name="SUPPLY18" library="SparkFun-PowerSymbols" deviceset="5V" device=""/>
<part name="GND14" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="GND15" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="C14" library="untiec" deviceset="CAPACITOR" device="0805" package3d_urn="urn:adsk.eagle:package:7642704/1" value="100nf"/>
<part name="C15" library="untiec" deviceset="CAPACITOR" device="0805" package3d_urn="urn:adsk.eagle:package:7642704/1" value="100nf"/>
<part name="SUPPLY19" library="SparkFun-PowerSymbols" deviceset="5V" device=""/>
<part name="D8" library="untiec" deviceset="LED" device="0805" package3d_urn="urn:adsk.eagle:package:7707399/2"/>
<part name="R43" library="untiec" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:7640128/1" value="680"/>
<part name="SUPPLY20" library="SparkFun-PowerSymbols" deviceset="3.3V" device=""/>
<part name="R44" library="untiec" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:7640128/1" value="10k"/>
<part name="R45" library="untiec" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:7640128/1" value="680"/>
<part name="R46" library="untiec" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:7640128/1" value="1k"/>
<part name="R47" library="untiec" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:7640128/1" value="680"/>
<part name="D9" library="untiec" deviceset="LED" device="0805" package3d_urn="urn:adsk.eagle:package:7707399/2"/>
<part name="D10" library="untiec" deviceset="LED" device="0805" package3d_urn="urn:adsk.eagle:package:7707399/2"/>
<part name="C16" library="untiec" deviceset="CAPACITOR" device="0805" package3d_urn="urn:adsk.eagle:package:7642704/1" value="100nf"/>
<part name="C17" library="untiec" deviceset="CAPACITOR" device="0805" package3d_urn="urn:adsk.eagle:package:7642704/1" value="100nf"/>
<part name="SUPPLY21" library="SparkFun-PowerSymbols" deviceset="3.3V" device=""/>
<part name="SUPPLY22" library="SparkFun-PowerSymbols" deviceset="3.3V" device=""/>
<part name="GND16" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="GND17" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="RS485_1" library="untiec" deviceset="RJ11" device="NICE-3D" package3d_urn="urn:adsk.eagle:package:7707326/3"/>
<part name="RS485_2" library="untiec" deviceset="RJ11" device="NICE-3D" package3d_urn="urn:adsk.eagle:package:7707326/3"/>
<part name="R48" library="untiec" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:7640128/1" value="120"/>
<part name="R49" library="untiec" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:7640128/1" value="1.5k"/>
<part name="R50" library="untiec" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:7640128/1" value="1.5k"/>
<part name="D11" library="untiec" deviceset="LED" device="0805" package3d_urn="urn:adsk.eagle:package:7707399/2"/>
<part name="D12" library="untiec" deviceset="LED" device="0805" package3d_urn="urn:adsk.eagle:package:7707399/2"/>
<part name="C18" library="untiec" deviceset="CAPACITOR" device="0805" package3d_urn="urn:adsk.eagle:package:7642704/1" value="100nf"/>
<part name="SUPPLY23" library="SparkFun-PowerSymbols" deviceset="5V" device=""/>
<part name="GND18" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="SUPPLY24" library="SparkFun-PowerSymbols" deviceset="5V" device=""/>
<part name="D15" library="untiec" deviceset="LED" device="0805" package3d_urn="urn:adsk.eagle:package:7707399/2"/>
<part name="R53" library="untiec" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:7640128/1" value="680"/>
<part name="SUPPLY29" library="SparkFun-PowerSymbols" deviceset="3.3V" device=""/>
<part name="SUPPLY27" library="SparkFun-PowerSymbols" deviceset="3.3V" device=""/>
<part name="GND20" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="PASAPAS" library="untiec" deviceset="HEADER4" device="MALE2.54MM" package3d_urn="urn:adsk.eagle:package:7706676/1"/>
<part name="I2C" library="untiec" deviceset="HEADER4" device="MALE2.54MM" package3d_urn="urn:adsk.eagle:package:7706676/1"/>
<part name="S1" library="untiec" deviceset="BUTTON" device="6MM" package3d_urn="urn:adsk.eagle:package:7707443/2"/>
<part name="D6" library="untiec" deviceset="LED" device="0805" package3d_urn="urn:adsk.eagle:package:7707399/2"/>
<part name="D7" library="untiec" deviceset="LED" device="0805" package3d_urn="urn:adsk.eagle:package:7707399/2"/>
<part name="Q1" library="untiec" deviceset="NPN" device="SOT-23" package3d_urn="urn:adsk.eagle:package:7655736/1"/>
<part name="R30" library="untiec" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:7640128/1" value="10k"/>
<part name="R32" library="untiec" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:7640128/1" value="1k"/>
<part name="SUPPLY28" library="SparkFun-PowerSymbols" deviceset="5V" device=""/>
<part name="GND22" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="IC7" library="untiec" deviceset="LM1117" device="SOT223" package3d_urn="urn:adsk.eagle:package:7654761/1" value="3.3V"/>
<part name="C2" library="untiec" deviceset="CAPACITOR" device="0805" package3d_urn="urn:adsk.eagle:package:7642704/1" value="100nf"/>
<part name="C20" library="untiec" deviceset="CAPACITOR" device="0805" package3d_urn="urn:adsk.eagle:package:7642704/1" value="100nf"/>
<part name="C21" library="untiec" deviceset="CAPACITOR" device="0805" package3d_urn="urn:adsk.eagle:package:7642704/1" value="100nf"/>
<part name="J1" library="untiec" deviceset="HEADER2" device="5.08MM-POWER" package3d_urn="urn:adsk.eagle:package:7642726/2"/>
<part name="D16" library="untiec" deviceset="DIODE" device="DO-214AC/SMA" package3d_urn="urn:adsk.eagle:package:7707360/1" value="1A"/>
<part name="C22" library="untiec" deviceset="CAPACITORP" device="6.3MM" package3d_urn="urn:adsk.eagle:package:7654791/1" value="100uf"/>
<part name="SUPPLY30" library="SparkFun-PowerSymbols" deviceset="3.3V" device=""/>
<part name="GND23" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="GND24" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="SUPPLY31" library="SparkFun-PowerSymbols" deviceset="5V" device=""/>
<part name="R34" library="untiec" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:7640128/1" value="10k"/>
<part name="GND25" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="R54" library="untiec" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:7640128/1" value="1k"/>
<part name="L1" library="untiec" deviceset="LED" device="0805" package3d_urn="urn:adsk.eagle:package:7707399/2"/>
<part name="R55" library="untiec" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:7640128/1" value="1.5k"/>
<part name="L2" library="untiec" deviceset="LED" device="0805" package3d_urn="urn:adsk.eagle:package:7707399/2"/>
<part name="R56" library="untiec" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:7640128/1" value="680"/>
<part name="F4" library="untiec" deviceset="FUSE" device="20X5MM" package3d_urn="urn:adsk.eagle:package:7654933/2" value="1A"/>
<part name="F5" library="untiec" deviceset="FUSE" device="20X5MM" package3d_urn="urn:adsk.eagle:package:7654933/2" value="&lt;10A"/>
<part name="IC8" library="untiec" deviceset="LM1117" device="SOT223" package3d_urn="urn:adsk.eagle:package:7654761/1" value="5V"/>
<part name="C23" library="untiec" deviceset="CAPACITOR" device="0805" package3d_urn="urn:adsk.eagle:package:7642704/1" value="100nf"/>
<part name="GND26" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="C27" library="untiec" deviceset="CAPACITOR" device="0805" package3d_urn="urn:adsk.eagle:package:7642704/1" value="10uf"/>
<part name="C28" library="untiec" deviceset="CAPACITOR" device="0805" package3d_urn="urn:adsk.eagle:package:7642704/1" value="10uf"/>
<part name="SUPPLY32" library="SparkFun-PowerSymbols" deviceset="5V" device=""/>
<part name="D18" library="untiec" deviceset="DIODE" device="DO-214AC/SMA" package3d_urn="urn:adsk.eagle:package:7707360/1" value="1A"/>
<part name="IC10" library="untiec" deviceset="MAX40203" device="SOT-23" package3d_urn="urn:adsk.eagle:package:9867714/1"/>
<part name="IC6" library="untiec" deviceset="MAX40203" device="SOT-23" package3d_urn="urn:adsk.eagle:package:9867714/1"/>
<part name="C32" library="untiec" deviceset="CAPACITORP" device="10MM" package3d_urn="urn:adsk.eagle:package:10452068/1" value="470u"/>
<part name="Q2" library="untiec" deviceset="MOSFET-N" device="TO252" package3d_urn="urn:adsk.eagle:package:10452077/1"/>
<part name="U1" library="untiec" deviceset="VNH5019A" device="" package3d_urn="urn:adsk.eagle:package:7638378/1"/>
<part name="GND10" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="D20" library="untiec" deviceset="DIODE" device="SOD-523" package3d_urn="urn:adsk.eagle:package:7707409/1"/>
<part name="SUPPLY3" library="SparkFun-PowerSymbols" deviceset="3.3V" device=""/>
<part name="MOTEUR1" library="untiec" deviceset="HEADER4" device="5.08MM" package3d_urn="urn:adsk.eagle:package:10452165/2"/>
<part name="GND19" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="GND21" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="R14" library="untiec" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:7640128/1" value="10k"/>
<part name="R15" library="untiec" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:7640128/1" value="1.5k"/>
<part name="R16" library="untiec" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:7640128/1" value="1k"/>
<part name="R17" library="untiec" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:7640128/1" value="1k"/>
<part name="R18" library="untiec" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:7640128/1" value="1k"/>
<part name="R19" library="untiec" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:7640128/1" value="1k"/>
<part name="C4" library="untiec" deviceset="CAPACITOR" device="0805" package3d_urn="urn:adsk.eagle:package:7642704/1" value="33nf"/>
<part name="GND9" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="SUPPLY6" library="SparkFun-PowerSymbols" deviceset="5V" device=""/>
<part name="R20" library="untiec" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:7640128/1" value="3.3k"/>
<part name="C5" library="untiec" deviceset="CAPACITORP" device="10MM" package3d_urn="urn:adsk.eagle:package:10452068/1" value="470u"/>
<part name="Q4" library="untiec" deviceset="MOSFET-N" device="TO252" package3d_urn="urn:adsk.eagle:package:10452077/1"/>
<part name="U2" library="untiec" deviceset="VNH5019A" device="" package3d_urn="urn:adsk.eagle:package:7638378/1"/>
<part name="GND27" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="D4" library="untiec" deviceset="DIODE" device="SOD-523" package3d_urn="urn:adsk.eagle:package:7707409/1"/>
<part name="SUPPLY7" library="SparkFun-PowerSymbols" deviceset="3.3V" device=""/>
<part name="MOTEUR2" library="untiec" deviceset="HEADER4" device="5.08MM" package3d_urn="urn:adsk.eagle:package:10452165/2"/>
<part name="GND28" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="R52" library="untiec" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:7640128/1" value="10k"/>
<part name="R57" library="untiec" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:7640128/1" value="1.5k"/>
<part name="R58" library="untiec" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:7640128/1" value="1k"/>
<part name="R59" library="untiec" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:7640128/1" value="1k"/>
<part name="R60" library="untiec" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:7640128/1" value="1k"/>
<part name="R61" library="untiec" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:7640128/1" value="1k"/>
<part name="C9" library="untiec" deviceset="CAPACITOR" device="0805" package3d_urn="urn:adsk.eagle:package:7642704/1" value="33nf"/>
<part name="GND32" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="SUPPLY11" library="SparkFun-PowerSymbols" deviceset="5V" device=""/>
<part name="R62" library="untiec" deviceset="RESISTOR" device="0805" package3d_urn="urn:adsk.eagle:package:7640128/1" value="3.3k"/>
<part name="C19" library="untiec" deviceset="CAPACITORP" device="10MM" package3d_urn="urn:adsk.eagle:package:10452068/1" value="470u"/>
<part name="Q6" library="untiec" deviceset="MOSFET-N" device="TO252" package3d_urn="urn:adsk.eagle:package:10452077/1"/>
<part name="U3" library="untiec" deviceset="VNH5019A" device="" package3d_urn="urn:adsk.eagle:package:7638378/1"/>
<part name="GND33" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="D13" library="untiec" deviceset="DIODE" device="SOD-523" package3d_urn="urn:adsk.eagle:package:7707409/1"/>
<part name="SUPPLY12" library="SparkFun-PowerSymbols" deviceset="3.3V" device=""/>
<part name="MOTEUR3" library="untiec" deviceset="HEADER4" device="5.08MM" package3d_urn="urn:adsk.eagle:package:10452165/2"/>
<part name="GND34" library="SparkFun-PowerSymbols" deviceset="GND" device=""/>
<part name="L3" library="untiec" deviceset="FERRITE" device="805" package3d_urn="urn:adsk.eagle:package:7640128/1"/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="SH1" gate="G$1" x="-33.02" y="-7.62" smashed="yes"/>
<instance part="IC4" gate="G$1" x="96.52" y="124.46" smashed="yes"/>
<instance part="F3" gate="G$1" x="-2.54" y="154.94" smashed="yes">
<attribute name="NAME" x="-6.35" y="156.337" size="1.778" layer="95"/>
<attribute name="VALUE" x="-6.35" y="152.019" size="1.778" layer="96"/>
</instance>
<instance part="USB" gate="G$1" x="-20.32" y="144.78" smashed="yes"/>
<instance part="IC5" gate="G$1" x="73.66" y="-116.84" smashed="yes"/>
<instance part="ICSP" gate="G$1" x="-78.74" y="129.54" smashed="yes" rot="R180"/>
<instance part="INCREMENTAL2" gate="G$1" x="-50.8" y="-149.86" smashed="yes" rot="R180"/>
<instance part="INCREMENTAL1" gate="G$1" x="-48.26" y="-106.68" smashed="yes" rot="R180"/>
<instance part="CONTACT1" gate="G$1" x="134.62" y="22.86" smashed="yes" rot="R180"/>
<instance part="CONTACT2" gate="G$1" x="134.62" y="50.8" smashed="yes" rot="R180"/>
<instance part="J6" gate="G$1" x="88.9" y="-154.94" smashed="yes"/>
<instance part="POWER_EN" gate="G$1" x="-218.44" y="38.1" smashed="yes"/>
<instance part="R2" gate="G$1" x="-238.76" y="-2.54" smashed="yes">
<attribute name="NAME" x="-241.3" y="0" size="1.778" layer="95"/>
<attribute name="VALUE" x="-241.3" y="-6.35" size="1.778" layer="96"/>
</instance>
<instance part="R1" gate="G$1" x="-231.14" y="10.16" smashed="yes" rot="R90">
<attribute name="NAME" x="-233.68" y="7.62" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-227.33" y="7.62" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R3" gate="G$1" x="-190.5" y="20.32" smashed="yes">
<attribute name="NAME" x="-193.04" y="22.86" size="1.778" layer="95"/>
<attribute name="VALUE" x="-193.04" y="16.51" size="1.778" layer="96"/>
</instance>
<instance part="R5" gate="G$1" x="-175.26" y="-2.54" smashed="yes">
<attribute name="NAME" x="-177.8" y="0" size="1.778" layer="95"/>
<attribute name="VALUE" x="-177.8" y="-6.35" size="1.778" layer="96"/>
</instance>
<instance part="R6" gate="G$1" x="-175.26" y="-7.62" smashed="yes">
<attribute name="NAME" x="-177.8" y="-5.08" size="1.778" layer="95"/>
<attribute name="VALUE" x="-177.8" y="-11.43" size="1.778" layer="96"/>
</instance>
<instance part="R7" gate="G$1" x="-175.26" y="-12.7" smashed="yes">
<attribute name="NAME" x="-177.8" y="-10.16" size="1.778" layer="95"/>
<attribute name="VALUE" x="-177.8" y="-16.51" size="1.778" layer="96"/>
</instance>
<instance part="C7" gate="G$1" x="-246.38" y="10.16" smashed="yes">
<attribute name="NAME" x="-243.84" y="12.7" size="1.778" layer="95"/>
<attribute name="VALUE" x="-243.84" y="10.16" size="1.778" layer="96"/>
</instance>
<instance part="C10" gate="G$1" x="45.72" y="127" smashed="yes">
<attribute name="NAME" x="48.26" y="129.54" size="1.778" layer="95"/>
<attribute name="VALUE" x="48.26" y="127" size="1.778" layer="96"/>
</instance>
<instance part="C11" gate="G$1" x="55.88" y="127" smashed="yes">
<attribute name="NAME" x="58.42" y="129.54" size="1.778" layer="95"/>
<attribute name="VALUE" x="58.42" y="127" size="1.778" layer="96"/>
</instance>
<instance part="C12" gate="G$1" x="66.04" y="88.9" smashed="yes">
<attribute name="NAME" x="68.58" y="91.44" size="1.778" layer="95"/>
<attribute name="VALUE" x="68.58" y="88.9" size="1.778" layer="96"/>
</instance>
<instance part="R22" gate="G$1" x="129.54" y="114.3" smashed="yes">
<attribute name="NAME" x="127" y="116.84" size="1.778" layer="95"/>
<attribute name="VALUE" x="127" y="110.49" size="1.778" layer="96"/>
</instance>
<instance part="R23" gate="G$1" x="129.54" y="109.22" smashed="yes">
<attribute name="NAME" x="127" y="111.76" size="1.778" layer="95"/>
<attribute name="VALUE" x="127" y="105.41" size="1.778" layer="96"/>
</instance>
<instance part="C13" gate="G$1" x="-83.82" y="104.14" smashed="yes" rot="R90">
<attribute name="NAME" x="-86.36" y="106.68" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-83.82" y="106.68" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="SUPPLY1" gate="G$1" x="-104.14" y="129.54" smashed="yes">
<attribute name="VALUE" x="-104.14" y="132.334" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="R24" gate="G$1" x="127" y="154.94" smashed="yes">
<attribute name="NAME" x="124.46" y="157.48" size="1.778" layer="95"/>
<attribute name="VALUE" x="124.46" y="151.13" size="1.778" layer="96"/>
</instance>
<instance part="R25" gate="G$1" x="127" y="149.86" smashed="yes">
<attribute name="NAME" x="124.46" y="152.4" size="1.778" layer="95"/>
<attribute name="VALUE" x="124.46" y="146.05" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY2" gate="G$1" x="45.72" y="165.1" smashed="yes">
<attribute name="VALUE" x="45.72" y="167.894" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="GND1" gate="1" x="73.66" y="76.2" smashed="yes">
<attribute name="VALUE" x="73.66" y="75.946" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="GND2" gate="1" x="-7.62" y="129.54" smashed="yes">
<attribute name="VALUE" x="-7.62" y="129.286" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="GND3" gate="1" x="-109.22" y="99.06" smashed="yes">
<attribute name="VALUE" x="-109.22" y="98.806" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="GND7" gate="1" x="-259.08" y="15.24" smashed="yes">
<attribute name="VALUE" x="-259.08" y="14.986" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="SUPPLY10" gate="G$1" x="-165.1" y="12.7" smashed="yes">
<attribute name="VALUE" x="-165.1" y="15.494" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="R29" gate="G$1" x="-165.1" y="5.08" smashed="yes" rot="R90">
<attribute name="NAME" x="-167.64" y="2.54" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-161.29" y="2.54" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="SUPPLY13" gate="G$1" x="-91.44" y="-35.56" smashed="yes" rot="R90">
<attribute name="VALUE" x="-94.234" y="-35.56" size="1.778" layer="96" rot="R90" align="bottom-center"/>
</instance>
<instance part="SUPPLY14" gate="G$1" x="-91.44" y="15.24" smashed="yes" rot="R90">
<attribute name="VALUE" x="-94.234" y="15.24" size="1.778" layer="96" rot="R90" align="bottom-center"/>
</instance>
<instance part="SUPPLY15" gate="G$1" x="27.94" y="25.4" smashed="yes" rot="R270">
<attribute name="VALUE" x="30.734" y="25.4" size="1.778" layer="96" rot="R270" align="bottom-center"/>
</instance>
<instance part="SUPPLY16" gate="G$1" x="27.94" y="-35.56" smashed="yes" rot="R270">
<attribute name="VALUE" x="30.734" y="-35.56" size="1.778" layer="96" rot="R270" align="bottom-center"/>
</instance>
<instance part="GND4" gate="1" x="-93.98" y="-30.48" smashed="yes" rot="R270">
<attribute name="VALUE" x="-94.234" y="-30.48" size="1.778" layer="96" rot="R270" align="top-center"/>
</instance>
<instance part="GND11" gate="1" x="-93.98" y="20.32" smashed="yes" rot="R270">
<attribute name="VALUE" x="-94.234" y="20.32" size="1.778" layer="96" rot="R270" align="top-center"/>
</instance>
<instance part="GND12" gate="1" x="30.48" y="20.32" smashed="yes" rot="R90">
<attribute name="VALUE" x="30.734" y="20.32" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="GND13" gate="1" x="30.48" y="-30.48" smashed="yes" rot="R90">
<attribute name="VALUE" x="30.734" y="-30.48" size="1.778" layer="96" rot="R90" align="top-center"/>
</instance>
<instance part="R35" gate="G$1" x="-68.58" y="-149.86" smashed="yes" rot="R180">
<attribute name="NAME" x="-66.04" y="-152.4" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="-66.04" y="-146.05" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R36" gate="G$1" x="-68.58" y="-154.94" smashed="yes" rot="R180">
<attribute name="NAME" x="-66.04" y="-157.48" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="-66.04" y="-151.13" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R37" gate="G$1" x="-88.9" y="-154.94" smashed="yes" rot="R180">
<attribute name="NAME" x="-86.36" y="-157.48" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="-86.36" y="-151.13" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R38" gate="G$1" x="-88.9" y="-149.86" smashed="yes" rot="R180">
<attribute name="NAME" x="-86.36" y="-152.4" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="-86.36" y="-146.05" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R39" gate="G$1" x="-68.58" y="-106.68" smashed="yes" rot="R180">
<attribute name="NAME" x="-66.04" y="-109.22" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="-66.04" y="-102.87" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R40" gate="G$1" x="-68.58" y="-111.76" smashed="yes" rot="R180">
<attribute name="NAME" x="-66.04" y="-114.3" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="-66.04" y="-107.95" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R41" gate="G$1" x="-88.9" y="-111.76" smashed="yes" rot="R180">
<attribute name="NAME" x="-86.36" y="-114.3" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="-86.36" y="-107.95" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R42" gate="G$1" x="-88.9" y="-106.68" smashed="yes" rot="R180">
<attribute name="NAME" x="-86.36" y="-109.22" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="-86.36" y="-102.87" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="SUPPLY17" gate="G$1" x="-96.52" y="-91.44" smashed="yes">
<attribute name="VALUE" x="-96.52" y="-88.646" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="SUPPLY18" gate="G$1" x="-96.52" y="-134.62" smashed="yes">
<attribute name="VALUE" x="-96.52" y="-131.826" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="GND14" gate="1" x="-96.52" y="-121.92" smashed="yes">
<attribute name="VALUE" x="-96.52" y="-122.174" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="GND15" gate="1" x="-96.52" y="-165.1" smashed="yes">
<attribute name="VALUE" x="-96.52" y="-165.354" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="C14" gate="G$1" x="-96.52" y="-142.24" smashed="yes">
<attribute name="NAME" x="-93.98" y="-139.7" size="1.778" layer="95"/>
<attribute name="VALUE" x="-93.98" y="-142.24" size="1.778" layer="96"/>
</instance>
<instance part="C15" gate="G$1" x="-96.52" y="-99.06" smashed="yes">
<attribute name="NAME" x="-93.98" y="-96.52" size="1.778" layer="95"/>
<attribute name="VALUE" x="-93.98" y="-99.06" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY19" gate="G$1" x="147.32" y="116.84" smashed="yes">
<attribute name="VALUE" x="147.32" y="119.634" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="D8" gate="G$1" x="40.64" y="-5.08" smashed="yes" rot="R270">
<attribute name="NAME" x="36.068" y="-8.636" size="1.778" layer="95"/>
</instance>
<instance part="R43" gate="G$1" x="50.8" y="-5.08" smashed="yes">
<attribute name="NAME" x="48.26" y="-2.54" size="1.778" layer="95"/>
<attribute name="VALUE" x="48.26" y="-8.89" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY20" gate="G$1" x="58.42" y="-2.54" smashed="yes">
<attribute name="VALUE" x="58.42" y="0.254" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="R44" gate="G$1" x="114.3" y="50.8" smashed="yes">
<attribute name="NAME" x="111.76" y="53.34" size="1.778" layer="95"/>
<attribute name="VALUE" x="111.76" y="46.99" size="1.778" layer="96"/>
</instance>
<instance part="R45" gate="G$1" x="93.98" y="58.42" smashed="yes" rot="R180">
<attribute name="NAME" x="96.52" y="55.88" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="96.52" y="62.23" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R46" gate="G$1" x="114.3" y="22.86" smashed="yes" rot="R180">
<attribute name="NAME" x="116.84" y="20.32" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="116.84" y="26.67" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R47" gate="G$1" x="93.98" y="30.48" smashed="yes" rot="R180">
<attribute name="NAME" x="96.52" y="27.94" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="96.52" y="34.29" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="D9" gate="G$1" x="86.36" y="50.8" smashed="yes" rot="R180">
<attribute name="NAME" x="82.804" y="55.372" size="1.778" layer="95" rot="R270"/>
</instance>
<instance part="D10" gate="G$1" x="86.36" y="22.86" smashed="yes" rot="R180">
<attribute name="NAME" x="82.804" y="27.432" size="1.778" layer="95" rot="R270"/>
</instance>
<instance part="C16" gate="G$1" x="101.6" y="53.34" smashed="yes">
<attribute name="NAME" x="104.14" y="55.88" size="1.778" layer="95"/>
<attribute name="VALUE" x="104.14" y="53.34" size="1.778" layer="96"/>
</instance>
<instance part="C17" gate="G$1" x="101.6" y="25.4" smashed="yes">
<attribute name="NAME" x="104.14" y="27.94" size="1.778" layer="95"/>
<attribute name="VALUE" x="104.14" y="25.4" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY21" gate="G$1" x="124.46" y="60.96" smashed="yes">
<attribute name="VALUE" x="124.46" y="63.754" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="SUPPLY22" gate="G$1" x="124.46" y="33.02" smashed="yes">
<attribute name="VALUE" x="124.46" y="35.814" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="GND16" gate="1" x="106.68" y="40.64" smashed="yes">
<attribute name="VALUE" x="106.68" y="40.386" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="GND17" gate="1" x="106.68" y="12.7" smashed="yes">
<attribute name="VALUE" x="106.68" y="12.446" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="RS485_1" gate="G$1" x="124.46" y="-101.6" smashed="yes" rot="MR0"/>
<instance part="RS485_2" gate="G$1" x="124.46" y="-137.16" smashed="yes" rot="MR0"/>
<instance part="R48" gate="G$1" x="104.14" y="-157.48" smashed="yes" rot="R180">
<attribute name="NAME" x="106.68" y="-160.02" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="106.68" y="-153.67" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R49" gate="G$1" x="40.64" y="-152.4" smashed="yes" rot="R180">
<attribute name="NAME" x="43.18" y="-154.94" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="43.18" y="-148.59" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R50" gate="G$1" x="40.64" y="-162.56" smashed="yes" rot="R180">
<attribute name="NAME" x="43.18" y="-165.1" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="43.18" y="-158.75" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="D11" gate="G$1" x="53.34" y="-152.4" smashed="yes" rot="R270">
<attribute name="NAME" x="48.768" y="-155.956" size="1.778" layer="95"/>
</instance>
<instance part="D12" gate="G$1" x="53.34" y="-162.56" smashed="yes" rot="R270">
<attribute name="NAME" x="48.768" y="-166.116" size="1.778" layer="95"/>
</instance>
<instance part="C18" gate="G$1" x="96.52" y="-104.14" smashed="yes">
<attribute name="NAME" x="99.06" y="-101.6" size="1.778" layer="95"/>
<attribute name="VALUE" x="99.06" y="-104.14" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY23" gate="G$1" x="91.44" y="-96.52" smashed="yes">
<attribute name="VALUE" x="91.44" y="-93.726" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="GND18" gate="1" x="96.52" y="-129.54" smashed="yes">
<attribute name="VALUE" x="96.52" y="-129.794" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="SUPPLY24" gate="G$1" x="33.02" y="-149.86" smashed="yes">
<attribute name="VALUE" x="33.02" y="-147.066" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="D15" gate="G$1" x="-104.14" y="10.16" smashed="yes" rot="R90">
<attribute name="NAME" x="-102.108" y="13.716" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="R53" gate="G$1" x="-114.3" y="10.16" smashed="yes">
<attribute name="NAME" x="-116.84" y="12.7" size="1.778" layer="95"/>
<attribute name="VALUE" x="-116.84" y="6.35" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY29" gate="G$1" x="-121.92" y="12.7" smashed="yes">
<attribute name="VALUE" x="-121.92" y="15.494" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="SUPPLY27" gate="G$1" x="-25.4" y="99.06" smashed="yes">
<attribute name="VALUE" x="-25.4" y="101.854" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="GND20" gate="1" x="-25.4" y="76.2" smashed="yes">
<attribute name="VALUE" x="-25.4" y="75.946" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="PASAPAS" gate="G$1" x="-132.08" y="-73.66" smashed="yes"/>
<instance part="I2C" gate="G$1" x="-15.24" y="88.9" smashed="yes" rot="R180"/>
<instance part="S1" gate="G$1" x="-101.6" y="104.14" smashed="yes" rot="R270">
<attribute name="NAME" x="-103.505" y="111.125" size="1.778" layer="95"/>
<attribute name="VALUE" x="-98.425" y="108.585" size="1.778" layer="96"/>
</instance>
<instance part="D6" gate="G$1" x="142.24" y="114.3" smashed="yes" rot="R270">
<attribute name="NAME" x="137.668" y="110.744" size="1.778" layer="95"/>
</instance>
<instance part="D7" gate="G$1" x="142.24" y="109.22" smashed="yes" rot="R270">
<attribute name="NAME" x="137.668" y="105.664" size="1.778" layer="95"/>
</instance>
<instance part="Q1" gate="G$1" x="35.56" y="-121.92" smashed="yes">
<attribute name="NAME" x="25.4" y="-114.3" size="1.778" layer="95"/>
<attribute name="VALUE" x="25.4" y="-116.84" size="1.778" layer="96"/>
</instance>
<instance part="R30" gate="G$1" x="38.1" y="-106.68" smashed="yes" rot="R90">
<attribute name="NAME" x="35.56" y="-109.22" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="41.91" y="-109.22" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R32" gate="G$1" x="22.86" y="-121.92" smashed="yes">
<attribute name="NAME" x="20.32" y="-119.38" size="1.778" layer="95"/>
<attribute name="VALUE" x="20.32" y="-125.73" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY28" gate="G$1" x="38.1" y="-99.06" smashed="yes">
<attribute name="VALUE" x="38.1" y="-96.266" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="GND22" gate="1" x="38.1" y="-132.08" smashed="yes">
<attribute name="VALUE" x="38.1" y="-132.334" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="IC7" gate="G$1" x="-223.52" y="68.58" smashed="yes"/>
<instance part="C2" gate="G$1" x="-248.92" y="99.06" smashed="yes">
<attribute name="NAME" x="-246.38" y="101.6" size="1.778" layer="95"/>
<attribute name="VALUE" x="-246.38" y="99.06" size="1.778" layer="96"/>
</instance>
<instance part="C20" gate="G$1" x="-190.5" y="66.04" smashed="yes">
<attribute name="NAME" x="-187.96" y="68.58" size="1.778" layer="95"/>
<attribute name="VALUE" x="-187.96" y="66.04" size="1.778" layer="96"/>
</instance>
<instance part="C21" gate="G$1" x="-190.5" y="99.06" smashed="yes">
<attribute name="NAME" x="-187.96" y="99.06" size="1.778" layer="95"/>
<attribute name="VALUE" x="-187.96" y="93.98" size="1.778" layer="96"/>
</instance>
<instance part="J1" gate="G$1" x="-231.14" y="134.62" smashed="yes"/>
<instance part="D16" gate="G$1" x="-198.12" y="137.16" smashed="yes">
<attribute name="NAME" x="-200.4314" y="139.8016" size="1.778" layer="95"/>
<attribute name="VALUE" x="-200.6854" y="132.6642" size="1.778" layer="96"/>
</instance>
<instance part="C22" gate="G$1" x="-190.5" y="132.08" smashed="yes">
<attribute name="NAME" x="-189.357" y="132.5626" size="1.778" layer="95"/>
<attribute name="VALUE" x="-189.357" y="127.4826" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY30" gate="G$1" x="-190.5" y="73.66" smashed="yes">
<attribute name="VALUE" x="-190.5" y="76.454" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="GND23" gate="1" x="-223.52" y="83.82" smashed="yes">
<attribute name="VALUE" x="-223.52" y="83.566" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="GND24" gate="1" x="-190.5" y="119.38" smashed="yes">
<attribute name="VALUE" x="-190.5" y="119.126" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="SUPPLY31" gate="G$1" x="-157.48" y="109.22" smashed="yes">
<attribute name="VALUE" x="-157.48" y="112.014" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="R34" gate="G$1" x="-162.56" y="152.4" smashed="yes" rot="R90">
<attribute name="NAME" x="-165.1" y="149.86" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-158.75" y="149.86" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND25" gate="1" x="-162.56" y="127" smashed="yes">
<attribute name="VALUE" x="-162.56" y="126.746" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="R54" gate="G$1" x="-162.56" y="137.16" smashed="yes" rot="R90">
<attribute name="NAME" x="-165.1" y="134.62" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-158.75" y="134.62" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="L1" gate="G$1" x="-142.24" y="99.06" smashed="yes">
<attribute name="NAME" x="-138.684" y="94.488" size="1.778" layer="95" rot="R90"/>
</instance>
<instance part="R55" gate="G$1" x="-149.86" y="104.14" smashed="yes">
<attribute name="NAME" x="-152.4" y="106.68" size="1.778" layer="95"/>
<attribute name="VALUE" x="-152.4" y="100.33" size="1.778" layer="96"/>
</instance>
<instance part="L2" gate="G$1" x="-167.64" y="66.04" smashed="yes">
<attribute name="NAME" x="-164.084" y="61.468" size="1.778" layer="95" rot="R90"/>
</instance>
<instance part="R56" gate="G$1" x="-175.26" y="71.12" smashed="yes">
<attribute name="NAME" x="-177.8" y="73.66" size="1.778" layer="95"/>
<attribute name="VALUE" x="-177.8" y="67.31" size="1.778" layer="96"/>
</instance>
<instance part="F4" gate="G$1" x="-210.82" y="137.16" smashed="yes">
<attribute name="NAME" x="-214.63" y="138.557" size="1.778" layer="95"/>
<attribute name="VALUE" x="-214.63" y="134.239" size="1.778" layer="96"/>
</instance>
<instance part="F5" gate="G$1" x="-210.82" y="165.1" smashed="yes">
<attribute name="NAME" x="-214.63" y="166.497" size="1.778" layer="95"/>
<attribute name="VALUE" x="-214.63" y="162.179" size="1.778" layer="96"/>
</instance>
<instance part="IC8" gate="G$1" x="-223.52" y="101.6" smashed="yes"/>
<instance part="C23" gate="G$1" x="-248.92" y="66.04" smashed="yes">
<attribute name="NAME" x="-246.38" y="66.04" size="1.778" layer="95"/>
<attribute name="VALUE" x="-246.38" y="60.96" size="1.778" layer="96"/>
</instance>
<instance part="GND26" gate="1" x="-223.52" y="50.8" smashed="yes">
<attribute name="VALUE" x="-223.52" y="50.546" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="C27" gate="G$1" x="-203.2" y="99.06" smashed="yes">
<attribute name="NAME" x="-200.66" y="99.06" size="1.778" layer="95"/>
<attribute name="VALUE" x="-200.66" y="93.98" size="1.778" layer="96"/>
</instance>
<instance part="C28" gate="G$1" x="-203.2" y="66.04" smashed="yes">
<attribute name="NAME" x="-200.66" y="68.58" size="1.778" layer="95"/>
<attribute name="VALUE" x="-200.66" y="66.04" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY32" gate="G$1" x="-271.78" y="76.2" smashed="yes">
<attribute name="VALUE" x="-271.78" y="78.994" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="D18" gate="G$1" x="-264.16" y="71.12" smashed="yes">
<attribute name="NAME" x="-266.4714" y="73.7616" size="1.778" layer="95"/>
<attribute name="VALUE" x="-266.7254" y="66.6242" size="1.778" layer="96"/>
</instance>
<instance part="IC10" gate="G$1" x="-172.72" y="99.06" smashed="yes"/>
<instance part="IC6" gate="G$1" x="30.48" y="154.94" smashed="yes"/>
<instance part="C32" gate="G$1" x="-218.44" y="-30.48" smashed="yes">
<attribute name="NAME" x="-217.297" y="-29.9974" size="1.778" layer="95"/>
<attribute name="VALUE" x="-217.297" y="-35.0774" size="1.778" layer="96"/>
</instance>
<instance part="Q2" gate="G$1" x="-203.2" y="-33.02" smashed="yes" rot="R180">
<attribute name="NAME" x="-196.85" y="-34.29" size="1.778" layer="95" rot="MR0"/>
</instance>
<instance part="U1" gate="G$1" x="-205.74" y="-2.54" smashed="yes" rot="R180"/>
<instance part="GND10" gate="1" x="-218.44" y="-40.64" smashed="yes">
<attribute name="VALUE" x="-218.44" y="-40.894" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="D20" gate="G$1" x="-254" y="2.54" smashed="yes" rot="R90">
<attribute name="NAME" x="-256.6416" y="0.2286" size="1.778" layer="95" rot="R90"/>
</instance>
<instance part="SUPPLY3" gate="G$1" x="-254" y="7.62" smashed="yes">
<attribute name="VALUE" x="-254" y="10.414" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="MOTEUR1" gate="G$1" x="-274.32" y="-10.16" smashed="yes"/>
<instance part="GND19" gate="1" x="-264.16" y="-22.86" smashed="yes">
<attribute name="VALUE" x="-264.16" y="-23.114" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="GND21" gate="1" x="-208.28" y="30.48" smashed="yes">
<attribute name="VALUE" x="-208.28" y="30.226" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="R14" gate="G$1" x="-238.76" y="-73.66" smashed="yes">
<attribute name="NAME" x="-241.3" y="-71.12" size="1.778" layer="95"/>
<attribute name="VALUE" x="-241.3" y="-77.47" size="1.778" layer="96"/>
</instance>
<instance part="R15" gate="G$1" x="-231.14" y="-60.96" smashed="yes" rot="R90">
<attribute name="NAME" x="-233.68" y="-63.5" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-227.33" y="-63.5" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R16" gate="G$1" x="-190.5" y="-50.8" smashed="yes">
<attribute name="NAME" x="-193.04" y="-48.26" size="1.778" layer="95"/>
<attribute name="VALUE" x="-193.04" y="-54.61" size="1.778" layer="96"/>
</instance>
<instance part="R17" gate="G$1" x="-175.26" y="-73.66" smashed="yes">
<attribute name="NAME" x="-177.8" y="-71.12" size="1.778" layer="95"/>
<attribute name="VALUE" x="-177.8" y="-77.47" size="1.778" layer="96"/>
</instance>
<instance part="R18" gate="G$1" x="-175.26" y="-78.74" smashed="yes">
<attribute name="NAME" x="-177.8" y="-76.2" size="1.778" layer="95"/>
<attribute name="VALUE" x="-177.8" y="-82.55" size="1.778" layer="96"/>
</instance>
<instance part="R19" gate="G$1" x="-175.26" y="-83.82" smashed="yes">
<attribute name="NAME" x="-177.8" y="-81.28" size="1.778" layer="95"/>
<attribute name="VALUE" x="-177.8" y="-87.63" size="1.778" layer="96"/>
</instance>
<instance part="C4" gate="G$1" x="-246.38" y="-60.96" smashed="yes">
<attribute name="NAME" x="-243.84" y="-58.42" size="1.778" layer="95"/>
<attribute name="VALUE" x="-243.84" y="-60.96" size="1.778" layer="96"/>
</instance>
<instance part="GND9" gate="1" x="-259.08" y="-55.88" smashed="yes">
<attribute name="VALUE" x="-259.08" y="-56.134" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="SUPPLY6" gate="G$1" x="-165.1" y="-58.42" smashed="yes">
<attribute name="VALUE" x="-165.1" y="-55.626" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="R20" gate="G$1" x="-165.1" y="-66.04" smashed="yes" rot="R90">
<attribute name="NAME" x="-167.64" y="-68.58" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-161.29" y="-68.58" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C5" gate="G$1" x="-218.44" y="-101.6" smashed="yes">
<attribute name="NAME" x="-217.297" y="-101.1174" size="1.778" layer="95"/>
<attribute name="VALUE" x="-217.297" y="-106.1974" size="1.778" layer="96"/>
</instance>
<instance part="Q4" gate="G$1" x="-203.2" y="-104.14" smashed="yes" rot="R180">
<attribute name="NAME" x="-196.85" y="-105.41" size="1.778" layer="95" rot="MR0"/>
</instance>
<instance part="U2" gate="G$1" x="-205.74" y="-73.66" smashed="yes" rot="R180"/>
<instance part="GND27" gate="1" x="-218.44" y="-111.76" smashed="yes">
<attribute name="VALUE" x="-218.44" y="-112.014" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="D4" gate="G$1" x="-254" y="-68.58" smashed="yes" rot="R90">
<attribute name="NAME" x="-256.6416" y="-70.8914" size="1.778" layer="95" rot="R90"/>
</instance>
<instance part="SUPPLY7" gate="G$1" x="-254" y="-63.5" smashed="yes">
<attribute name="VALUE" x="-254" y="-60.706" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="MOTEUR2" gate="G$1" x="-274.32" y="-81.28" smashed="yes"/>
<instance part="GND28" gate="1" x="-264.16" y="-93.98" smashed="yes">
<attribute name="VALUE" x="-264.16" y="-94.234" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="R52" gate="G$1" x="-238.76" y="-142.24" smashed="yes">
<attribute name="NAME" x="-241.3" y="-139.7" size="1.778" layer="95"/>
<attribute name="VALUE" x="-241.3" y="-146.05" size="1.778" layer="96"/>
</instance>
<instance part="R57" gate="G$1" x="-231.14" y="-129.54" smashed="yes" rot="R90">
<attribute name="NAME" x="-233.68" y="-132.08" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-227.33" y="-132.08" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R58" gate="G$1" x="-190.5" y="-119.38" smashed="yes">
<attribute name="NAME" x="-193.04" y="-116.84" size="1.778" layer="95"/>
<attribute name="VALUE" x="-193.04" y="-123.19" size="1.778" layer="96"/>
</instance>
<instance part="R59" gate="G$1" x="-175.26" y="-142.24" smashed="yes">
<attribute name="NAME" x="-177.8" y="-139.7" size="1.778" layer="95"/>
<attribute name="VALUE" x="-177.8" y="-146.05" size="1.778" layer="96"/>
</instance>
<instance part="R60" gate="G$1" x="-175.26" y="-147.32" smashed="yes">
<attribute name="NAME" x="-177.8" y="-144.78" size="1.778" layer="95"/>
<attribute name="VALUE" x="-177.8" y="-151.13" size="1.778" layer="96"/>
</instance>
<instance part="R61" gate="G$1" x="-175.26" y="-152.4" smashed="yes">
<attribute name="NAME" x="-177.8" y="-149.86" size="1.778" layer="95"/>
<attribute name="VALUE" x="-177.8" y="-156.21" size="1.778" layer="96"/>
</instance>
<instance part="C9" gate="G$1" x="-246.38" y="-129.54" smashed="yes">
<attribute name="NAME" x="-243.84" y="-127" size="1.778" layer="95"/>
<attribute name="VALUE" x="-243.84" y="-129.54" size="1.778" layer="96"/>
</instance>
<instance part="GND32" gate="1" x="-259.08" y="-124.46" smashed="yes">
<attribute name="VALUE" x="-259.08" y="-124.714" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="SUPPLY11" gate="G$1" x="-165.1" y="-127" smashed="yes">
<attribute name="VALUE" x="-165.1" y="-124.206" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="R62" gate="G$1" x="-165.1" y="-134.62" smashed="yes" rot="R90">
<attribute name="NAME" x="-167.64" y="-137.16" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-161.29" y="-137.16" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C19" gate="G$1" x="-218.44" y="-170.18" smashed="yes">
<attribute name="NAME" x="-217.297" y="-169.6974" size="1.778" layer="95"/>
<attribute name="VALUE" x="-217.297" y="-174.7774" size="1.778" layer="96"/>
</instance>
<instance part="Q6" gate="G$1" x="-203.2" y="-172.72" smashed="yes" rot="R180">
<attribute name="NAME" x="-196.85" y="-173.99" size="1.778" layer="95" rot="MR0"/>
</instance>
<instance part="U3" gate="G$1" x="-205.74" y="-142.24" smashed="yes" rot="R180"/>
<instance part="GND33" gate="1" x="-218.44" y="-180.34" smashed="yes">
<attribute name="VALUE" x="-218.44" y="-180.594" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="D13" gate="G$1" x="-254" y="-137.16" smashed="yes" rot="R90">
<attribute name="NAME" x="-256.6416" y="-139.4714" size="1.778" layer="95" rot="R90"/>
</instance>
<instance part="SUPPLY12" gate="G$1" x="-254" y="-132.08" smashed="yes">
<attribute name="VALUE" x="-254" y="-129.286" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="MOTEUR3" gate="G$1" x="-274.32" y="-149.86" smashed="yes"/>
<instance part="GND34" gate="1" x="-264.16" y="-162.56" smashed="yes">
<attribute name="VALUE" x="-264.16" y="-162.814" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="L3" gate="G$1" x="10.16" y="154.94" smashed="yes"/>
</instances>
<busses>
</busses>
<nets>
<net name="VPOWER" class="0">
<segment>
<label x="-190.5" y="165.1" size="1.778" layer="95"/>
<pinref part="F5" gate="G$1" pin="2"/>
<wire x1="-190.5" y1="165.1" x2="-205.74" y2="165.1" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="Q2" gate="G$1" pin="S"/>
<pinref part="U1" gate="G$1" pin="VBAT"/>
<wire x1="-205.74" y1="-27.94" x2="-205.74" y2="-25.4" width="0.1524" layer="91"/>
<wire x1="-205.74" y1="-25.4" x2="-205.74" y2="-22.86" width="0.1524" layer="91"/>
<wire x1="-195.58" y1="-25.4" x2="-205.74" y2="-25.4" width="0.1524" layer="91"/>
<junction x="-205.74" y="-25.4"/>
<label x="-195.58" y="-25.4" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="Q4" gate="G$1" pin="S"/>
<pinref part="U2" gate="G$1" pin="VBAT"/>
<wire x1="-205.74" y1="-99.06" x2="-205.74" y2="-96.52" width="0.1524" layer="91"/>
<wire x1="-205.74" y1="-96.52" x2="-205.74" y2="-93.98" width="0.1524" layer="91"/>
<wire x1="-195.58" y1="-96.52" x2="-205.74" y2="-96.52" width="0.1524" layer="91"/>
<junction x="-205.74" y="-96.52"/>
<label x="-195.58" y="-96.52" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="Q6" gate="G$1" pin="S"/>
<pinref part="U3" gate="G$1" pin="VBAT"/>
<wire x1="-205.74" y1="-167.64" x2="-205.74" y2="-165.1" width="0.1524" layer="91"/>
<wire x1="-205.74" y1="-165.1" x2="-205.74" y2="-162.56" width="0.1524" layer="91"/>
<wire x1="-195.58" y1="-165.1" x2="-205.74" y2="-165.1" width="0.1524" layer="91"/>
<junction x="-205.74" y="-165.1"/>
<label x="-195.58" y="-165.1" size="1.778" layer="95"/>
</segment>
</net>
<net name="VSENSE" class="0">
<segment>
<pinref part="R34" gate="G$1" pin="2"/>
<wire x1="-162.56" y1="160.02" x2="-162.56" y2="157.48" width="0.1524" layer="91"/>
<label x="-162.56" y="160.02" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="F4" gate="G$1" pin="2"/>
<pinref part="D16" gate="G$1" pin="A"/>
<wire x1="-205.74" y1="137.16" x2="-203.2" y2="137.16" width="0.1524" layer="91"/>
<wire x1="-203.2" y1="137.16" x2="-200.66" y2="137.16" width="0.1524" layer="91"/>
<wire x1="-203.2" y1="137.16" x2="-203.2" y2="142.24" width="0.1524" layer="91"/>
<junction x="-203.2" y="137.16"/>
<label x="-203.2" y="142.24" size="1.778" layer="95"/>
</segment>
</net>
<net name="A2" class="0">
<segment>
<wire x1="-119.38" y1="-76.2" x2="-124.46" y2="-76.2" width="0.1524" layer="91"/>
<label x="-119.38" y="-76.2" size="1.778" layer="95"/>
<pinref part="PASAPAS" gate="G$1" pin="3"/>
</segment>
<segment>
<wire x1="-226.06" y1="-83.82" x2="-266.7" y2="-83.82" width="0.1524" layer="91"/>
<label x="-246.38" y="-83.82" size="1.778" layer="95"/>
<pinref part="U2" gate="G$1" pin="OUTA"/>
<pinref part="MOTEUR2" gate="G$1" pin="3"/>
</segment>
</net>
<net name="B2" class="0">
<segment>
<wire x1="-119.38" y1="-81.28" x2="-124.46" y2="-81.28" width="0.1524" layer="91"/>
<label x="-119.38" y="-81.28" size="1.778" layer="95"/>
<pinref part="PASAPAS" gate="G$1" pin="4"/>
</segment>
<segment>
<wire x1="-266.7" y1="-78.74" x2="-226.06" y2="-78.74" width="0.1524" layer="91"/>
<label x="-246.38" y="-78.74" size="1.778" layer="95"/>
<pinref part="U2" gate="G$1" pin="OUTB"/>
<pinref part="MOTEUR2" gate="G$1" pin="2"/>
</segment>
</net>
<net name="A1" class="0">
<segment>
<wire x1="-226.06" y1="-12.7" x2="-266.7" y2="-12.7" width="0.1524" layer="91"/>
<label x="-246.38" y="-12.7" size="1.778" layer="95"/>
<pinref part="U1" gate="G$1" pin="OUTA"/>
<pinref part="MOTEUR1" gate="G$1" pin="3"/>
</segment>
<segment>
<wire x1="-119.38" y1="-66.04" x2="-124.46" y2="-66.04" width="0.1524" layer="91"/>
<label x="-119.38" y="-66.04" size="1.778" layer="95"/>
<pinref part="PASAPAS" gate="G$1" pin="1"/>
</segment>
</net>
<net name="B1" class="0">
<segment>
<wire x1="-266.7" y1="-7.62" x2="-226.06" y2="-7.62" width="0.1524" layer="91"/>
<label x="-246.38" y="-7.62" size="1.778" layer="95"/>
<pinref part="U1" gate="G$1" pin="OUTB"/>
<pinref part="MOTEUR1" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="-119.38" y1="-71.12" x2="-124.46" y2="-71.12" width="0.1524" layer="91"/>
<label x="-119.38" y="-71.12" size="1.778" layer="95"/>
<pinref part="PASAPAS" gate="G$1" pin="2"/>
</segment>
</net>
<net name="ISENSE1" class="0">
<segment>
<wire x1="-246.38" y1="5.08" x2="-246.38" y2="-2.54" width="0.1524" layer="91"/>
<pinref part="R2" gate="G$1" pin="1"/>
<wire x1="-246.38" y1="-2.54" x2="-243.84" y2="-2.54" width="0.1524" layer="91"/>
<wire x1="-256.54" y1="-2.54" x2="-254" y2="-2.54" width="0.1524" layer="91"/>
<junction x="-246.38" y="-2.54"/>
<label x="-256.54" y="-2.54" size="1.778" layer="95" rot="MR0"/>
<pinref part="C7" gate="G$1" pin="2"/>
<pinref part="D20" gate="G$1" pin="A"/>
<wire x1="-254" y1="-2.54" x2="-246.38" y2="-2.54" width="0.1524" layer="91"/>
<wire x1="-254" y1="0" x2="-254" y2="-2.54" width="0.1524" layer="91"/>
<junction x="-254" y="-2.54"/>
</segment>
<segment>
<pinref part="SH1" gate="G$1" pin="AN6/DAC1RM/RP16/CN8/RC0"/>
<wire x1="27.94" y1="-50.8" x2="25.4" y2="-50.8" width="0.1524" layer="91"/>
<label x="27.94" y="-50.8" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="R2" gate="G$1" pin="2"/>
<wire x1="-233.68" y1="-2.54" x2="-231.14" y2="-2.54" width="0.1524" layer="91"/>
<pinref part="R1" gate="G$1" pin="1"/>
<wire x1="-231.14" y1="-2.54" x2="-226.06" y2="-2.54" width="0.1524" layer="91"/>
<wire x1="-231.14" y1="5.08" x2="-231.14" y2="-2.54" width="0.1524" layer="91"/>
<junction x="-231.14" y="-2.54"/>
<pinref part="U1" gate="G$1" pin="CS"/>
</segment>
</net>
<net name="ISENSE2" class="0">
<segment>
<pinref part="SH1" gate="G$1" pin="AN7/DAC1LM/RP17/CN9/RC1"/>
<wire x1="27.94" y1="-45.72" x2="25.4" y2="-45.72" width="0.1524" layer="91"/>
<label x="27.94" y="-45.72" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="-246.38" y1="-66.04" x2="-246.38" y2="-73.66" width="0.1524" layer="91"/>
<pinref part="R14" gate="G$1" pin="1"/>
<wire x1="-246.38" y1="-73.66" x2="-243.84" y2="-73.66" width="0.1524" layer="91"/>
<wire x1="-256.54" y1="-73.66" x2="-254" y2="-73.66" width="0.1524" layer="91"/>
<junction x="-246.38" y="-73.66"/>
<label x="-256.54" y="-73.66" size="1.778" layer="95" rot="MR0"/>
<pinref part="C4" gate="G$1" pin="2"/>
<pinref part="D4" gate="G$1" pin="A"/>
<wire x1="-254" y1="-73.66" x2="-246.38" y2="-73.66" width="0.1524" layer="91"/>
<wire x1="-254" y1="-71.12" x2="-254" y2="-73.66" width="0.1524" layer="91"/>
<junction x="-254" y="-73.66"/>
</segment>
</net>
<net name="ISENSE3" class="0">
<segment>
<pinref part="SH1" gate="G$1" pin="AN8/CVREF/RP18/PMA2/CN10/RC2"/>
<wire x1="27.94" y1="-40.64" x2="25.4" y2="-40.64" width="0.1524" layer="91"/>
<label x="27.94" y="-40.64" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="-246.38" y1="-134.62" x2="-246.38" y2="-142.24" width="0.1524" layer="91"/>
<pinref part="R52" gate="G$1" pin="1"/>
<wire x1="-246.38" y1="-142.24" x2="-243.84" y2="-142.24" width="0.1524" layer="91"/>
<wire x1="-256.54" y1="-142.24" x2="-254" y2="-142.24" width="0.1524" layer="91"/>
<junction x="-246.38" y="-142.24"/>
<label x="-256.54" y="-142.24" size="1.778" layer="95" rot="MR0"/>
<pinref part="C9" gate="G$1" pin="2"/>
<pinref part="D13" gate="G$1" pin="A"/>
<wire x1="-254" y1="-142.24" x2="-246.38" y2="-142.24" width="0.1524" layer="91"/>
<wire x1="-254" y1="-139.7" x2="-254" y2="-142.24" width="0.1524" layer="91"/>
<junction x="-254" y="-142.24"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<pinref part="R3" gate="G$1" pin="1"/>
<wire x1="-198.12" y1="17.78" x2="-198.12" y2="20.32" width="0.1524" layer="91"/>
<wire x1="-198.12" y1="20.32" x2="-195.58" y2="20.32" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="PWM"/>
</segment>
</net>
<net name="N$24" class="0">
<segment>
<pinref part="R6" gate="G$1" pin="1"/>
<wire x1="-180.34" y1="-7.62" x2="-185.42" y2="-7.62" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="INB"/>
</segment>
</net>
<net name="N$25" class="0">
<segment>
<pinref part="R7" gate="G$1" pin="1"/>
<wire x1="-185.42" y1="-12.7" x2="-180.34" y2="-12.7" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="INA"/>
</segment>
</net>
<net name="INA1" class="0">
<segment>
<pinref part="R7" gate="G$1" pin="2"/>
<wire x1="-167.64" y1="-12.7" x2="-170.18" y2="-12.7" width="0.1524" layer="91"/>
<label x="-167.64" y="-12.7" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="SH1" gate="G$1" pin="PWM1H1/DAC1LP/RTCC/RP14/CN12/PMWR/RB14"/>
<wire x1="-91.44" y1="-20.32" x2="-88.9" y2="-20.32" width="0.1524" layer="91"/>
<label x="-91.44" y="-20.32" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="INB1" class="0">
<segment>
<pinref part="R6" gate="G$1" pin="2"/>
<wire x1="-167.64" y1="-7.62" x2="-170.18" y2="-7.62" width="0.1524" layer="91"/>
<label x="-167.64" y="-7.62" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="SH1" gate="G$1" pin="TDO/PMA8/RA8"/>
<wire x1="27.94" y1="-15.24" x2="25.4" y2="-15.24" width="0.1524" layer="91"/>
<label x="27.94" y="-15.24" size="1.778" layer="95"/>
</segment>
</net>
<net name="EN1" class="0">
<segment>
<pinref part="R5" gate="G$1" pin="2"/>
<wire x1="-162.56" y1="-2.54" x2="-165.1" y2="-2.54" width="0.1524" layer="91"/>
<label x="-162.56" y="-2.54" size="1.778" layer="95"/>
<wire x1="-165.1" y1="-2.54" x2="-170.18" y2="-2.54" width="0.1524" layer="91"/>
<junction x="-165.1" y="-2.54"/>
<wire x1="-165.1" y1="0" x2="-165.1" y2="-2.54" width="0.1524" layer="91"/>
<pinref part="R29" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="SH1" gate="G$1" pin="RP19/CN28/PMBE/RC3"/>
<wire x1="27.94" y1="5.08" x2="25.4" y2="5.08" width="0.1524" layer="91"/>
<label x="27.94" y="5.08" size="1.778" layer="95"/>
</segment>
</net>
<net name="PWM1" class="0">
<segment>
<pinref part="R3" gate="G$1" pin="2"/>
<wire x1="-182.88" y1="20.32" x2="-185.42" y2="20.32" width="0.1524" layer="91"/>
<label x="-182.88" y="20.32" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="SH1" gate="G$1" pin="PWM1L1/DAC1LN/RP15/CN11/PMCS1/RB15"/>
<wire x1="-91.44" y1="-25.4" x2="-88.9" y2="-25.4" width="0.1524" layer="91"/>
<label x="-91.44" y="-25.4" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="PWM2" class="0">
<segment>
<pinref part="SH1" gate="G$1" pin="PWM1L2/DAC1RN/RP13/CN13/PMRD/RB13"/>
<wire x1="-91.44" y1="-5.08" x2="-88.9" y2="-5.08" width="0.1524" layer="91"/>
<label x="-91.44" y="-5.08" size="1.778" layer="95" rot="MR0"/>
</segment>
<segment>
<pinref part="R16" gate="G$1" pin="2"/>
<wire x1="-182.88" y1="-50.8" x2="-185.42" y2="-50.8" width="0.1524" layer="91"/>
<label x="-182.88" y="-50.8" size="1.778" layer="95"/>
</segment>
</net>
<net name="INA2" class="0">
<segment>
<pinref part="SH1" gate="G$1" pin="PWM1H2/DAC1RP/RP12/CN14/PMD0/RB12"/>
<wire x1="-88.9" y1="0" x2="-91.44" y2="0" width="0.1524" layer="91"/>
<label x="-91.44" y="0" size="1.778" layer="95" rot="MR0"/>
</segment>
<segment>
<pinref part="R19" gate="G$1" pin="2"/>
<wire x1="-167.64" y1="-83.82" x2="-170.18" y2="-83.82" width="0.1524" layer="91"/>
<label x="-167.64" y="-83.82" size="1.778" layer="95"/>
</segment>
</net>
<net name="INB2" class="0">
<segment>
<pinref part="SH1" gate="G$1" pin="SOSCI/RP4/CN1/RB4"/>
<wire x1="27.94" y1="-10.16" x2="25.4" y2="-10.16" width="0.1524" layer="91"/>
<label x="27.94" y="-10.16" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R18" gate="G$1" pin="2"/>
<wire x1="-167.64" y1="-78.74" x2="-170.18" y2="-78.74" width="0.1524" layer="91"/>
<label x="-167.64" y="-78.74" size="1.778" layer="95"/>
</segment>
</net>
<net name="EN2" class="0">
<segment>
<pinref part="SH1" gate="G$1" pin="RP20/CN25/PMA4/RC4"/>
<wire x1="27.94" y1="10.16" x2="25.4" y2="10.16" width="0.1524" layer="91"/>
<label x="27.94" y="10.16" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R17" gate="G$1" pin="2"/>
<wire x1="-162.56" y1="-73.66" x2="-165.1" y2="-73.66" width="0.1524" layer="91"/>
<label x="-162.56" y="-73.66" size="1.778" layer="95"/>
<wire x1="-165.1" y1="-73.66" x2="-170.18" y2="-73.66" width="0.1524" layer="91"/>
<junction x="-165.1" y="-73.66"/>
<wire x1="-165.1" y1="-71.12" x2="-165.1" y2="-73.66" width="0.1524" layer="91"/>
<pinref part="R20" gate="G$1" pin="1"/>
</segment>
</net>
<net name="INA3" class="0">
<segment>
<pinref part="SH1" gate="G$1" pin="TMS/PMA10/RA10"/>
<wire x1="-91.44" y1="-10.16" x2="-88.9" y2="-10.16" width="0.1524" layer="91"/>
<label x="-91.44" y="-10.16" size="1.778" layer="95" rot="MR0"/>
</segment>
<segment>
<pinref part="R61" gate="G$1" pin="2"/>
<wire x1="-167.64" y1="-152.4" x2="-170.18" y2="-152.4" width="0.1524" layer="91"/>
<label x="-167.64" y="-152.4" size="1.778" layer="95"/>
</segment>
</net>
<net name="INB3" class="0">
<segment>
<pinref part="SH1" gate="G$1" pin="TCK/PMA7/RA7"/>
<wire x1="-91.44" y1="-15.24" x2="-88.9" y2="-15.24" width="0.1524" layer="91"/>
<label x="-91.44" y="-15.24" size="1.778" layer="95" rot="MR0"/>
</segment>
<segment>
<pinref part="R60" gate="G$1" pin="2"/>
<wire x1="-167.64" y1="-147.32" x2="-170.18" y2="-147.32" width="0.1524" layer="91"/>
<label x="-167.64" y="-147.32" size="1.778" layer="95"/>
</segment>
</net>
<net name="EN3" class="0">
<segment>
<pinref part="SH1" gate="G$1" pin="RP21/CN26/PMA3/RC5"/>
<wire x1="27.94" y1="15.24" x2="25.4" y2="15.24" width="0.1524" layer="91"/>
<label x="27.94" y="15.24" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R59" gate="G$1" pin="2"/>
<wire x1="-162.56" y1="-142.24" x2="-165.1" y2="-142.24" width="0.1524" layer="91"/>
<label x="-162.56" y="-142.24" size="1.778" layer="95"/>
<wire x1="-165.1" y1="-142.24" x2="-170.18" y2="-142.24" width="0.1524" layer="91"/>
<junction x="-165.1" y="-142.24"/>
<wire x1="-165.1" y1="-139.7" x2="-165.1" y2="-142.24" width="0.1524" layer="91"/>
<pinref part="R62" gate="G$1" pin="1"/>
</segment>
</net>
<net name="PWM3" class="0">
<segment>
<pinref part="SH1" gate="G$1" pin="PGEC2/PWM1L3/RP11/CN15/PMD1/RB11"/>
<wire x1="-91.44" y1="5.08" x2="-88.9" y2="5.08" width="0.1524" layer="91"/>
<label x="-91.44" y="5.08" size="1.778" layer="95" rot="MR0"/>
</segment>
<segment>
<pinref part="R58" gate="G$1" pin="2"/>
<wire x1="-182.88" y1="-119.38" x2="-185.42" y2="-119.38" width="0.1524" layer="91"/>
<label x="-182.88" y="-119.38" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="C12" gate="G$1" pin="1"/>
<wire x1="66.04" y1="91.44" x2="66.04" y2="93.98" width="0.1524" layer="91"/>
<pinref part="IC4" gate="G$1" pin="3V3OUT"/>
<wire x1="66.04" y1="93.98" x2="73.66" y2="93.98" width="0.1524" layer="91"/>
</segment>
</net>
<net name="DTR" class="0">
<segment>
<pinref part="IC4" gate="G$1" pin="_DTR"/>
<wire x1="119.38" y1="134.62" x2="121.92" y2="134.62" width="0.1524" layer="91"/>
<label x="121.92" y="134.62" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="C13" gate="G$1" pin="2"/>
<wire x1="-76.2" y1="104.14" x2="-78.74" y2="104.14" width="0.1524" layer="91"/>
<label x="-76.2" y="104.14" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<pinref part="R22" gate="G$1" pin="1"/>
<pinref part="IC4" gate="G$1" pin="CBUS0"/>
<wire x1="124.46" y1="114.3" x2="119.38" y2="114.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$29" class="0">
<segment>
<pinref part="IC4" gate="G$1" pin="CBUS1"/>
<pinref part="R23" gate="G$1" pin="1"/>
<wire x1="119.38" y1="109.22" x2="124.46" y2="109.22" width="0.1524" layer="91"/>
</segment>
</net>
<net name="_MCLR" class="0">
<segment>
<wire x1="-96.52" y1="104.14" x2="-88.9" y2="104.14" width="0.1524" layer="91"/>
<wire x1="-88.9" y1="104.14" x2="-86.36" y2="104.14" width="0.1524" layer="91"/>
<junction x="-88.9" y="104.14"/>
<wire x1="-76.2" y1="93.98" x2="-88.9" y2="93.98" width="0.1524" layer="91"/>
<wire x1="-88.9" y1="93.98" x2="-88.9" y2="104.14" width="0.1524" layer="91"/>
<label x="-76.2" y="93.98" size="1.778" layer="95"/>
<pinref part="C13" gate="G$1" pin="1"/>
<wire x1="-88.9" y1="104.14" x2="-88.9" y2="116.84" width="0.1524" layer="91"/>
<pinref part="ICSP" gate="G$1" pin="1"/>
<wire x1="-88.9" y1="116.84" x2="-86.36" y2="116.84" width="0.1524" layer="91"/>
<pinref part="S1" gate="G$1" pin="S"/>
</segment>
<segment>
<pinref part="SH1" gate="G$1" pin="_MCLR"/>
<wire x1="-91.44" y1="-40.64" x2="-88.9" y2="-40.64" width="0.1524" layer="91"/>
<label x="-91.44" y="-40.64" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="PGEC" class="0">
<segment>
<pinref part="ICSP" gate="G$1" pin="5"/>
<wire x1="-88.9" y1="137.16" x2="-86.36" y2="137.16" width="0.1524" layer="91"/>
<label x="-88.9" y="137.16" size="1.778" layer="95" rot="MR0"/>
</segment>
<segment>
<pinref part="SH1" gate="G$1" pin="PGEC3/ASCL1/RP6/CN24/PMD6/RB6"/>
<wire x1="27.94" y1="35.56" x2="25.4" y2="35.56" width="0.1524" layer="91"/>
<label x="27.94" y="35.56" size="1.778" layer="95"/>
</segment>
</net>
<net name="PGED" class="0">
<segment>
<pinref part="ICSP" gate="G$1" pin="4"/>
<wire x1="-88.9" y1="132.08" x2="-86.36" y2="132.08" width="0.1524" layer="91"/>
<label x="-88.9" y="132.08" size="1.778" layer="95" rot="MR0"/>
</segment>
<segment>
<pinref part="SH1" gate="G$1" pin="PGED3/ASDA1/RP5/CN27/PMD7/RB5"/>
<wire x1="27.94" y1="30.48" x2="25.4" y2="30.48" width="0.1524" layer="91"/>
<label x="27.94" y="30.48" size="1.778" layer="95"/>
</segment>
</net>
<net name="5V" class="0">
<segment>
<pinref part="SUPPLY1" gate="G$1" pin="5V"/>
<wire x1="-104.14" y1="129.54" x2="-104.14" y2="121.92" width="0.1524" layer="91"/>
<pinref part="ICSP" gate="G$1" pin="2"/>
<wire x1="-104.14" y1="121.92" x2="-86.36" y2="121.92" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R29" gate="G$1" pin="2"/>
<wire x1="-165.1" y1="10.16" x2="-165.1" y2="12.7" width="0.1524" layer="91"/>
<pinref part="SUPPLY10" gate="G$1" pin="5V"/>
</segment>
<segment>
<wire x1="-60.96" y1="-137.16" x2="-96.52" y2="-137.16" width="0.1524" layer="91"/>
<pinref part="SUPPLY18" gate="G$1" pin="5V"/>
<wire x1="-96.52" y1="-137.16" x2="-96.52" y2="-134.62" width="0.1524" layer="91"/>
<pinref part="C14" gate="G$1" pin="1"/>
<wire x1="-96.52" y1="-137.16" x2="-96.52" y2="-139.7" width="0.1524" layer="91"/>
<junction x="-96.52" y="-137.16"/>
<wire x1="-60.96" y1="-137.16" x2="-60.96" y2="-144.78" width="0.1524" layer="91"/>
<pinref part="INCREMENTAL2" gate="G$1" pin="4"/>
<wire x1="-60.96" y1="-144.78" x2="-58.42" y2="-144.78" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SUPPLY17" gate="G$1" pin="5V"/>
<pinref part="C15" gate="G$1" pin="1"/>
<wire x1="-96.52" y1="-91.44" x2="-96.52" y2="-93.98" width="0.1524" layer="91"/>
<wire x1="-96.52" y1="-93.98" x2="-96.52" y2="-96.52" width="0.1524" layer="91"/>
<wire x1="-96.52" y1="-93.98" x2="-58.42" y2="-93.98" width="0.1524" layer="91"/>
<junction x="-96.52" y="-93.98"/>
<pinref part="INCREMENTAL1" gate="G$1" pin="4"/>
<wire x1="-58.42" y1="-93.98" x2="-58.42" y2="-101.6" width="0.1524" layer="91"/>
<wire x1="-58.42" y1="-101.6" x2="-55.88" y2="-101.6" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC5" gate="G$1" pin="VCC"/>
<pinref part="SUPPLY23" gate="G$1" pin="5V"/>
<wire x1="88.9" y1="-109.22" x2="91.44" y2="-109.22" width="0.1524" layer="91"/>
<wire x1="91.44" y1="-109.22" x2="91.44" y2="-99.06" width="0.1524" layer="91"/>
<pinref part="C18" gate="G$1" pin="1"/>
<wire x1="91.44" y1="-99.06" x2="91.44" y2="-96.52" width="0.1524" layer="91"/>
<wire x1="91.44" y1="-99.06" x2="96.52" y2="-99.06" width="0.1524" layer="91"/>
<wire x1="96.52" y1="-99.06" x2="96.52" y2="-101.6" width="0.1524" layer="91"/>
<junction x="91.44" y="-99.06"/>
</segment>
<segment>
<pinref part="R50" gate="G$1" pin="2"/>
<wire x1="33.02" y1="-149.86" x2="33.02" y2="-152.4" width="0.1524" layer="91"/>
<wire x1="33.02" y1="-152.4" x2="33.02" y2="-162.56" width="0.1524" layer="91"/>
<wire x1="33.02" y1="-162.56" x2="35.56" y2="-162.56" width="0.1524" layer="91"/>
<pinref part="R49" gate="G$1" pin="2"/>
<wire x1="35.56" y1="-152.4" x2="33.02" y2="-152.4" width="0.1524" layer="91"/>
<junction x="33.02" y="-152.4"/>
<pinref part="SUPPLY24" gate="G$1" pin="5V"/>
</segment>
<segment>
<pinref part="SUPPLY19" gate="G$1" pin="5V"/>
<wire x1="144.78" y1="109.22" x2="147.32" y2="109.22" width="0.1524" layer="91"/>
<wire x1="147.32" y1="109.22" x2="147.32" y2="114.3" width="0.1524" layer="91"/>
<wire x1="147.32" y1="114.3" x2="147.32" y2="116.84" width="0.1524" layer="91"/>
<wire x1="147.32" y1="114.3" x2="144.78" y2="114.3" width="0.1524" layer="91"/>
<junction x="147.32" y="114.3"/>
<pinref part="D6" gate="G$1" pin="A"/>
<pinref part="D7" gate="G$1" pin="A"/>
</segment>
<segment>
<pinref part="R30" gate="G$1" pin="2"/>
<pinref part="SUPPLY28" gate="G$1" pin="5V"/>
<wire x1="38.1" y1="-101.6" x2="38.1" y2="-99.06" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SUPPLY32" gate="G$1" pin="5V"/>
<wire x1="-271.78" y1="76.2" x2="-271.78" y2="71.12" width="0.1524" layer="91"/>
<pinref part="D18" gate="G$1" pin="A"/>
<wire x1="-271.78" y1="71.12" x2="-266.7" y2="71.12" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC10" gate="G$1" pin="OUT"/>
<pinref part="R55" gate="G$1" pin="1"/>
<wire x1="-160.02" y1="104.14" x2="-157.48" y2="104.14" width="0.1524" layer="91"/>
<pinref part="SUPPLY31" gate="G$1" pin="5V"/>
<wire x1="-157.48" y1="104.14" x2="-154.94" y2="104.14" width="0.1524" layer="91"/>
<wire x1="-157.48" y1="109.22" x2="-157.48" y2="104.14" width="0.1524" layer="91"/>
<junction x="-157.48" y="104.14"/>
</segment>
<segment>
<pinref part="C11" gate="G$1" pin="1"/>
<wire x1="55.88" y1="129.54" x2="55.88" y2="149.86" width="0.1524" layer="91"/>
<pinref part="IC4" gate="G$1" pin="VCC"/>
<wire x1="55.88" y1="149.86" x2="73.66" y2="149.86" width="0.1524" layer="91"/>
<wire x1="55.88" y1="149.86" x2="45.72" y2="149.86" width="0.1524" layer="91"/>
<junction x="55.88" y="149.86"/>
<wire x1="45.72" y1="149.86" x2="45.72" y2="154.94" width="0.1524" layer="91"/>
<pinref part="C10" gate="G$1" pin="1"/>
<wire x1="45.72" y1="149.86" x2="45.72" y2="129.54" width="0.1524" layer="91"/>
<junction x="45.72" y="149.86"/>
<pinref part="IC4" gate="G$1" pin="VCCIO"/>
<wire x1="45.72" y1="154.94" x2="73.66" y2="154.94" width="0.1524" layer="91"/>
<pinref part="IC6" gate="G$1" pin="OUT"/>
<wire x1="43.18" y1="160.02" x2="45.72" y2="160.02" width="0.1524" layer="91"/>
<wire x1="45.72" y1="160.02" x2="45.72" y2="154.94" width="0.1524" layer="91"/>
<junction x="45.72" y="154.94"/>
<pinref part="SUPPLY2" gate="G$1" pin="5V"/>
<wire x1="45.72" y1="160.02" x2="45.72" y2="165.1" width="0.1524" layer="91"/>
<junction x="45.72" y="160.02"/>
</segment>
<segment>
<pinref part="R20" gate="G$1" pin="2"/>
<wire x1="-165.1" y1="-60.96" x2="-165.1" y2="-58.42" width="0.1524" layer="91"/>
<pinref part="SUPPLY6" gate="G$1" pin="5V"/>
</segment>
<segment>
<pinref part="R62" gate="G$1" pin="2"/>
<wire x1="-165.1" y1="-129.54" x2="-165.1" y2="-127" width="0.1524" layer="91"/>
<pinref part="SUPPLY11" gate="G$1" pin="5V"/>
</segment>
</net>
<net name="N$28" class="0">
<segment>
<pinref part="R24" gate="G$1" pin="1"/>
<pinref part="IC4" gate="G$1" pin="TXD"/>
<wire x1="121.92" y1="154.94" x2="119.38" y2="154.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$30" class="0">
<segment>
<pinref part="IC4" gate="G$1" pin="RXD"/>
<pinref part="R25" gate="G$1" pin="1"/>
<wire x1="119.38" y1="149.86" x2="121.92" y2="149.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="DSP_RX1" class="0">
<segment>
<pinref part="R24" gate="G$1" pin="2"/>
<wire x1="134.62" y1="154.94" x2="132.08" y2="154.94" width="0.1524" layer="91"/>
<label x="134.62" y="154.94" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="SH1" gate="G$1" pin="RP25/CN19/PMA6/RC9"/>
<wire x1="-91.44" y1="25.4" x2="-88.9" y2="25.4" width="0.1524" layer="91"/>
<label x="-91.44" y="25.4" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="DSP_TX1" class="0">
<segment>
<pinref part="R25" gate="G$1" pin="2"/>
<wire x1="134.62" y1="149.86" x2="132.08" y2="149.86" width="0.1524" layer="91"/>
<label x="134.62" y="149.86" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="SH1" gate="G$1" pin="RP24/CN20/PMA5/RC8"/>
<wire x1="-91.44" y1="30.48" x2="-88.9" y2="30.48" width="0.1524" layer="91"/>
<label x="-91.44" y="30.48" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="3.3V" class="0">
<segment>
<pinref part="SH1" gate="G$1" pin="AVDD"/>
<wire x1="-91.44" y1="-35.56" x2="-88.9" y2="-35.56" width="0.1524" layer="91"/>
<pinref part="SUPPLY13" gate="G$1" pin="3.3V"/>
</segment>
<segment>
<pinref part="SH1" gate="G$1" pin="VCAP"/>
<wire x1="-91.44" y1="15.24" x2="-88.9" y2="15.24" width="0.1524" layer="91"/>
<pinref part="SUPPLY14" gate="G$1" pin="3.3V"/>
</segment>
<segment>
<pinref part="SH1" gate="G$1" pin="VDD2"/>
<wire x1="27.94" y1="-35.56" x2="25.4" y2="-35.56" width="0.1524" layer="91"/>
<pinref part="SUPPLY16" gate="G$1" pin="3.3V"/>
</segment>
<segment>
<pinref part="CONTACT2" gate="G$1" pin="3"/>
<pinref part="SUPPLY21" gate="G$1" pin="3.3V"/>
<wire x1="127" y1="55.88" x2="124.46" y2="55.88" width="0.1524" layer="91"/>
<wire x1="124.46" y1="55.88" x2="124.46" y2="60.96" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="CONTACT1" gate="G$1" pin="3"/>
<pinref part="SUPPLY22" gate="G$1" pin="3.3V"/>
<wire x1="127" y1="27.94" x2="124.46" y2="27.94" width="0.1524" layer="91"/>
<wire x1="124.46" y1="27.94" x2="124.46" y2="33.02" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R43" gate="G$1" pin="2"/>
<wire x1="55.88" y1="-5.08" x2="58.42" y2="-5.08" width="0.1524" layer="91"/>
<pinref part="SUPPLY20" gate="G$1" pin="3.3V"/>
<wire x1="58.42" y1="-5.08" x2="58.42" y2="-2.54" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SUPPLY27" gate="G$1" pin="3.3V"/>
<wire x1="-22.86" y1="96.52" x2="-25.4" y2="96.52" width="0.1524" layer="91"/>
<wire x1="-25.4" y1="96.52" x2="-25.4" y2="99.06" width="0.1524" layer="91"/>
<pinref part="I2C" gate="G$1" pin="4"/>
</segment>
<segment>
<pinref part="SUPPLY29" gate="G$1" pin="3.3V"/>
<wire x1="-121.92" y1="12.7" x2="-121.92" y2="10.16" width="0.1524" layer="91"/>
<pinref part="R53" gate="G$1" pin="1"/>
<wire x1="-121.92" y1="10.16" x2="-119.38" y2="10.16" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C20" gate="G$1" pin="1"/>
<pinref part="IC7" gate="G$1" pin="VOUT"/>
<wire x1="-190.5" y1="68.58" x2="-190.5" y2="71.12" width="0.1524" layer="91"/>
<wire x1="-190.5" y1="71.12" x2="-203.2" y2="71.12" width="0.1524" layer="91"/>
<pinref part="SUPPLY30" gate="G$1" pin="3.3V"/>
<wire x1="-203.2" y1="71.12" x2="-208.28" y2="71.12" width="0.1524" layer="91"/>
<wire x1="-190.5" y1="73.66" x2="-190.5" y2="71.12" width="0.1524" layer="91"/>
<junction x="-190.5" y="71.12"/>
<pinref part="C28" gate="G$1" pin="1"/>
<wire x1="-203.2" y1="68.58" x2="-203.2" y2="71.12" width="0.1524" layer="91"/>
<junction x="-203.2" y="71.12"/>
<pinref part="R56" gate="G$1" pin="1"/>
<wire x1="-180.34" y1="71.12" x2="-190.5" y2="71.12" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SUPPLY15" gate="G$1" pin="3.3V"/>
<pinref part="SH1" gate="G$1" pin="VDD3"/>
<wire x1="27.94" y1="25.4" x2="25.4" y2="25.4" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="D20" gate="G$1" pin="C"/>
<pinref part="SUPPLY3" gate="G$1" pin="3.3V"/>
<wire x1="-254" y1="5.08" x2="-254" y2="7.62" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="D4" gate="G$1" pin="C"/>
<pinref part="SUPPLY7" gate="G$1" pin="3.3V"/>
<wire x1="-254" y1="-66.04" x2="-254" y2="-63.5" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="D13" gate="G$1" pin="C"/>
<pinref part="SUPPLY12" gate="G$1" pin="3.3V"/>
<wire x1="-254" y1="-134.62" x2="-254" y2="-132.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="IC4" gate="G$1" pin="TEST"/>
<wire x1="101.6" y1="83.82" x2="101.6" y2="81.28" width="0.1524" layer="91"/>
<pinref part="C10" gate="G$1" pin="2"/>
<wire x1="101.6" y1="81.28" x2="96.52" y2="81.28" width="0.1524" layer="91"/>
<wire x1="96.52" y1="81.28" x2="91.44" y2="81.28" width="0.1524" layer="91"/>
<wire x1="91.44" y1="81.28" x2="73.66" y2="81.28" width="0.1524" layer="91"/>
<wire x1="73.66" y1="81.28" x2="66.04" y2="81.28" width="0.1524" layer="91"/>
<wire x1="66.04" y1="81.28" x2="55.88" y2="81.28" width="0.1524" layer="91"/>
<wire x1="55.88" y1="81.28" x2="45.72" y2="81.28" width="0.1524" layer="91"/>
<wire x1="45.72" y1="81.28" x2="45.72" y2="121.92" width="0.1524" layer="91"/>
<pinref part="C11" gate="G$1" pin="2"/>
<wire x1="55.88" y1="121.92" x2="55.88" y2="81.28" width="0.1524" layer="91"/>
<junction x="55.88" y="81.28"/>
<pinref part="C12" gate="G$1" pin="2"/>
<wire x1="66.04" y1="83.82" x2="66.04" y2="81.28" width="0.1524" layer="91"/>
<junction x="66.04" y="81.28"/>
<wire x1="73.66" y1="81.28" x2="73.66" y2="78.74" width="0.1524" layer="91"/>
<junction x="73.66" y="81.28"/>
<pinref part="IC4" gate="G$1" pin="AGND"/>
<wire x1="91.44" y1="83.82" x2="91.44" y2="81.28" width="0.1524" layer="91"/>
<junction x="91.44" y="81.28"/>
<pinref part="IC4" gate="G$1" pin="GND"/>
<wire x1="96.52" y1="83.82" x2="96.52" y2="81.28" width="0.1524" layer="91"/>
<junction x="96.52" y="81.28"/>
<pinref part="GND1" gate="1" pin="GND"/>
</segment>
<segment>
<wire x1="-109.22" y1="101.6" x2="-109.22" y2="104.14" width="0.1524" layer="91"/>
<wire x1="-109.22" y1="104.14" x2="-106.68" y2="104.14" width="0.1524" layer="91"/>
<pinref part="ICSP" gate="G$1" pin="3"/>
<wire x1="-86.36" y1="127" x2="-109.22" y2="127" width="0.1524" layer="91"/>
<wire x1="-109.22" y1="127" x2="-109.22" y2="104.14" width="0.1524" layer="91"/>
<junction x="-109.22" y="104.14"/>
<pinref part="GND3" gate="1" pin="GND"/>
<pinref part="S1" gate="G$1" pin="P"/>
</segment>
<segment>
<wire x1="-259.08" y1="17.78" x2="-259.08" y2="20.32" width="0.1524" layer="91"/>
<wire x1="-259.08" y1="20.32" x2="-246.38" y2="20.32" width="0.1524" layer="91"/>
<wire x1="-246.38" y1="20.32" x2="-231.14" y2="20.32" width="0.1524" layer="91"/>
<wire x1="-231.14" y1="20.32" x2="-215.9" y2="20.32" width="0.1524" layer="91"/>
<wire x1="-215.9" y1="20.32" x2="-210.82" y2="20.32" width="0.1524" layer="91"/>
<wire x1="-210.82" y1="20.32" x2="-210.82" y2="17.78" width="0.1524" layer="91"/>
<wire x1="-215.9" y1="17.78" x2="-215.9" y2="20.32" width="0.1524" layer="91"/>
<junction x="-215.9" y="20.32"/>
<pinref part="R1" gate="G$1" pin="2"/>
<wire x1="-231.14" y1="15.24" x2="-231.14" y2="20.32" width="0.1524" layer="91"/>
<junction x="-231.14" y="20.32"/>
<wire x1="-246.38" y1="12.7" x2="-246.38" y2="20.32" width="0.1524" layer="91"/>
<junction x="-246.38" y="20.32"/>
<pinref part="C7" gate="G$1" pin="1"/>
<pinref part="GND7" gate="1" pin="GND"/>
<pinref part="U1" gate="G$1" pin="GNDA"/>
<pinref part="U1" gate="G$1" pin="GNDB"/>
</segment>
<segment>
<pinref part="SH1" gate="G$1" pin="AVSS"/>
<wire x1="-91.44" y1="-30.48" x2="-88.9" y2="-30.48" width="0.1524" layer="91"/>
<pinref part="GND4" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="SH1" gate="G$1" pin="VSS"/>
<wire x1="-91.44" y1="20.32" x2="-88.9" y2="20.32" width="0.1524" layer="91"/>
<pinref part="GND11" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="SH1" gate="G$1" pin="VSS2"/>
<wire x1="27.94" y1="-30.48" x2="25.4" y2="-30.48" width="0.1524" layer="91"/>
<pinref part="GND13" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C14" gate="G$1" pin="2"/>
<pinref part="GND15" gate="1" pin="GND"/>
<wire x1="-96.52" y1="-147.32" x2="-96.52" y2="-149.86" width="0.1524" layer="91"/>
<pinref part="INCREMENTAL2" gate="G$1" pin="1"/>
<wire x1="-96.52" y1="-149.86" x2="-96.52" y2="-154.94" width="0.1524" layer="91"/>
<wire x1="-96.52" y1="-154.94" x2="-96.52" y2="-160.02" width="0.1524" layer="91"/>
<wire x1="-96.52" y1="-160.02" x2="-96.52" y2="-162.56" width="0.1524" layer="91"/>
<wire x1="-58.42" y1="-160.02" x2="-96.52" y2="-160.02" width="0.1524" layer="91"/>
<junction x="-96.52" y="-160.02"/>
<pinref part="R37" gate="G$1" pin="2"/>
<wire x1="-96.52" y1="-154.94" x2="-93.98" y2="-154.94" width="0.1524" layer="91"/>
<junction x="-96.52" y="-154.94"/>
<pinref part="R38" gate="G$1" pin="2"/>
<wire x1="-93.98" y1="-149.86" x2="-96.52" y2="-149.86" width="0.1524" layer="91"/>
<junction x="-96.52" y="-149.86"/>
</segment>
<segment>
<pinref part="INCREMENTAL1" gate="G$1" pin="1"/>
<pinref part="GND14" gate="1" pin="GND"/>
<wire x1="-55.88" y1="-116.84" x2="-96.52" y2="-116.84" width="0.1524" layer="91"/>
<wire x1="-96.52" y1="-116.84" x2="-96.52" y2="-119.38" width="0.1524" layer="91"/>
<pinref part="C15" gate="G$1" pin="2"/>
<wire x1="-96.52" y1="-116.84" x2="-96.52" y2="-111.76" width="0.1524" layer="91"/>
<junction x="-96.52" y="-116.84"/>
<pinref part="R42" gate="G$1" pin="2"/>
<wire x1="-96.52" y1="-111.76" x2="-96.52" y2="-106.68" width="0.1524" layer="91"/>
<wire x1="-96.52" y1="-106.68" x2="-96.52" y2="-104.14" width="0.1524" layer="91"/>
<wire x1="-96.52" y1="-106.68" x2="-93.98" y2="-106.68" width="0.1524" layer="91"/>
<junction x="-96.52" y="-106.68"/>
<pinref part="R41" gate="G$1" pin="2"/>
<wire x1="-93.98" y1="-111.76" x2="-96.52" y2="-111.76" width="0.1524" layer="91"/>
<junction x="-96.52" y="-111.76"/>
</segment>
<segment>
<pinref part="CONTACT2" gate="G$1" pin="1"/>
<pinref part="C16" gate="G$1" pin="2"/>
<wire x1="127" y1="45.72" x2="106.68" y2="45.72" width="0.1524" layer="91"/>
<wire x1="106.68" y1="45.72" x2="101.6" y2="45.72" width="0.1524" layer="91"/>
<wire x1="101.6" y1="45.72" x2="101.6" y2="48.26" width="0.1524" layer="91"/>
<pinref part="GND16" gate="1" pin="GND"/>
<wire x1="106.68" y1="45.72" x2="106.68" y2="43.18" width="0.1524" layer="91"/>
<junction x="106.68" y="45.72"/>
<wire x1="106.68" y1="45.72" x2="106.68" y2="50.8" width="0.1524" layer="91"/>
<pinref part="R44" gate="G$1" pin="1"/>
<wire x1="106.68" y1="50.8" x2="109.22" y2="50.8" width="0.1524" layer="91"/>
<wire x1="101.6" y1="45.72" x2="86.36" y2="45.72" width="0.1524" layer="91"/>
<junction x="101.6" y="45.72"/>
<pinref part="D9" gate="G$1" pin="A"/>
<wire x1="86.36" y1="45.72" x2="86.36" y2="48.26" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R46" gate="G$1" pin="2"/>
<pinref part="GND17" gate="1" pin="GND"/>
<wire x1="109.22" y1="22.86" x2="106.68" y2="22.86" width="0.1524" layer="91"/>
<wire x1="106.68" y1="22.86" x2="106.68" y2="17.78" width="0.1524" layer="91"/>
<pinref part="CONTACT1" gate="G$1" pin="1"/>
<pinref part="D10" gate="G$1" pin="A"/>
<wire x1="127" y1="17.78" x2="106.68" y2="17.78" width="0.1524" layer="91"/>
<wire x1="106.68" y1="17.78" x2="101.6" y2="17.78" width="0.1524" layer="91"/>
<wire x1="101.6" y1="17.78" x2="86.36" y2="17.78" width="0.1524" layer="91"/>
<wire x1="86.36" y1="17.78" x2="86.36" y2="20.32" width="0.1524" layer="91"/>
<pinref part="C17" gate="G$1" pin="2"/>
<wire x1="101.6" y1="17.78" x2="101.6" y2="20.32" width="0.1524" layer="91"/>
<junction x="101.6" y="17.78"/>
<junction x="106.68" y="17.78"/>
<wire x1="106.68" y1="17.78" x2="106.68" y2="15.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND18" gate="1" pin="GND"/>
<pinref part="IC5" gate="G$1" pin="GND"/>
<wire x1="88.9" y1="-124.46" x2="96.52" y2="-124.46" width="0.1524" layer="91"/>
<junction x="96.52" y="-124.46"/>
<wire x1="96.52" y1="-124.46" x2="96.52" y2="-127" width="0.1524" layer="91"/>
<pinref part="C18" gate="G$1" pin="2"/>
<wire x1="96.52" y1="-109.22" x2="96.52" y2="-124.46" width="0.1524" layer="91"/>
<pinref part="RS485_1" gate="G$1" pin="5"/>
<wire x1="116.84" y1="-109.22" x2="101.6" y2="-109.22" width="0.1524" layer="91"/>
<wire x1="101.6" y1="-109.22" x2="101.6" y2="-124.46" width="0.1524" layer="91"/>
<pinref part="RS485_2" gate="G$1" pin="5"/>
<wire x1="101.6" y1="-124.46" x2="101.6" y2="-144.78" width="0.1524" layer="91"/>
<wire x1="101.6" y1="-144.78" x2="116.84" y2="-144.78" width="0.1524" layer="91"/>
<wire x1="96.52" y1="-124.46" x2="101.6" y2="-124.46" width="0.1524" layer="91"/>
<junction x="101.6" y="-124.46"/>
</segment>
<segment>
<pinref part="GND20" gate="1" pin="GND"/>
<wire x1="-22.86" y1="81.28" x2="-25.4" y2="81.28" width="0.1524" layer="91"/>
<wire x1="-25.4" y1="81.28" x2="-25.4" y2="78.74" width="0.1524" layer="91"/>
<pinref part="I2C" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="Q1" gate="G$1" pin="E"/>
<pinref part="GND22" gate="1" pin="GND"/>
<wire x1="38.1" y1="-127" x2="38.1" y2="-129.54" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="-223.52" y1="86.36" x2="-223.52" y2="88.9" width="0.1524" layer="91"/>
<junction x="-223.52" y="88.9"/>
<pinref part="C2" gate="G$1" pin="2"/>
<wire x1="-248.92" y1="88.9" x2="-248.92" y2="93.98" width="0.1524" layer="91"/>
<wire x1="-248.92" y1="88.9" x2="-223.52" y2="88.9" width="0.1524" layer="91"/>
<junction x="-248.92" y="88.9"/>
<wire x1="-223.52" y1="88.9" x2="-223.52" y2="91.44" width="0.1524" layer="91"/>
<pinref part="GND23" gate="1" pin="GND"/>
<pinref part="IC8" gate="G$1" pin="GND"/>
<pinref part="C21" gate="G$1" pin="2"/>
<wire x1="-223.52" y1="88.9" x2="-203.2" y2="88.9" width="0.1524" layer="91"/>
<wire x1="-203.2" y1="88.9" x2="-190.5" y2="88.9" width="0.1524" layer="91"/>
<wire x1="-190.5" y1="88.9" x2="-190.5" y2="93.98" width="0.1524" layer="91"/>
<junction x="-223.52" y="88.9"/>
<pinref part="C27" gate="G$1" pin="2"/>
<wire x1="-203.2" y1="93.98" x2="-203.2" y2="88.9" width="0.1524" layer="91"/>
<junction x="-203.2" y="88.9"/>
<pinref part="L1" gate="G$1" pin="C"/>
<wire x1="-142.24" y1="93.98" x2="-142.24" y2="88.9" width="0.1524" layer="91"/>
<wire x1="-142.24" y1="88.9" x2="-170.18" y2="88.9" width="0.1524" layer="91"/>
<junction x="-190.5" y="88.9"/>
<pinref part="IC10" gate="G$1" pin="GND"/>
<wire x1="-170.18" y1="88.9" x2="-190.5" y2="88.9" width="0.1524" layer="91"/>
<wire x1="-170.18" y1="91.44" x2="-170.18" y2="88.9" width="0.1524" layer="91"/>
<junction x="-170.18" y="88.9"/>
</segment>
<segment>
<pinref part="C20" gate="G$1" pin="2"/>
<wire x1="-190.5" y1="55.88" x2="-190.5" y2="60.96" width="0.1524" layer="91"/>
<pinref part="IC7" gate="G$1" pin="GND"/>
<wire x1="-223.52" y1="55.88" x2="-223.52" y2="58.42" width="0.1524" layer="91"/>
<wire x1="-223.52" y1="55.88" x2="-203.2" y2="55.88" width="0.1524" layer="91"/>
<junction x="-223.52" y="55.88"/>
<pinref part="GND26" gate="1" pin="GND"/>
<wire x1="-203.2" y1="55.88" x2="-190.5" y2="55.88" width="0.1524" layer="91"/>
<wire x1="-248.92" y1="55.88" x2="-223.52" y2="55.88" width="0.1524" layer="91"/>
<wire x1="-223.52" y1="55.88" x2="-223.52" y2="53.34" width="0.1524" layer="91"/>
<pinref part="C23" gate="G$1" pin="2"/>
<wire x1="-248.92" y1="60.96" x2="-248.92" y2="55.88" width="0.1524" layer="91"/>
<junction x="-248.92" y="55.88"/>
<pinref part="C28" gate="G$1" pin="2"/>
<wire x1="-203.2" y1="60.96" x2="-203.2" y2="55.88" width="0.1524" layer="91"/>
<junction x="-203.2" y="55.88"/>
<pinref part="L2" gate="G$1" pin="C"/>
<wire x1="-190.5" y1="55.88" x2="-167.64" y2="55.88" width="0.1524" layer="91"/>
<wire x1="-167.64" y1="55.88" x2="-167.64" y2="60.96" width="0.1524" layer="91"/>
<junction x="-190.5" y="55.88"/>
</segment>
<segment>
<pinref part="C22" gate="G$1" pin="-"/>
<pinref part="GND24" gate="1" pin="GND"/>
<wire x1="-190.5" y1="127" x2="-190.5" y2="124.46" width="0.1524" layer="91"/>
<pinref part="J1" gate="G$1" pin="2"/>
<wire x1="-190.5" y1="124.46" x2="-190.5" y2="121.92" width="0.1524" layer="91"/>
<wire x1="-223.52" y1="132.08" x2="-220.98" y2="132.08" width="0.1524" layer="91"/>
<wire x1="-190.5" y1="124.46" x2="-220.98" y2="124.46" width="0.1524" layer="91"/>
<wire x1="-220.98" y1="124.46" x2="-220.98" y2="132.08" width="0.1524" layer="91"/>
<junction x="-190.5" y="124.46"/>
</segment>
<segment>
<pinref part="GND25" gate="1" pin="GND"/>
<pinref part="R54" gate="G$1" pin="1"/>
<wire x1="-162.56" y1="129.54" x2="-162.56" y2="132.08" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND12" gate="1" pin="GND"/>
<pinref part="SH1" gate="G$1" pin="VSS3"/>
<wire x1="27.94" y1="20.32" x2="25.4" y2="20.32" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="33.02" y1="134.62" x2="-7.62" y2="134.62" width="0.1524" layer="91"/>
<pinref part="IC6" gate="G$1" pin="GND"/>
<wire x1="33.02" y1="147.32" x2="33.02" y2="134.62" width="0.1524" layer="91"/>
<pinref part="USB" gate="G$1" pin="GND"/>
<wire x1="-10.16" y1="134.62" x2="-7.62" y2="134.62" width="0.1524" layer="91"/>
<pinref part="GND2" gate="1" pin="GND"/>
<wire x1="-7.62" y1="134.62" x2="-7.62" y2="132.08" width="0.1524" layer="91"/>
<junction x="-7.62" y="134.62"/>
</segment>
<segment>
<pinref part="GND10" gate="1" pin="GND"/>
<pinref part="C32" gate="G$1" pin="-"/>
<wire x1="-218.44" y1="-38.1" x2="-218.44" y2="-35.56" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="MOTEUR1" gate="G$1" pin="4"/>
<pinref part="GND19" gate="1" pin="GND"/>
<wire x1="-266.7" y1="-17.78" x2="-264.16" y2="-17.78" width="0.1524" layer="91"/>
<wire x1="-264.16" y1="-17.78" x2="-264.16" y2="-20.32" width="0.1524" layer="91"/>
<pinref part="MOTEUR1" gate="G$1" pin="1"/>
<wire x1="-266.7" y1="-2.54" x2="-264.16" y2="-2.54" width="0.1524" layer="91"/>
<wire x1="-264.16" y1="-2.54" x2="-264.16" y2="-17.78" width="0.1524" layer="91"/>
<junction x="-264.16" y="-17.78"/>
</segment>
<segment>
<pinref part="POWER_EN" gate="G$1" pin="2"/>
<wire x1="-210.82" y1="35.56" x2="-208.28" y2="35.56" width="0.1524" layer="91"/>
<pinref part="GND21" gate="1" pin="GND"/>
<wire x1="-208.28" y1="35.56" x2="-208.28" y2="33.02" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="-259.08" y1="-53.34" x2="-259.08" y2="-50.8" width="0.1524" layer="91"/>
<wire x1="-259.08" y1="-50.8" x2="-246.38" y2="-50.8" width="0.1524" layer="91"/>
<wire x1="-246.38" y1="-50.8" x2="-231.14" y2="-50.8" width="0.1524" layer="91"/>
<wire x1="-231.14" y1="-50.8" x2="-215.9" y2="-50.8" width="0.1524" layer="91"/>
<wire x1="-215.9" y1="-50.8" x2="-210.82" y2="-50.8" width="0.1524" layer="91"/>
<wire x1="-210.82" y1="-50.8" x2="-210.82" y2="-53.34" width="0.1524" layer="91"/>
<wire x1="-215.9" y1="-53.34" x2="-215.9" y2="-50.8" width="0.1524" layer="91"/>
<junction x="-215.9" y="-50.8"/>
<pinref part="R15" gate="G$1" pin="2"/>
<wire x1="-231.14" y1="-55.88" x2="-231.14" y2="-50.8" width="0.1524" layer="91"/>
<junction x="-231.14" y="-50.8"/>
<wire x1="-246.38" y1="-58.42" x2="-246.38" y2="-50.8" width="0.1524" layer="91"/>
<junction x="-246.38" y="-50.8"/>
<pinref part="C4" gate="G$1" pin="1"/>
<pinref part="GND9" gate="1" pin="GND"/>
<pinref part="U2" gate="G$1" pin="GNDA"/>
<pinref part="U2" gate="G$1" pin="GNDB"/>
</segment>
<segment>
<pinref part="GND27" gate="1" pin="GND"/>
<pinref part="C5" gate="G$1" pin="-"/>
<wire x1="-218.44" y1="-109.22" x2="-218.44" y2="-106.68" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="MOTEUR2" gate="G$1" pin="4"/>
<pinref part="GND28" gate="1" pin="GND"/>
<wire x1="-266.7" y1="-88.9" x2="-264.16" y2="-88.9" width="0.1524" layer="91"/>
<wire x1="-264.16" y1="-88.9" x2="-264.16" y2="-91.44" width="0.1524" layer="91"/>
<pinref part="MOTEUR2" gate="G$1" pin="1"/>
<wire x1="-266.7" y1="-73.66" x2="-264.16" y2="-73.66" width="0.1524" layer="91"/>
<wire x1="-264.16" y1="-73.66" x2="-264.16" y2="-88.9" width="0.1524" layer="91"/>
<junction x="-264.16" y="-88.9"/>
</segment>
<segment>
<wire x1="-259.08" y1="-121.92" x2="-259.08" y2="-119.38" width="0.1524" layer="91"/>
<wire x1="-259.08" y1="-119.38" x2="-246.38" y2="-119.38" width="0.1524" layer="91"/>
<wire x1="-246.38" y1="-119.38" x2="-231.14" y2="-119.38" width="0.1524" layer="91"/>
<wire x1="-231.14" y1="-119.38" x2="-215.9" y2="-119.38" width="0.1524" layer="91"/>
<wire x1="-215.9" y1="-119.38" x2="-210.82" y2="-119.38" width="0.1524" layer="91"/>
<wire x1="-210.82" y1="-119.38" x2="-210.82" y2="-121.92" width="0.1524" layer="91"/>
<wire x1="-215.9" y1="-121.92" x2="-215.9" y2="-119.38" width="0.1524" layer="91"/>
<junction x="-215.9" y="-119.38"/>
<pinref part="R57" gate="G$1" pin="2"/>
<wire x1="-231.14" y1="-124.46" x2="-231.14" y2="-119.38" width="0.1524" layer="91"/>
<junction x="-231.14" y="-119.38"/>
<wire x1="-246.38" y1="-127" x2="-246.38" y2="-119.38" width="0.1524" layer="91"/>
<junction x="-246.38" y="-119.38"/>
<pinref part="C9" gate="G$1" pin="1"/>
<pinref part="GND32" gate="1" pin="GND"/>
<pinref part="U3" gate="G$1" pin="GNDA"/>
<pinref part="U3" gate="G$1" pin="GNDB"/>
</segment>
<segment>
<pinref part="GND33" gate="1" pin="GND"/>
<pinref part="C19" gate="G$1" pin="-"/>
<wire x1="-218.44" y1="-177.8" x2="-218.44" y2="-175.26" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="MOTEUR3" gate="G$1" pin="4"/>
<pinref part="GND34" gate="1" pin="GND"/>
<wire x1="-266.7" y1="-157.48" x2="-264.16" y2="-157.48" width="0.1524" layer="91"/>
<wire x1="-264.16" y1="-157.48" x2="-264.16" y2="-160.02" width="0.1524" layer="91"/>
<pinref part="MOTEUR3" gate="G$1" pin="1"/>
<wire x1="-266.7" y1="-142.24" x2="-264.16" y2="-142.24" width="0.1524" layer="91"/>
<wire x1="-264.16" y1="-142.24" x2="-264.16" y2="-157.48" width="0.1524" layer="91"/>
<junction x="-264.16" y="-157.48"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="USB" gate="G$1" pin="VBUS"/>
<pinref part="F3" gate="G$1" pin="1"/>
<wire x1="-10.16" y1="154.94" x2="-7.62" y2="154.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VBAT_SENSE" class="0">
<segment>
<pinref part="SH1" gate="G$1" pin="AN0/VREF+/CN2/RA0"/>
<wire x1="-91.44" y1="-45.72" x2="-88.9" y2="-45.72" width="0.1524" layer="91"/>
<label x="-91.44" y="-45.72" size="1.778" layer="95" rot="MR0"/>
</segment>
<segment>
<pinref part="R54" gate="G$1" pin="2"/>
<pinref part="R34" gate="G$1" pin="1"/>
<wire x1="-162.56" y1="142.24" x2="-162.56" y2="144.78" width="0.1524" layer="91"/>
<wire x1="-162.56" y1="144.78" x2="-162.56" y2="147.32" width="0.1524" layer="91"/>
<wire x1="-160.02" y1="144.78" x2="-162.56" y2="144.78" width="0.1524" layer="91"/>
<junction x="-162.56" y="144.78"/>
<label x="-160.02" y="144.78" size="1.778" layer="95"/>
</segment>
</net>
<net name="Q1B" class="0">
<segment>
<pinref part="SH1" gate="G$1" pin="AN5/C1IN+/RP3/CN7/RB3"/>
<wire x1="27.94" y1="-55.88" x2="25.4" y2="-55.88" width="0.1524" layer="91"/>
<label x="27.94" y="-55.88" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R42" gate="G$1" pin="1"/>
<pinref part="R39" gate="G$1" pin="2"/>
<wire x1="-83.82" y1="-106.68" x2="-73.66" y2="-106.68" width="0.1524" layer="91"/>
<label x="-81.28" y="-106.68" size="1.778" layer="95"/>
</segment>
</net>
<net name="Q1A" class="0">
<segment>
<pinref part="SH1" gate="G$1" pin="AN4/C1IN-/RP2/CN6/RB2"/>
<wire x1="27.94" y1="-60.96" x2="25.4" y2="-60.96" width="0.1524" layer="91"/>
<label x="27.94" y="-60.96" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R40" gate="G$1" pin="2"/>
<pinref part="R41" gate="G$1" pin="1"/>
<wire x1="-73.66" y1="-111.76" x2="-83.82" y2="-111.76" width="0.1524" layer="91"/>
<label x="-81.28" y="-111.76" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$38" class="0">
<segment>
<pinref part="R35" gate="G$1" pin="1"/>
<pinref part="INCREMENTAL2" gate="G$1" pin="3"/>
<wire x1="-63.5" y1="-149.86" x2="-58.42" y2="-149.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$39" class="0">
<segment>
<pinref part="INCREMENTAL2" gate="G$1" pin="2"/>
<pinref part="R36" gate="G$1" pin="1"/>
<wire x1="-58.42" y1="-154.94" x2="-63.5" y2="-154.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="INCREMENTAL1" gate="G$1" pin="3"/>
<pinref part="R39" gate="G$1" pin="1"/>
<wire x1="-55.88" y1="-106.68" x2="-63.5" y2="-106.68" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$40" class="0">
<segment>
<pinref part="INCREMENTAL1" gate="G$1" pin="2"/>
<pinref part="R40" gate="G$1" pin="1"/>
<wire x1="-55.88" y1="-111.76" x2="-63.5" y2="-111.76" width="0.1524" layer="91"/>
</segment>
</net>
<net name="DSP_RX2" class="0">
<segment>
<pinref part="IC5" gate="G$1" pin="RO"/>
<wire x1="55.88" y1="-109.22" x2="58.42" y2="-109.22" width="0.1524" layer="91"/>
<label x="55.88" y="-109.22" size="1.778" layer="95" rot="MR0"/>
</segment>
<segment>
<pinref part="D12" gate="G$1" pin="A"/>
<wire x1="58.42" y1="-162.56" x2="55.88" y2="-162.56" width="0.1524" layer="91"/>
<label x="58.42" y="-162.56" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="SH1" gate="G$1" pin="PWM2L1/RP23/CN17/PMA0/RC7"/>
<wire x1="-91.44" y1="35.56" x2="-88.9" y2="35.56" width="0.1524" layer="91"/>
<label x="-91.44" y="35.56" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="RS485_DIR" class="0">
<segment>
<pinref part="SH1" gate="G$1" pin="INT0/RP7/CN23/PMD5/RB7"/>
<wire x1="27.94" y1="40.64" x2="25.4" y2="40.64" width="0.1524" layer="91"/>
<label x="27.94" y="40.64" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R32" gate="G$1" pin="1"/>
<wire x1="15.24" y1="-121.92" x2="17.78" y2="-121.92" width="0.1524" layer="91"/>
<label x="15.24" y="-121.92" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="N$45" class="0">
<segment>
<pinref part="SH1" gate="G$1" pin="SOSCO/T1CK/CN0/RA4"/>
<pinref part="D8" gate="G$1" pin="C"/>
<wire x1="25.4" y1="-5.08" x2="35.56" y2="-5.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$46" class="0">
<segment>
<pinref part="D8" gate="G$1" pin="A"/>
<pinref part="R43" gate="G$1" pin="1"/>
<wire x1="43.18" y1="-5.08" x2="45.72" y2="-5.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="CON1" class="0">
<segment>
<pinref part="C16" gate="G$1" pin="1"/>
<pinref part="R45" gate="G$1" pin="1"/>
<wire x1="99.06" y1="58.42" x2="101.6" y2="58.42" width="0.1524" layer="91"/>
<wire x1="101.6" y1="58.42" x2="101.6" y2="55.88" width="0.1524" layer="91"/>
<pinref part="R44" gate="G$1" pin="2"/>
<pinref part="CONTACT2" gate="G$1" pin="2"/>
<wire x1="119.38" y1="50.8" x2="121.92" y2="50.8" width="0.1524" layer="91"/>
<wire x1="121.92" y1="50.8" x2="127" y2="50.8" width="0.1524" layer="91"/>
<wire x1="101.6" y1="58.42" x2="121.92" y2="58.42" width="0.1524" layer="91"/>
<wire x1="121.92" y1="58.42" x2="121.92" y2="50.8" width="0.1524" layer="91"/>
<junction x="101.6" y="58.42"/>
<junction x="121.92" y="50.8"/>
<label x="101.6" y="58.42" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="SH1" gate="G$1" pin="AN1/VREF-/CN3/RA1"/>
<wire x1="-91.44" y1="-50.8" x2="-88.9" y2="-50.8" width="0.1524" layer="91"/>
<label x="-91.44" y="-50.8" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="CON2" class="0">
<segment>
<pinref part="SH1" gate="G$1" pin="TDI/PMA9/RA9"/>
<wire x1="27.94" y1="0" x2="25.4" y2="0" width="0.1524" layer="91"/>
<label x="27.94" y="0" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="CONTACT1" gate="G$1" pin="2"/>
<pinref part="R46" gate="G$1" pin="1"/>
<wire x1="127" y1="22.86" x2="121.92" y2="22.86" width="0.1524" layer="91"/>
<pinref part="R47" gate="G$1" pin="1"/>
<wire x1="121.92" y1="22.86" x2="119.38" y2="22.86" width="0.1524" layer="91"/>
<wire x1="99.06" y1="30.48" x2="101.6" y2="30.48" width="0.1524" layer="91"/>
<wire x1="101.6" y1="30.48" x2="121.92" y2="30.48" width="0.1524" layer="91"/>
<wire x1="121.92" y1="30.48" x2="121.92" y2="22.86" width="0.1524" layer="91"/>
<junction x="121.92" y="22.86"/>
<pinref part="C17" gate="G$1" pin="1"/>
<wire x1="101.6" y1="30.48" x2="101.6" y2="27.94" width="0.1524" layer="91"/>
<junction x="101.6" y="30.48"/>
<label x="101.6" y="30.48" size="1.778" layer="95"/>
</segment>
</net>
<net name="SCL" class="0">
<segment>
<pinref part="SH1" gate="G$1" pin="SCL1/RP8/CN22/PMD4/RB8"/>
<wire x1="27.94" y1="45.72" x2="25.4" y2="45.72" width="0.1524" layer="91"/>
<label x="27.94" y="45.72" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="-25.4" y1="86.36" x2="-22.86" y2="86.36" width="0.1524" layer="91"/>
<label x="-25.4" y="86.36" size="1.778" layer="95" rot="MR0"/>
<pinref part="I2C" gate="G$1" pin="2"/>
</segment>
</net>
<net name="SDA" class="0">
<segment>
<pinref part="SH1" gate="G$1" pin="SDA1/RP9/CN21/PMD3/RB9"/>
<wire x1="-91.44" y1="45.72" x2="-88.9" y2="45.72" width="0.1524" layer="91"/>
<label x="-91.44" y="45.72" size="1.778" layer="95" rot="MR0"/>
</segment>
<segment>
<wire x1="-25.4" y1="91.44" x2="-22.86" y2="91.44" width="0.1524" layer="91"/>
<label x="-25.4" y="91.44" size="1.778" layer="95" rot="MR0"/>
<pinref part="I2C" gate="G$1" pin="3"/>
</segment>
</net>
<net name="N$48" class="0">
<segment>
<pinref part="D9" gate="G$1" pin="C"/>
<wire x1="86.36" y1="55.88" x2="86.36" y2="58.42" width="0.1524" layer="91"/>
<pinref part="R45" gate="G$1" pin="2"/>
<wire x1="86.36" y1="58.42" x2="88.9" y2="58.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$51" class="0">
<segment>
<pinref part="D10" gate="G$1" pin="C"/>
<pinref part="R47" gate="G$1" pin="2"/>
<wire x1="86.36" y1="27.94" x2="86.36" y2="30.48" width="0.1524" layer="91"/>
<wire x1="86.36" y1="30.48" x2="88.9" y2="30.48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$47" class="0">
<segment>
<pinref part="RS485_1" gate="G$1" pin="3"/>
<wire x1="106.68" y1="-99.06" x2="106.68" y2="-114.3" width="0.1524" layer="91"/>
<wire x1="106.68" y1="-114.3" x2="106.68" y2="-134.62" width="0.1524" layer="91"/>
<wire x1="116.84" y1="-99.06" x2="106.68" y2="-99.06" width="0.1524" layer="91"/>
<pinref part="RS485_2" gate="G$1" pin="3"/>
<wire x1="106.68" y1="-134.62" x2="116.84" y2="-134.62" width="0.1524" layer="91"/>
<pinref part="J6" gate="G$1" pin="1"/>
<wire x1="96.52" y1="-152.4" x2="106.68" y2="-152.4" width="0.1524" layer="91"/>
<wire x1="106.68" y1="-152.4" x2="106.68" y2="-134.62" width="0.1524" layer="91"/>
<junction x="106.68" y="-134.62"/>
<pinref part="IC5" gate="G$1" pin="B"/>
<wire x1="88.9" y1="-114.3" x2="106.68" y2="-114.3" width="0.1524" layer="91"/>
<junction x="106.68" y="-114.3"/>
</segment>
</net>
<net name="N$52" class="0">
<segment>
<pinref part="R48" gate="G$1" pin="2"/>
<pinref part="J6" gate="G$1" pin="2"/>
<wire x1="99.06" y1="-157.48" x2="96.52" y2="-157.48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="DSP_TX2" class="0">
<segment>
<pinref part="IC5" gate="G$1" pin="DI"/>
<wire x1="55.88" y1="-124.46" x2="58.42" y2="-124.46" width="0.1524" layer="91"/>
<label x="55.88" y="-124.46" size="1.778" layer="95" rot="MR0"/>
</segment>
<segment>
<pinref part="D11" gate="G$1" pin="A"/>
<wire x1="58.42" y1="-152.4" x2="55.88" y2="-152.4" width="0.1524" layer="91"/>
<label x="58.42" y="-152.4" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="SH1" gate="G$1" pin="PWM2H1/RP22/CN18/PMA1/RC6"/>
<wire x1="-91.44" y1="40.64" x2="-88.9" y2="40.64" width="0.1524" layer="91"/>
<label x="-91.44" y="40.64" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="N$53" class="0">
<segment>
<pinref part="R49" gate="G$1" pin="1"/>
<pinref part="D11" gate="G$1" pin="C"/>
<wire x1="45.72" y1="-152.4" x2="48.26" y2="-152.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$54" class="0">
<segment>
<pinref part="D12" gate="G$1" pin="C"/>
<pinref part="R50" gate="G$1" pin="1"/>
<wire x1="48.26" y1="-162.56" x2="45.72" y2="-162.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$61" class="0">
<segment>
<pinref part="R53" gate="G$1" pin="2"/>
<pinref part="D15" gate="G$1" pin="A"/>
<wire x1="-109.22" y1="10.16" x2="-106.68" y2="10.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$31" class="0">
<segment>
<pinref part="R22" gate="G$1" pin="2"/>
<wire x1="134.62" y1="114.3" x2="137.16" y2="114.3" width="0.1524" layer="91"/>
<pinref part="D6" gate="G$1" pin="C"/>
</segment>
</net>
<net name="N$37" class="0">
<segment>
<pinref part="R23" gate="G$1" pin="2"/>
<wire x1="137.16" y1="109.22" x2="134.62" y2="109.22" width="0.1524" layer="91"/>
<pinref part="D7" gate="G$1" pin="C"/>
</segment>
</net>
<net name="N$57" class="0">
<segment>
<pinref part="D15" gate="G$1" pin="C"/>
<pinref part="SH1" gate="G$1" pin="PGED2/EMCD2/PWM1H3/RP10/CN16/PMD2/RB10"/>
<wire x1="-99.06" y1="10.16" x2="-88.9" y2="10.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="Q2A" class="0">
<segment>
<pinref part="SH1" gate="G$1" pin="PGED1/AN2/C2IN-/RP0/CN4/RB0"/>
<wire x1="-91.44" y1="-55.88" x2="-88.9" y2="-55.88" width="0.1524" layer="91"/>
<label x="-91.44" y="-55.88" size="1.778" layer="95" rot="MR0"/>
</segment>
<segment>
<pinref part="R36" gate="G$1" pin="2"/>
<pinref part="R37" gate="G$1" pin="1"/>
<wire x1="-73.66" y1="-154.94" x2="-83.82" y2="-154.94" width="0.1524" layer="91"/>
<label x="-81.28" y="-154.94" size="1.778" layer="95"/>
</segment>
</net>
<net name="Q2B" class="0">
<segment>
<pinref part="SH1" gate="G$1" pin="PGEC1/AN3/C2IN+/RP1/CN5/RB1"/>
<wire x1="-91.44" y1="-60.96" x2="-88.9" y2="-60.96" width="0.1524" layer="91"/>
<label x="-91.44" y="-60.96" size="1.778" layer="95" rot="MR0"/>
</segment>
<segment>
<pinref part="R38" gate="G$1" pin="1"/>
<pinref part="R35" gate="G$1" pin="2"/>
<wire x1="-83.82" y1="-149.86" x2="-73.66" y2="-149.86" width="0.1524" layer="91"/>
<label x="-81.28" y="-149.86" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="RS485_2" gate="G$1" pin="2"/>
<wire x1="116.84" y1="-129.54" x2="111.76" y2="-129.54" width="0.1524" layer="91"/>
<pinref part="RS485_1" gate="G$1" pin="2"/>
<wire x1="111.76" y1="-129.54" x2="111.76" y2="-119.38" width="0.1524" layer="91"/>
<wire x1="111.76" y1="-119.38" x2="111.76" y2="-93.98" width="0.1524" layer="91"/>
<wire x1="111.76" y1="-93.98" x2="116.84" y2="-93.98" width="0.1524" layer="91"/>
<pinref part="R48" gate="G$1" pin="1"/>
<wire x1="111.76" y1="-129.54" x2="111.76" y2="-157.48" width="0.1524" layer="91"/>
<wire x1="111.76" y1="-157.48" x2="109.22" y2="-157.48" width="0.1524" layer="91"/>
<junction x="111.76" y="-129.54"/>
<pinref part="IC5" gate="G$1" pin="A"/>
<wire x1="88.9" y1="-119.38" x2="111.76" y2="-119.38" width="0.1524" layer="91"/>
<junction x="111.76" y="-119.38"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="R32" gate="G$1" pin="2"/>
<pinref part="Q1" gate="G$1" pin="B"/>
<wire x1="27.94" y1="-121.92" x2="33.02" y2="-121.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="1"/>
<pinref part="F4" gate="G$1" pin="1"/>
<wire x1="-223.52" y1="137.16" x2="-220.98" y2="137.16" width="0.1524" layer="91"/>
<pinref part="F5" gate="G$1" pin="1"/>
<wire x1="-220.98" y1="137.16" x2="-215.9" y2="137.16" width="0.1524" layer="91"/>
<wire x1="-220.98" y1="137.16" x2="-220.98" y2="165.1" width="0.1524" layer="91"/>
<wire x1="-220.98" y1="165.1" x2="-215.9" y2="165.1" width="0.1524" layer="91"/>
<junction x="-220.98" y="137.16"/>
</segment>
</net>
<net name="VLOGIC" class="0">
<segment>
<pinref part="D16" gate="G$1" pin="C"/>
<pinref part="C22" gate="G$1" pin="+"/>
<wire x1="-195.58" y1="137.16" x2="-190.5" y2="137.16" width="0.1524" layer="91"/>
<wire x1="-190.5" y1="137.16" x2="-190.5" y2="134.62" width="0.1524" layer="91"/>
<label x="-190.5" y="137.16" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="-238.76" y1="104.14" x2="-248.92" y2="104.14" width="0.1524" layer="91"/>
<pinref part="C2" gate="G$1" pin="1"/>
<wire x1="-248.92" y1="104.14" x2="-248.92" y2="101.6" width="0.1524" layer="91"/>
<pinref part="IC8" gate="G$1" pin="VIN"/>
<label x="-248.92" y="104.14" size="1.778" layer="95" rot="MR0"/>
</segment>
<segment>
<pinref part="IC7" gate="G$1" pin="VIN"/>
<pinref part="C23" gate="G$1" pin="1"/>
<wire x1="-248.92" y1="71.12" x2="-238.76" y2="71.12" width="0.1524" layer="91"/>
<wire x1="-248.92" y1="68.58" x2="-248.92" y2="71.12" width="0.1524" layer="91"/>
<pinref part="D18" gate="G$1" pin="C"/>
<wire x1="-261.62" y1="71.12" x2="-248.92" y2="71.12" width="0.1524" layer="91"/>
<junction x="-248.92" y="71.12"/>
<label x="-248.92" y="71.12" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="N$58" class="0">
<segment>
<pinref part="L2" gate="G$1" pin="A"/>
<pinref part="R56" gate="G$1" pin="2"/>
<wire x1="-167.64" y1="68.58" x2="-167.64" y2="71.12" width="0.1524" layer="91"/>
<wire x1="-167.64" y1="71.12" x2="-170.18" y2="71.12" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$59" class="0">
<segment>
<pinref part="R55" gate="G$1" pin="2"/>
<wire x1="-144.78" y1="104.14" x2="-142.24" y2="104.14" width="0.1524" layer="91"/>
<pinref part="L1" gate="G$1" pin="A"/>
<wire x1="-142.24" y1="104.14" x2="-142.24" y2="101.6" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$60" class="0">
<segment>
<pinref part="C21" gate="G$1" pin="1"/>
<wire x1="-190.5" y1="101.6" x2="-190.5" y2="104.14" width="0.1524" layer="91"/>
<pinref part="IC8" gate="G$1" pin="VOUT"/>
<wire x1="-208.28" y1="104.14" x2="-203.2" y2="104.14" width="0.1524" layer="91"/>
<pinref part="C27" gate="G$1" pin="1"/>
<wire x1="-203.2" y1="104.14" x2="-190.5" y2="104.14" width="0.1524" layer="91"/>
<wire x1="-203.2" y1="101.6" x2="-203.2" y2="104.14" width="0.1524" layer="91"/>
<junction x="-203.2" y="104.14"/>
<pinref part="IC10" gate="G$1" pin="VDD"/>
<wire x1="-190.5" y1="104.14" x2="-182.88" y2="104.14" width="0.1524" layer="91"/>
<junction x="-190.5" y="104.14"/>
<pinref part="IC10" gate="G$1" pin="EN"/>
<wire x1="-182.88" y1="104.14" x2="-180.34" y2="104.14" width="0.1524" layer="91"/>
<wire x1="-180.34" y1="99.06" x2="-182.88" y2="99.06" width="0.1524" layer="91"/>
<wire x1="-182.88" y1="99.06" x2="-182.88" y2="104.14" width="0.1524" layer="91"/>
<junction x="-182.88" y="104.14"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="IC6" gate="G$1" pin="EN"/>
<pinref part="IC6" gate="G$1" pin="VDD"/>
<wire x1="20.32" y1="154.94" x2="22.86" y2="154.94" width="0.1524" layer="91"/>
<wire x1="22.86" y1="160.02" x2="20.32" y2="160.02" width="0.1524" layer="91"/>
<wire x1="20.32" y1="160.02" x2="20.32" y2="154.94" width="0.1524" layer="91"/>
<pinref part="L3" gate="G$1" pin="P$2"/>
<wire x1="15.367" y1="154.94" x2="20.32" y2="154.94" width="0.1524" layer="91"/>
<junction x="20.32" y="154.94"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="Q1" gate="G$1" pin="C"/>
<pinref part="R30" gate="G$1" pin="1"/>
<wire x1="38.1" y1="-116.84" x2="38.1" y2="-114.3" width="0.1524" layer="91"/>
<pinref part="IC5" gate="G$1" pin="_RE"/>
<wire x1="38.1" y1="-114.3" x2="38.1" y2="-111.76" width="0.1524" layer="91"/>
<wire x1="38.1" y1="-114.3" x2="55.88" y2="-114.3" width="0.1524" layer="91"/>
<junction x="38.1" y="-114.3"/>
<pinref part="IC5" gate="G$1" pin="DE"/>
<wire x1="55.88" y1="-114.3" x2="58.42" y2="-114.3" width="0.1524" layer="91"/>
<wire x1="58.42" y1="-119.38" x2="55.88" y2="-119.38" width="0.1524" layer="91"/>
<wire x1="55.88" y1="-119.38" x2="55.88" y2="-114.3" width="0.1524" layer="91"/>
<junction x="55.88" y="-114.3"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<wire x1="20.32" y1="149.86" x2="20.32" y2="144.78" width="0.1524" layer="91"/>
<pinref part="IC4" gate="G$1" pin="USBDM"/>
<wire x1="20.32" y1="144.78" x2="73.66" y2="144.78" width="0.1524" layer="91"/>
<pinref part="USB" gate="G$1" pin="D-"/>
<wire x1="20.32" y1="149.86" x2="-10.16" y2="149.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$55" class="0">
<segment>
<wire x1="15.24" y1="144.78" x2="15.24" y2="139.7" width="0.1524" layer="91"/>
<pinref part="IC4" gate="G$1" pin="USBDP"/>
<wire x1="15.24" y1="139.7" x2="73.66" y2="139.7" width="0.1524" layer="91"/>
<pinref part="USB" gate="G$1" pin="D+"/>
<wire x1="-10.16" y1="144.78" x2="15.24" y2="144.78" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$65" class="0">
<segment>
<pinref part="C32" gate="G$1" pin="+"/>
<wire x1="-218.44" y1="-27.94" x2="-218.44" y2="-25.4" width="0.1524" layer="91"/>
<wire x1="-218.44" y1="-25.4" x2="-210.82" y2="-25.4" width="0.1524" layer="91"/>
<wire x1="-210.82" y1="-25.4" x2="-210.82" y2="-40.64" width="0.1524" layer="91"/>
<wire x1="-210.82" y1="-40.64" x2="-205.74" y2="-40.64" width="0.1524" layer="91"/>
<pinref part="Q2" gate="G$1" pin="D"/>
<wire x1="-205.74" y1="-40.64" x2="-205.74" y2="-38.1" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="VCC"/>
<wire x1="-210.82" y1="-25.4" x2="-210.82" y2="-22.86" width="0.1524" layer="91"/>
<junction x="-210.82" y="-25.4"/>
</segment>
</net>
<net name="N$67" class="0">
<segment>
<pinref part="Q2" gate="G$1" pin="G"/>
<pinref part="U1" gate="G$1" pin="CP"/>
<wire x1="-200.66" y1="-30.48" x2="-200.66" y2="-22.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="DISABLE_MOTOR" class="0">
<segment>
<pinref part="POWER_EN" gate="G$1" pin="1"/>
<wire x1="-208.28" y1="40.64" x2="-210.82" y2="40.64" width="0.1524" layer="91"/>
<label x="-208.28" y="40.64" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R5" gate="G$1" pin="1"/>
<wire x1="-185.42" y1="-2.54" x2="-182.88" y2="-2.54" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="DIAGA/ENA"/>
<pinref part="U1" gate="G$1" pin="DIAGB/ENB"/>
<wire x1="-182.88" y1="-2.54" x2="-180.34" y2="-2.54" width="0.1524" layer="91"/>
<wire x1="-185.42" y1="2.54" x2="-182.88" y2="2.54" width="0.1524" layer="91"/>
<wire x1="-182.88" y1="2.54" x2="-182.88" y2="-2.54" width="0.1524" layer="91"/>
<junction x="-182.88" y="-2.54"/>
<wire x1="-182.88" y1="2.54" x2="-182.88" y2="7.62" width="0.1524" layer="91"/>
<junction x="-182.88" y="2.54"/>
<wire x1="-182.88" y1="7.62" x2="-180.34" y2="7.62" width="0.1524" layer="91"/>
<label x="-180.34" y="7.62" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R17" gate="G$1" pin="1"/>
<wire x1="-185.42" y1="-73.66" x2="-182.88" y2="-73.66" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="DIAGA/ENA"/>
<pinref part="U2" gate="G$1" pin="DIAGB/ENB"/>
<wire x1="-182.88" y1="-73.66" x2="-180.34" y2="-73.66" width="0.1524" layer="91"/>
<wire x1="-185.42" y1="-68.58" x2="-182.88" y2="-68.58" width="0.1524" layer="91"/>
<wire x1="-182.88" y1="-68.58" x2="-182.88" y2="-73.66" width="0.1524" layer="91"/>
<junction x="-182.88" y="-73.66"/>
<wire x1="-182.88" y1="-68.58" x2="-182.88" y2="-63.5" width="0.1524" layer="91"/>
<junction x="-182.88" y="-68.58"/>
<wire x1="-182.88" y1="-63.5" x2="-180.34" y2="-63.5" width="0.1524" layer="91"/>
<label x="-180.34" y="-63.5" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R59" gate="G$1" pin="1"/>
<wire x1="-185.42" y1="-142.24" x2="-182.88" y2="-142.24" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="DIAGA/ENA"/>
<pinref part="U3" gate="G$1" pin="DIAGB/ENB"/>
<wire x1="-182.88" y1="-142.24" x2="-180.34" y2="-142.24" width="0.1524" layer="91"/>
<wire x1="-185.42" y1="-137.16" x2="-182.88" y2="-137.16" width="0.1524" layer="91"/>
<wire x1="-182.88" y1="-137.16" x2="-182.88" y2="-142.24" width="0.1524" layer="91"/>
<junction x="-182.88" y="-142.24"/>
<wire x1="-182.88" y1="-137.16" x2="-182.88" y2="-132.08" width="0.1524" layer="91"/>
<junction x="-182.88" y="-137.16"/>
<wire x1="-182.88" y1="-132.08" x2="-180.34" y2="-132.08" width="0.1524" layer="91"/>
<label x="-180.34" y="-132.08" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="R14" gate="G$1" pin="2"/>
<wire x1="-233.68" y1="-73.66" x2="-231.14" y2="-73.66" width="0.1524" layer="91"/>
<pinref part="R15" gate="G$1" pin="1"/>
<wire x1="-231.14" y1="-73.66" x2="-226.06" y2="-73.66" width="0.1524" layer="91"/>
<wire x1="-231.14" y1="-66.04" x2="-231.14" y2="-73.66" width="0.1524" layer="91"/>
<junction x="-231.14" y="-73.66"/>
<pinref part="U2" gate="G$1" pin="CS"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<pinref part="R16" gate="G$1" pin="1"/>
<wire x1="-198.12" y1="-53.34" x2="-198.12" y2="-50.8" width="0.1524" layer="91"/>
<wire x1="-198.12" y1="-50.8" x2="-195.58" y2="-50.8" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="PWM"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<pinref part="R18" gate="G$1" pin="1"/>
<wire x1="-180.34" y1="-78.74" x2="-185.42" y2="-78.74" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="INB"/>
</segment>
</net>
<net name="N$23" class="0">
<segment>
<pinref part="R19" gate="G$1" pin="1"/>
<wire x1="-185.42" y1="-83.82" x2="-180.34" y2="-83.82" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="INA"/>
</segment>
</net>
<net name="N$26" class="0">
<segment>
<pinref part="C5" gate="G$1" pin="+"/>
<wire x1="-218.44" y1="-99.06" x2="-218.44" y2="-96.52" width="0.1524" layer="91"/>
<wire x1="-218.44" y1="-96.52" x2="-210.82" y2="-96.52" width="0.1524" layer="91"/>
<wire x1="-210.82" y1="-96.52" x2="-210.82" y2="-111.76" width="0.1524" layer="91"/>
<wire x1="-210.82" y1="-111.76" x2="-205.74" y2="-111.76" width="0.1524" layer="91"/>
<pinref part="Q4" gate="G$1" pin="D"/>
<wire x1="-205.74" y1="-111.76" x2="-205.74" y2="-109.22" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="VCC"/>
<wire x1="-210.82" y1="-96.52" x2="-210.82" y2="-93.98" width="0.1524" layer="91"/>
<junction x="-210.82" y="-96.52"/>
</segment>
</net>
<net name="N$27" class="0">
<segment>
<pinref part="Q4" gate="G$1" pin="G"/>
<pinref part="U2" gate="G$1" pin="CP"/>
<wire x1="-200.66" y1="-101.6" x2="-200.66" y2="-93.98" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$43" class="0">
<segment>
<pinref part="R52" gate="G$1" pin="2"/>
<wire x1="-233.68" y1="-142.24" x2="-231.14" y2="-142.24" width="0.1524" layer="91"/>
<pinref part="R57" gate="G$1" pin="1"/>
<wire x1="-231.14" y1="-142.24" x2="-226.06" y2="-142.24" width="0.1524" layer="91"/>
<wire x1="-231.14" y1="-134.62" x2="-231.14" y2="-142.24" width="0.1524" layer="91"/>
<junction x="-231.14" y="-142.24"/>
<pinref part="U3" gate="G$1" pin="CS"/>
</segment>
</net>
<net name="N$44" class="0">
<segment>
<pinref part="R58" gate="G$1" pin="1"/>
<wire x1="-198.12" y1="-121.92" x2="-198.12" y2="-119.38" width="0.1524" layer="91"/>
<wire x1="-198.12" y1="-119.38" x2="-195.58" y2="-119.38" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="PWM"/>
</segment>
</net>
<net name="N$50" class="0">
<segment>
<pinref part="R60" gate="G$1" pin="1"/>
<wire x1="-180.34" y1="-147.32" x2="-185.42" y2="-147.32" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="INB"/>
</segment>
</net>
<net name="N$56" class="0">
<segment>
<pinref part="R61" gate="G$1" pin="1"/>
<wire x1="-185.42" y1="-152.4" x2="-180.34" y2="-152.4" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="INA"/>
</segment>
</net>
<net name="N$62" class="0">
<segment>
<pinref part="C19" gate="G$1" pin="+"/>
<wire x1="-218.44" y1="-167.64" x2="-218.44" y2="-165.1" width="0.1524" layer="91"/>
<wire x1="-218.44" y1="-165.1" x2="-210.82" y2="-165.1" width="0.1524" layer="91"/>
<wire x1="-210.82" y1="-165.1" x2="-210.82" y2="-180.34" width="0.1524" layer="91"/>
<wire x1="-210.82" y1="-180.34" x2="-205.74" y2="-180.34" width="0.1524" layer="91"/>
<pinref part="Q6" gate="G$1" pin="D"/>
<wire x1="-205.74" y1="-180.34" x2="-205.74" y2="-177.8" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="VCC"/>
<wire x1="-210.82" y1="-165.1" x2="-210.82" y2="-162.56" width="0.1524" layer="91"/>
<junction x="-210.82" y="-165.1"/>
</segment>
</net>
<net name="N$63" class="0">
<segment>
<pinref part="Q6" gate="G$1" pin="G"/>
<pinref part="U3" gate="G$1" pin="CP"/>
<wire x1="-200.66" y1="-170.18" x2="-200.66" y2="-162.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="B3" class="0">
<segment>
<wire x1="-266.7" y1="-147.32" x2="-226.06" y2="-147.32" width="0.1524" layer="91"/>
<label x="-246.38" y="-147.32" size="1.778" layer="95"/>
<pinref part="U3" gate="G$1" pin="OUTB"/>
<pinref part="MOTEUR3" gate="G$1" pin="2"/>
</segment>
</net>
<net name="A3" class="0">
<segment>
<wire x1="-226.06" y1="-152.4" x2="-266.7" y2="-152.4" width="0.1524" layer="91"/>
<label x="-246.38" y="-152.4" size="1.778" layer="95"/>
<pinref part="U3" gate="G$1" pin="OUTA"/>
<pinref part="MOTEUR3" gate="G$1" pin="3"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="F3" gate="G$1" pin="2"/>
<pinref part="L3" gate="G$1" pin="P$1"/>
<wire x1="2.54" y1="154.94" x2="4.953" y2="154.94" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
